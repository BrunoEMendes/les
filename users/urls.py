from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from  . import views

from users.views import *


urlpatterns = [
	#path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
	path('login/', MyLogin, name='login'),
	path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
	path('register/', UserFormView.as_view(), name='register'),
	path('register/update/<int:pk>/', UserUpdateView, name='register_update'),
	path('perfil/criar/', ProfileCreateView.as_view(), name='profile_add'),
	path('perfil/update/<int:pk>/', ProfileUpdateView.as_view(), name='profile_update'),

	#
	path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
	path('password_reset/', auth_views.PasswordResetView.as_view(template_name='users/registration/password_reset_form.html', email_template_name = 'users/registration/password_reset_email.html', subject_template_name = 'users/registration/password_reset_subject.txt'), name='password_reset'),
	path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/registration/password_reset_done.html'), name='password_reset_done'),
	path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/registration/password_reset_confirm.html'), name='password_reset_confirm'),    
	path('reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/registration/password_reset_complete.html'), name='password_reset_complete'),

	#path('add_profile/', AddProfileView.as_view(), name='add_profile'),
	#
	path('perfil/administrador/', views.ProfAdminCreate, name='perfil_admin'),
	path('perfil/cordenador/', views.ProfCordenadorCreate, name='perfil_cordenador'),
	path('perfil/professor/', views.ProfProfessorCreate, name='perfil_professor'),
	path('perfil/colaborador/', views.ProfcolaboradorCreate, name='perfil_colaborador'),
	path('perfil/participante/', views.ProfileParticipanteCreate, name='perfil_participante'),

	#
	path('utilizadores/', views.UtilizadoresList, name='utilizadores_list'),
   #path('utilizadores/utilizador_list/', TemplateView.as_view(template_name="users/utilizador_list.html"), name='utilizador_list'),
	path('utilizadores/visualizar/<int:pk>/', UtilizadorDetailView.as_view(), name='detail'),
	path('utilizadores/apagar/<int:pk>/', UtilizadorDeleteView.as_view(), name='delete'),
	path('utilizadores/editar/<int:pk>/', UtilizadorUpdateView, name='update'),
	path('utilizadores/update_estado/<int:pk>/', update_estado, name='update_estado'),
	#
	path('list_profile/', ProfileListView.as_view(), name='list_profile'),
	path('profile/<int:pk>/', ProfileDetailView.as_view(), name='profile_detail'),
	#
	path('usuario/', views.UserProfileView, name='user_profile'),
	path('usuario/password_change/', change_password, name='password_change'),
	path('usuario/apagar/<int:pk>/', UserDeleteView.as_view(), name='user_delete'),
	path('usuario/editar', views.UserProfileEditView, name='user_profile_edit'),
	path('usuario/update/<int:pk>/', views.UsuarioUpdateView, name='usuario_update'),
	path('usuario/update_perfil/<int:pk>/', Usuario2UpdateView.as_view(), name='usuario2_update'),
	path('usuario/criar_perfil/<int:pk>/', Usuario2CreateView.as_view(), name='usuario2_profile'),
	
	#
	path('update_group/<int:pk>/', GroupUpdateView.as_view(), name='update_group'),
	path('delete_group/<int:pk>/', GroupDeleteView.as_view(), name='delete_group'),
	#
	path('participante/criar/', ParticipantCreateView.as_view(), name='add_participant'),
	#path('add_individual_participant/', IndividualParticipantCreateView.as_view(), name='add_individual_participant'),
	#path('add_participant_group/', ParticipantGroupCreateView.as_view(), name='add_participant_group'),
	#path('panel_of_participant/', ParticipantGroupCreateView.as_view(), name='panel_of_participant'),
	#path('participant_group_add/', ParticipantGroupCreateView.as_view(), name='participant_group_add'),
	#path('secondary_teacher_add/', SecondaryTeacherCreateView.as_view(), name='secondary_teacher_add'),
	#
	path('colaborador/criar/', ColaboratorCreateView.as_view(), name='colaborator_add'),
	path('colaborador/schedule/', ScheduleColaboratorCreateView.as_view(), name='add_schedule_colaborator'),
	#path('colaborador/update/<int:pk>/', ColaboratorUpdateView.as_view(), name='colaborator_update'),
	

	#path('colaborator_list/', ColaboratorListView.as_view(), name='colaborator_list'),
	#
	#path('teacher_list/', TeacherListView.as_view(), name='teacher_list'),
	path('docente/criar/', TeacherCreateView.as_view(), name='teacher_add'),
	#
  
	path('cordenador/criar', CoordinatorCreateView.as_view(), name='coordinator_add'),
	#
	path('administrador/criar/', AdministratorCreateView.as_view(), name='administrator_add'),

	#
	path('administrador/update_estado/<int:pk>/', update_estado_adm, name='update_estado_adm'),
    path('professor/update_estado/<int:pk>/', update_estado_tch, name='update_estado_tch'),
	path('colaborador/update_estado/<int:pk>/', update_estado_colab, name='update_estado_colab'),
	path('coordenador/update_estado/<int:pk>/', update_estado_coord, name='update_estado_coord')



]