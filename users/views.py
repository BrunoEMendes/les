from django.shortcuts import render, redirect,  get_object_or_404
from django.template.context import RequestContext
from django.views.generic import View, DetailView, ListView
from django.contrib.auth import authenticate, login, logout
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm

from django.http import HttpResponse 

from django.contrib.auth.models import Group
#from django.contrib.auth.models import User_Groups
from django.contrib.auth.models import User
from university.models import Department, OrganicUnit

from .models import *
from .forms import * 












# class createUser(CreateView):
# 	model = User
# 	fields = ['username', 'email']

# 	def form_valid(form, self):
# 		instance = form.save(commit=False)

# 		password = randon(8)

# 		sendEmail = (username, password)





# **********************************************        REGISTAR       ****************************************#


class UserFormView(View):
	form_class = UserRegisterForm
	template_name = 'users/registration_form.html'

	def get(self, request):
		form = self.form_class(None)
		
		form.fields['username'].help_text = 'Obrigatório. 150 carateres ou menos. Apenas letras, dígitos @/./+/-/_.'
		form.fields['password1'].help_text = 'Sua palavra-passe não pode ser muito semelhante às suas outras informações pessoais.\nA sua palavra-passe deve conter pelo menos 8 caracteres.\nSua senha não pode ser totalmente numérica.'
		form.fields['password2'].help_text = ''
		form.fields['email'].help_text = ''

		return render(request, self.template_name, {'form':form})

	def post(self, request):
		form = self.form_class(request.POST)

		if form.is_valid():

			user = form.save(commit=False)

			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user.set_password(password)
			user.save()

			user = authenticate(username=username, password = password)

			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect ('profile_add')			

		else:

			form.fields['username'].help_text = 'Obrigatório. 150 carateres ou menos. Apenas letras, dígitos @/./+/-/_.'
			form.fields['password1'].help_text = 'Sua palavra-passe não pode ser muito semelhante às suas outras informações pessoais.\nA sua palavra-passe deve conter pelo menos 8 caracteres.\nSua senha não pode ser totalmente numérica.'
			form.fields['password2'].help_text = None
			form.fields['email'].help_text = None				

			

		return render(request, self.template_name, {'form':form})


#class UserUpdateView (UpdateView):
#	model = User
#	form_class = UserRegisterForm
#	template_name = "users/register_update_form.html"
#
#	def form_valid(self, form):
#
#		form.save()
#		username = self.request.POST['username']
#		
#		user = User.objects.get(username = username)
#		login(self.request, user)
#		return redirect('users:profile_add')


def UserUpdateView(request,pk):
	user = get_object_or_404(User, pk = pk)
	
	if(request.method == 'GET'):	
		#print(user.username)
		#print(user.id)
		
		form = UserRegisterForm(instance=user)
		
		
		
		return render(request,'users/registration_form.html', {'form':form})
		
		
	elif (request.method == 'POST'):
		form = UserRegisterForm(request.POST,instance=user)
		if form.is_valid():
			form.save()
			username = request.POST['username']
			user = User.objects.get(username = username)
			login(request, user)
			return redirect('profile_add')		

		

		return render(request,'users/registration_form.html', {'form':form})


def MyLogin(request):
	
	template = "users/login.html"

	if request.user.is_authenticated:
		return render(request, 'inscricao/index.html')

	if request.method == "POST":

		username = request.POST.get("username_form")
		password = request.POST.get("password_form")

		try:
			user = User.objects.get(username=username) # falha qndo username ñ existe
			

			if Colaborator.objects.filter(user_id = user.id).exists():
				if user.colaborator.valid:
					user = authenticate(request, username=username, password=password)

					if user is not None:
						login(request, user)
						return render(request, 'inscricao/index.html')

					else:
						messages.error(request,'O password ou username não estão corretos.')
						return redirect('/login/')
				else:
					messages.error(request,'O utilizador ainda não tem permissão')
					return redirect('/login/')

			elif Teacher.objects.filter(user_id = user.id).exists():
				if user.teacher.valid:
					user = authenticate(request, username=username, password=password)

					if user is not None:
						login(request, user)
						return render(request, 'inscricao/index.html')

					else:
						messages.error(request,'O password ou username não estão corretos.')
						return redirect('/login/')

				else:
					messages.error(request,'O utilizador ainda não tem permissão')
					return redirect('/login/')

			elif Coordinator.objects.filter(user_id = user.id).exists():
				if user.coordinator.valid:
					user = authenticate(request, username=username, password=password)

					if user is not None:
						login(request, user)
						return render(request, 'inscricao/index.html')

					else:
						messages.error(request,'O password ou username não estão corretos.')
						return redirect('/login/')
						
				else:
					messages.error(request,'O utilizador ainda não tem permissão')
					return redirect('/login/')

				 

			elif Administrator.objects.filter(user_id = user.id).exists():
				if user.administrator:
					user = authenticate(request, username=username, password=password)

					if user is not None:
						login(request, user)
						return render(request, 'inscricao/index.html')

					else:
						messages.error(request,'O password ou username não estão corretos.')
						return redirect('/login/')

				else:
					messages.error(request,'O utilizador ainda não tem permissão')
					return redirect('/login/')

			elif Participant.objects.filter(user_id = user.id).exists():
				user = authenticate(request, username=username, password=password)

				if user is not None:
					login(request, user)
					return render(request, 'inscricao/index.html')

				else:
					messages.error(request,'O password ou username não estão corretos.')
					return redirect('/login/')

				

			else:
				messages.error(request,'O utilizador ainda não tem registo activo')
				return redirect('/login/')

		except:
			messages.error(request,'O username não existe no sistema ')
			return redirect('/login/')


	else:

		return render(request, template)




# **********************************************        PERFIL       ****************************************#



#@ nao visivel para quem ja tem perfil('redirecionado para a view de edicao')
class ProfileCreateView(CreateView):

	def get(self, request, *args, **kwargs):
		context = {'form': ProfileForm()}
		return render(request, 'users/profile_form.html', context)

	def post(self, request, *args, **kwargs):
		form = ProfileForm(request.POST)
		
		if form.is_valid():
			form = form.save(commit=False)
			form.user = self.request.user
			form.save()

			if form.tipos_de_utilizadores == 'Participante':
				return redirect('add_participant')
			
			elif form.tipos_de_utilizadores == 'Professor Universitário':
				return redirect('teacher_add')
			
			elif form.tipos_de_utilizadores == 'Colaborador':
				return redirect('colaborator_add')
			
			elif form.tipos_de_utilizadores == 'Coordenador':
				return redirect('coordinator_add')
			
			elif form.tipos_de_utilizadores == 'Administrator':
				return redirect('administrator_add')

		return render(request, 'users/profile_form.html', {'form': form})

	
class ProfileCreateCoordinatorView(CreateView):
	model = Profile
	fields = ['birth_date', 'citizen_card', 'phone_number', 'tipos_de_utilizadores']

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user

		if not instance.user.groups.filter(name='Participantes'):
			grupo = Group.objects.get(name='Participantes')
			instance.user.groups.add(grupo.id)

			print('Usuario cadastrado no grupo Participantes')

		else:
			print('Usuario ja esta no grupo Participantes')


		return super(ProfileFormView, self).form_valid(form)




class AddProfileView(CreateView):
	model = Group
	fields = ['name']
	template_name = 'users/add_profile.html'
	success_url = '/list_profile/'



class ProfileListView(ListView):
	template_name = 'users/profile_list.html'
	context_object_name = 'profiles'

	

	def get_queryset(self):    	
		return Group.objects.all()
		

class ProfileDetailView(DetailView):
	model = Group
	template_name = 'users/profile_detail.html'	


class ProfileUpdateView(UpdateView):
	model = Profile
	form_class = ProfileForm
	template_name = "users/profile_update_form.html"

	def form_valid(self, object):
		if self.object.tipos_de_utilizadores == 'Participante':
			return redirect('add_participant')
		
		elif self.object.tipos_de_utilizadores== 'Professor Universitário':
			return redirect('teacher_add')
		
		elif self.object.tipos_de_utilizadores == 'Colaborador':
			return redirect('colaborator_add')
		
		elif self.object.tipos_de_utilizadores == 'Coordenador':
			return redirect('coordinator_add')
		
		elif self.object.tipos_de_utilizadores == 'Administrador':
			return redirect('administrator_add')


def UserProfileView (request):
	
	
	args = request.user

	context = {'args' : args}
	


	return render(request, 'users/user_profile.html', context)


def UserProfileEditView (request):

	args = request.user

	
	if Profile.objects.get(user_id = args.id) != '':
		profile_user = Profile.objects.get(user_id = args.id)
		form = ProfileForm(instance = profile_user)

		old_profile = profile_user.tipos_de_utilizadores

	elif request.method == 'POST':
		
		form = ProfileForm(request.POST, instance = profile_user)
		if form.is_valid():
			instance = form.save(commit=False)
			usuario = instance.user


			if profile_user.tipos_de_utilizadores != '' :
				profile_type = profile_user.tipos_de_utilizadores
				print(profile_type)

			
				if old_profile == profile_type :
					form.save()
					print("igual")
					print(usuario.profile.tipos_de_utilizadores)
					print(profile_type)
					print(profile_user.tipos_de_utilizadores)
					print(args.profile.tipos_de_utilizadores)

					return redirect ('/usuario/')

				else :
					print("chegeuei")
					if profile_user.tipos_de_utilizadores == 'Colaborador' :
						profile_old = Colaborator.objects.get(user_id = args.id)
						profile_old.delete()
						form.save()
						print("vou apagar")
						return redirect ('/usuario/')

					if profile_user.tipos_de_utilizadores == 'Participante' :
						profile_old = Participant.objects.get(user_id = args.id)
						profile_old.delete()
						form.save()
						print("vou apagar")
						return redirect ('/usuario/')

			else :
				form.save()
				return redirect ('/usuario/')

		context = {'form' : form}
		return render(request, 'users/profile_form.html', context)
	
	else:
		context = {'form' : form}
		return render(request, 'users/profile_form.html', context)





# **********************************************        Profile Admin / cordenaodr       ****************************************#


def ParticipantCreateView_admin(request):
	args = request.user
	context = {'args' : args }

	return render(request, 'users/user_profile.html', context)

def ProfAdminCreate (request):

	args = request.user
	context = {'args' : args }

	return render(request, 'users/user_profile.html', context)

def ProfCordenadorCreate (request):



	#form_profile = ProfileForm()

	if request.method == 'POST':
		form = CordinatorForm(request.POST)



		if form.is_valid():
			instance = form.save(commit=False)
			usuario = instance.user

			
			
			if Coordinator.objects.filter(user_id = usuario.id).exists():
				
				return HttpResponse(" erro")
			else:


				if Profile.objects.filter(user_id = usuario.id).exists():

					profile = Profile.objects.get(user_id = usuario.id)
					profile.tipos_de_utilizadores = 'Coordenador'
					profile.save()
					
					form.save()

					return redirect('utilizadores_list')

				else :

					profile = Profile.objects.create(user_id = usuario.id, tipos_de_utilizadores = 'Coordenador')
					profile.save()
					form.save()

					return redirect('utilizadores_list')

		else:
			form = CordinatorForm()

			context = {}
			context ['form'] = form

			return render(request, 'users/profile_cordinator_form.html', context)

	else:

		form = CordinatorForm()

		context = {}
		context ['form'] = form

		

		return render(request, 'users/profile_cordinator_form.html', context)


def ProfProfessorCreate (request):

	#form_profile = ProfileForm()

	if request.method == 'POST':
		form = ProfProfessorForm(request.POST)



		if form.is_valid():
			instance = form.save(commit=False)
			usuario = instance.user

			
			
			if Colaborator.objects.filter(user_id = usuario.id).exists():
				
				return HttpResponse(" erro")
			else:


				if Profile.objects.filter(user_id = usuario.id).exists():

					profile = Profile.objects.get(user_id = usuario.id)
					profile.tipos_de_utilizadores = 'Professor Universitário'
					profile.save()
					
					form.save()

					return redirect('utilizadores_list')

				else :

					profile = Profile.objects.create(user_id = usuario.id, tipos_de_utilizadores = 'Professor Universitário')
					profile.save()
					form.save()

					return redirect('utilizadores_list')

		else:
			form = ProfProfessorForm()

			context = {}
			context ['form'] = form

			return render(request, 'users/profile_professor_form.html', context)

	else:

		form = ProfProfessorForm()

		context = {}
		context ['form'] = form

		

		return render(request, 'users/profile_professor_form.html', context)

def ProfcolaboradorCreate (request):

	#form_profile = ProfileForm()

	if request.method == 'POST':
		form = ProfcolaboradorForm(request.POST)



		if form.is_valid():
			instance = form.save(commit=False)
			usuario = instance.user

			
			
			if Colaborator.objects.filter(user_id = usuario.id).exists():
				
				return HttpResponse(" erro")
			else:


				if Profile.objects.filter(user_id = usuario.id).exists():

					profile = Profile.objects.get(user_id = usuario.id)
					profile.tipos_de_utilizadores = 'Colaborador'
					profile.save()
					
					form.save()

					return redirect('utilizadores_list')

				else :

					profile = Profile.objects.create(user_id = usuario.id, tipos_de_utilizadores = 'Colaborador')
					profile.save()
					form.save()

					return redirect('utilizadores_list')

		else:
			form = ProfcolaboradorForm()

			context = {}
			context ['form'] = form

			return render(request, 'users/profile_colaborator_form.html', context)

	else:

		form = ProfcolaboradorForm()

		context = {}
		context ['form'] = form

		

		return render(request, 'users/profile_colaborator_form.html', context)
  
def ProfileParticipanteCreate (request):

	#form_profile = ProfileForm()

	if request.method == 'POST':
		form = ParticipantForm(request.POST)



		if form.is_valid():
			instance = form.save(commit=False)
			usuario = instance.user

			
			
			if Participant.objects.filter(user_id = usuario.id).exists():
				
				return HttpResponse(" erro")
			else:


				if Profile.objects.filter(user_id = usuario.id).exists():

					profile = Profile.objects.get(user_id = usuario.id)
					profile.tipos_de_utilizadores = 'Participante'
					profile.save()
					
					form.save()

					return redirect('utilizadores_list')

				else :

					profile = Profile.objects.create(user_id = usuario.id, tipos_de_utilizadores = 'Participante')
					profile.save()
					form.save()

					return redirect('utilizadores_list')

		else:
			form = ParticipantForm()

			context = {}
			context ['form'] = form

			return render(request, 'users/profile_participant_form.html', context)

	else:

		form = ParticipantForm()

		context = {}
		context ['form'] = form

		return render(request, 'users/profile_participant_form.html', context)



# **********************************************        PARTICIPANTE       ****************************************#


class ParticipantCreateView(CreateView):
	model = Participant	
	fields = ['school_year', 'area']

	template_name = 'users/participant_form.html'
	success_url = '/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user				

		return super(ParticipantCreateView, self).form_valid(form)

class IndividualParticipantCreateView(CreateView):
	model = Participant_Individual
	fields = ['acompanhantes']
	success_url = '/'
	template_name = 'users/individual_participant_form.html'

	def form_valid(self, form):
		instance = form.save(commit=False)		
		instance.part = Participant.objects.get(user_id = self.request.user.id)
		print(instance.part)
		return super(IndividualParticipantCreateView, self).form_valid(form)

class ParticipantGroupCreateView(CreateView):
	model = Participant_Group
	fields = ['n_participants', 'n_teachers']
	success_url = '/'
	template_name = 'users/teacher_participant_form.html'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.part = Participant.objects.filter(user = self.request.user)

		return super(IndividualParticipantCreateView, self).form_valid(form)


class SecondaryTeacherCreateView(CreateView):
	model = Participant	
	fields = ['school_year', 'area']

	template_name = 'users/teacher_participant_form.html'
	success_url = 'participant_group_add'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user

		#Inserir codigo para salvar o usruario em Groups dentro de auth				

		return super(SecondaryTeacherCreateView, self).form_valid(form)


class ParticipantGroupCreateView(CreateView):
	model = Participant_Group	
	fields = ['n_participants', 'n_teachers']

	template_name = 'users/participant_group_form.html'
	success_url = '/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		
		participant = Participant.objects.filter(user=self.request.user)

		instance.part = participant 

		return super(IndividualParticipantCreateView, self).form_valid(form)




# *********************************************      COLABORADOR       *******************************************#


class ColaboratorListView(ListView):
	template_name = 'users/colaborator_list.html'
	context_object_name = 'colaborators'

	

	def get_queryset(self):    	
		return Colaborator.objects.all()


class ColaboratorCreateView(CreateView):
	model = Colaborator
	fields = ['schedule', 'department']
	template_name = 'users/colaborator_form.html'
	success_url = '/logout/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user


		#if not instance.user.groups.filter(name='Colaborador'):
			# Id = 2 eh do grupo colaborador
		#	grupo = Group.objects.get(name="Colaboradores")
		#	instance.user.groups.add(grupo.id)

		#	print('Usuario cadastrado no grupo Colaborador')

		#else:
		#	print('Usuario ja esta no grupo Colaborador')

		return super(ColaboratorCreateView, self).form_valid(form)

class ScheduleColaboratorCreateView(CreateView):
	model = Schedule_Colaborator
	fields = ['start_hour','end_hour', 'date']
	template_name = 'users/schedule_colaborator_form.html'
	success_url = '/'



# ************************************************       DOCENTE       ***************************************************#
	  
class TeacherCreateView(CreateView):
	model = Teacher
	fields = ['department']
	template_name = 'users/teacher_form.html'
	success_url = '/logout/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user

	#	if not instance.user.groups.filter(name='Professor Universitário'):
	#		grupo = Group.objects.get(name = 'Professores Universitários')
	#		instance.user.groups.add(grupo.id)
	#		print('Usuario cadastrado no grupo Professores Universitarios')
	#
	#	else:
	#		print('Usuario ja esta no grupo Professores Universitarios')

		return super(TeacherCreateView, self).form_valid(form)



class TeacherListView(ListView):
	template_name = 'users/teacher_list.html'
	context_object_name = 'teachers'

	def get_queryset(self):    	
		return Teacher.objects.all()

# ************************************************        CORDENADOR       ***************************************************#

class CoordinatorCreateView(CreateView):
	model = Coordinator
	form_class = CordinatorForm
	template_name = 'users/coordinator_form.html'
	success_url = '/logout/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user

		return super(CoordinatorCreateView, self).form_valid(form)



# ***************************************************       ADMINISTRADOR     ***********************************************#

class AdministratorCreateView(CreateView):
	model = Administrator
	fields = '__all__'
	template_name = 'users/admin_form.html'
	success_url = '/logout/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user

		return super(CoordinatorCreateView, self).form_valid(form)


def update_estado(request, pk):

	#adm = get_object_or_404(Administrator, pk=pk)
	
	try:
		user = Administrator.objects.get(user_id=pk)

		if user.valid == 1:
			user.valid = 0
		else:
			user.valid = 1

		user.save()
		print(user)
		return redirect('/utilizadores/')

	except:
		pass

	finally:

		try:  
			user = Colaborator.objects.get(user_id=pk)

			if user.valid == 1:
				user.valid = 0
			else:
				user.valid = 1

			user.save()
			print(user)
			return redirect('/utilizadores/')

		except:
			pass

		finally:

			try:  
				user = Coordinator.objects.get(user_id=pk)

				if user.valid == 1:
					user.valid = 0
				else:
					user.valid = 1

				user.save()
				print(user)
				return redirect('/utilizadores/')

			except:
				pass

			finally:
				try:  
					user = Teacher.objects.get(user_id=pk)

					if user.valid == 1:
						user.valid = 0
					else:
						user.valid = 1

					user.save()
					print(user)
					return redirect('/utilizadores/')

				except:
					pass
					pass
				finally:
					try:  
						user = Participant.objects.get(user_id=pk)
						return redirect('/utilizadores/')

					except:
						messages.error(request, "utilizador não encontrado")









# ***************************************************     GRUPO      ******************************************************#


class GroupDeleteView(DeleteView):
	model = Group
	success_url = '/list_profile/'
	template_name = 'users/delete_group.html'


class GroupUpdateView(UpdateView):
	model = Group
	fields = ['name']
	template_name = 'users/update_group_form.html'
	success_url = '/list_profile/'






#**************************************************        UTILIZADORES      ***********************************************#

def UtilizadoresList(request):

	
	#group = Group.objects.all()
	#participante =  Participant.objects.all()
	#cordenador = Coordinator.objects.all()
	#administrador = Administrator.objects.all()
	#colaborador = Colaborator.objects.all()
	#teacher = Teacher.objects.all()
	
	users = User.objects.all()
	departamentos = Department.objects.all()
	unidadesOrganicas = OrganicUnit.objects.all()

	
		
	#context = {'participante' : participante, 'teacher ' : teacher , 'colaborador' : colaborador, 'cordenador' : cordenador,'administrador' :administrador }

	context = {
		'users' : users,
		'departamentos': departamentos,
		'unidadesOrganicas': unidadesOrganicas,
	}
	return render(request, 'users/utilizadores_list.html', context)



class UtilizadorDetailView(DetailView):
	model = User
	template_name = 'users/utilizadores_detail.html'


	

class UtilizadorDeleteView(DeleteView):
	model = User
	success_url = '/utilizadores/'
	template_name = 'users/delete_utilizador.html'
	success_message = 'utilizador apagado com sucesso'

class UserDeleteView(DeleteView):
	model = User
	success_url = '/'
	template_name = 'users/delete_utilizador.html'
	success_message = 'utilizador apagado com sucesso'



def UsuarioUpdateView(request,pk):
	user = get_object_or_404(User, pk = pk)
	
	if(request.method == 'GET'):	
		print(user.username)
		print(user.id)
		form = UserUpdateForm(instance=user)

		
		form.fields['username'].help_text = 'Obrigatório. 150 carateres ou menos. Apenas letras, dígitos @/./+/-/_.'
		form.fields['email'].help_text = None
		
		
		return render(request,'users/usuario_registration_form.html', {'form':form})
		
		
	elif (request.method == 'POST'):
		form = UserUpdateForm(request.POST,instance=user)
		if form.is_valid():
			form.save()
			username = request.POST['username']
			user = User.objects.get(username = username)
			login(request, user)
			# ver o caso de ele não conseguir logar
			user.valid = 1
			return redirect('user_profile')		

		else:
					
			
			form.fields['username'].help_text = 'Obrigatório. 150 carateres ou menos. Apenas letras, dígitos @/./+/-/_.'
			form.fields['email'].help_text = None
			

		return render(request,'users/usuario_registration_form.html', {'form':form})



class Usuario2UpdateView(UpdateView):
	model = Profile
	fields = ['birth_date', 'citizen_card', 'phone_number'] #'tipos_de_utilizadores'
	template_name = 'users/update_usuario_profile_form.html'
	success_url = '/usuario/'

class Usuario2CreateView(CreateView):
	model = Profile
	fields = ['birth_date', 'citizen_card', 'phone_number'] #'tipos_de_utilizadores'
	template_name = 'users/update_usuario_profile_form.html'
	success_url = '/usuario/'

	def form_valid(self, form):
		instance = form.save(commit=False)
		instance.user = self.request.user

		if Teacher.objects.filter(user_id = self.request.user.id).exists():
			print(self.request.user.teacher)
			instance.tipos_de_utilizadores = 'Professor Universitário'

		elif Colaborator.objects.filter(user_id = self.request.user.id).exists():
			print(self.request.user.colaborator)
			instance.tipos_de_utilizadores = 'Colaborador' 

		elif Coordinator.objects.filter(user_id = self.request.user.id).exists():
			print(self.request.user.coordinator)
			instance.tipos_de_utilizadores = 'Coordenador'

		elif Administrator.objects.filter(user_id = self.request.user.id).exists():
			print(self.request.user.administrator)
			instance.tipos_de_utilizadores = 'Administrador'

		elif Participant.objects.filter(user_id = self.request.user.id).exists():
			print(self.request.user.participant)
			instance.tipos_de_utilizadores = 'Participante'

		instance.save()

		return super(Usuario2CreateView, self).form_valid(form)





def UtilizadorUpdateView(request, pk):


	#print(pk)

	 
		
	#context = {'participante' : participante, 'teacher ' : teacher , 'colaborador' : colaborador, 'cordenador' : cordenador,'administrador' :administrador }

	context = {'users' : users}

	return render(request, 'users/utilizadores_list.html', context)

	#model = User
	#fields = ['username']
	#template_name = 'users/update_utilizador.html'
	#success_url = '/utilizadores/'




def update_estado_adm(request, pk):

	#adm = get_object_or_404(Administrator, pk=pk)
	try:
		adm = Administrator.objects.get(user_id=pk)
		if adm.valid == 1:
			adm.valid = 0
		else:
			adm.valid = 1

		adm.save()
		messages.success(request, 'Estado alterado com sucesso.')
		return redirect('/utilizadores/')

	except:
		messages.error(request, "Administrator não encontrado.")
		return redirect('/utilizadores/')


def update_estado_tch(request, pk):

	#adm = get_object_or_404(Administrator, pk=pk)
	try:
		teacher = Teacher.objects.get(user_id=pk)
		if teacher.valid == 1:
			teacher.valid = 0
		else:
			teacher.valid = 1

		teacher.save()
		messages.success(request, 'Estado alterado com sucesso.')
		return redirect('/utilizadores/')

	except:
		messages.error(request, "Professor não encontrado.")
		return redirect('/utilizadores/')


def update_estado_colab(request, pk):

	#adm = get_object_or_404(Administrator, pk=pk)
	try:
		colab = Colaborator.objects.get(user_id=pk)
		if colab.valid == 1:
			colab.valid = 0
		else:
			colab.valid = 1

		colab.save()
		messages.success(request, 'Estado alterado com sucesso.')
		return redirect('/utilizadores/')

	except:
		messages.error(request, "Colaborador não encontrado.")
		return redirect('/utilizadores/')


def update_estado_coord(request, pk):

	#adm = get_object_or_404(Administrator, pk=pk)
	try:
		coord = Coordinator.objects.get(user_id=pk)
		if coord.valid == 1:
			coord.valid = 0
		else:
			coord.valid = 1

		coord.save()
		messages.success(request, 'Estado alterado com sucesso.')
		return redirect('/utilizadores/')

	except:
		messages.error(request, "Coordenador não encontrado.")
		return redirect('/utilizadores/')


def change_password(request):

	if request.user.is_authenticated:
		if request.method == 'POST':
			form = PasswordChangeForm(request.user, request.POST)
			if form.is_valid():
				user = form.save()
				update_session_auth_hash(request, user)  # Important!
				messages.success(request, 'Sua password foi alterada com sucesso!')
				return redirect('logout')
			else:
				messages.error(request, 'Porfavor, corrija o erro.')
				form.fields['old_password'].help_text=None
				form.fields['new_password1'].help_text=None
				form.fields['new_password2'].help_text=None
		else:
			form = PasswordChangeForm(request.user)
			form.fields['old_password'].help_text=None
			form.fields['new_password1'].help_text=None
			form.fields['new_password2'].help_text=None

		return render(request, 'users/change_password.html', {'form': form })
	else:
		return render(request, 'inscricao/index.html')