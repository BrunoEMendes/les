from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from university.models import Department, OrganicUnit

User._meta.get_field('username').verbose_name = 'Nome'
User._meta.get_field('last_name').verbose_name = 'Último Nome'
User._meta.get_field('first_name').verbose_name = 'Primeiro Nome'
User._meta.get_field('email').verbose_name = 'Email'

class Participant(models.Model):
    school_year = models.IntegerField(verbose_name = 'Ano Escolar')
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    area = models.CharField(max_length = 32, verbose_name = 'area')

    def __str__(self):
        return str(self.user.username) + '-' + str(self.school_year) + '-' + str(self.area)

    class Meta:
        verbose_name_plural = 'Participante'
        ordering = ['user__username']


class Participant_Group(models.Model):
    n_participants = models.IntegerField(
        default = 1,
        verbose_name = 'Nº de estudantes',
        validators =
        [
            MinValueValidator(1),
        ]
    )
    n_teachers = models.BigIntegerField(
        default = 1,
        verbose_name = "Nº de professores",
        validators =
        [
            MinValueValidator(1),
        ]
    )
    part = models.ForeignKey(Participant, on_delete = models.CASCADE)

    def __str__(self):

        #return self.part + '-' + self.n_participants + '-' + self.n_teachers
        return f'{self.part}|{self.n_participants}|{self.n_teachers}'


    class Meta:
        verbose_name_plural = 'Participante Grupo'


class Participant_Individual(models.Model):
    acompanhantes = models.IntegerField(
        default = 1,
        verbose_name = 'Nº do grupo',
        validators =
        [
            MinValueValidator(1),
        ]
    )
    part = models.ForeignKey(Participant, on_delete = models.CASCADE)

    def __str__(self):
        return f'{self.part}'

    class Meta:
        verbose_name_plural = 'Participante Individual'

class Teacher(models.Model):
    valid = models.BooleanField(default = False)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return f'Docente {self.user.username}'

    def is_valid(self):
        return self.valid

    class Meta:
        verbose_name_plural = 'Docente'

class Coordinator(models.Model):
    valid = models.BooleanField(default = False)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    ou = models.ForeignKey(OrganicUnit, on_delete = models.CASCADE)

    def __str__(self):
        return f'Coordenador {self.user.username}'

    class Meta:
        verbose_name_plural = 'Coordenadores'
        ordering = ['user__username']


class Schedule_Colaborator(models.Model):
    start_hour = models.TimeField()
    end_hour = models.TimeField()
    date = models.DateField()

    def __str__(self):
        return f'Dia {self.date}'


    class Meta:
        verbose_name_plural = 'Horários colaboradores'

class Colaborator(models.Model):
    valid = models.BooleanField(default = False)
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    schedule = models.ForeignKey(Schedule_Colaborator, on_delete = models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user.username}'

    class Meta:
        verbose_name_plural = 'Colaboradores'
        ordering = ['user__username']


class Administrator(models.Model):
    id_t = models.AutoField(primary_key = True)
    user = models.OneToOneField(User, on_delete = models.CASCADE)

    def __str__(self):
        return f'Administrador {self.user.username}'


    class Meta:
        verbose_name_plural = 'Administrador'

    class Meta:
        verbose_name_plural = 'Administrador'
        ordering = ['user__username']

# Create your models here.

#isto vai ser apagado

# Create your models here.
class Profile(models.Model):
    user_list = (
        ('Participante','Participante'),
        ('Professor Universitário','Professor Universitário'),
        ('Coordenador','Coordenador'),
        ('Colaborador','Colaborador'),
        ('Administrador','Administrador'),
    )

    tipos_de_utilizadores = models.CharField(max_length = 30, choices=user_list ,verbose_name='Tipo de Utilizador')
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(blank = True, null = True, verbose_name='Data de Nascimento')

    #https://github.com/stefanfoulis/django-phonenumber-field change to this later
    phone_number = models.CharField(max_length = 20, blank = True, null = True)

    #sets min value to 100000000 which is the lowest possible cc
    #idk the max of the cc, fix later, not sure if it works either
    citizen_card = models.IntegerField(
        default = 100000000,
        verbose_name = 'Cartão de Cidadão',
        validators=
        [
            MaxValueValidator(999999999),
            MinValueValidator(100000000)
        ]
    )

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Perfis'

    # deficienciqas = models.TextField()
    #adicionar mais cenas

