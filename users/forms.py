from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from bootstrap_datepicker_plus import DatePickerInput
from dateutil.relativedelta import relativedelta
from intl_tel_input.widgets import IntlTelInputWidget
from crispy_forms.helper import FormHelper
from django.core.validators import MaxValueValidator, MinValueValidator
import datetime
from django.contrib.auth.forms import UserChangeForm
from .models import Profile, Participant, Coordinator, Schedule_Colaborator, Teacher, Administrator, Colaborator


#utilizado porque qndo se edita o registo, não alterar a senha
# usado para editar o registo do usuario, com campos extra
# como o primeiro e o ultimo nome

class UserUpdateForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError(u'Endereço de email tem de ser unico.')

        return email



class UserRegisterForm(UserCreationForm):
  

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError(u'Endereço de email tem de ser unico.')
       
        return email

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['tipos_de_utilizadores','birth_date', 'citizen_card', 'phone_number']
        widgets = {
            'birth_date' : DatePickerInput(
                format = '%Y-%m-%d',
                options = {
                    'minDate': '1900/01/01',
                    'maxDate': (datetime.datetime.today() - relativedelta(years=16)).strftime('%Y-%m-%d'),
                }
            ),
            'phone_number' : IntlTelInputWidget(preferred_countries = ['pt', 'es', 'gb'], default_code = 'pt'),
        }

# usada no coordinatorcreateview, onde atribui uma label diferente
class CordinatorForm(forms.ModelForm):
    class Meta:
        model = Coordinator
        fields = ['ou']
        labels = { 'ou':'Unidade Organica',}





#----------------------------------#

class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = '__all__'




class ProfcolaboradorForm(forms.ModelForm):
    class Meta:
        model = Colaborator
        fields = '__all__'



class ScheduleColaboratorForm(forms.ModelForm):
    class Meta:
        model = Schedule_Colaborator
        fields = '__all__'


class ProfProfessorForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'


class ProfAdminForm(forms.ModelForm):
    class Meta:
        model = Administrator
        fields = '__all__'