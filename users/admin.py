from django.contrib import admin
from django.contrib.auth.models import User
from .models import (Profile,
                    Participant,
                    Participant_Group,
                    Participant_Individual,
                    Teacher,
                    Colaborator,
                    Coordinator,
                    Administrator,
                    Schedule_Colaborator,
)

# Register your models here.
admin.site.register(Profile)
admin.site.register(Participant)
admin.site.register(Participant_Group)
admin.site.register(Participant_Individual)
admin.site.register(Teacher)
admin.site.register(Colaborator)
admin.site.register(Coordinator)
admin.site.register(Administrator)
admin.site.register(Schedule_Colaborator)

