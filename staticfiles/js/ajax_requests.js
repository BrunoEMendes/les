// GLOBAL VARS
// add your paths here :)
url_paths = {
    // university
    buildings: 'atividades/ajax/load-buildings',
    location: 'atividades/ajax/load-location',

    // location based on if its reserved already

    build_res: 'atividades/ajax/load-free-buildings',
    location_free: 'atividades/ajax/load_free_location',

    // session data
    session_table: 'atividades/ajax/activity_detail_info',

    // change status

    change_status: 'atividades/ajax/change_status',

    // material

    modal_info: 'atividades/ajax/modal_fill/',

}

id_forms = {
    campus: '#id_lform-campus',
    buildings: '#id_lform-building',
    location: '#id_lform-location',
}

id_forms_update = {
    campus: '#id_campus',
    buildings: '#id_building',
    location: '#id_location',
}


forms_coord = {
    // location
    campus: '#div_campus',
    buildings: '#div_building',
    location: '#div_location',

    modal_info: '#modal-fill',
}


/**
 * idk why windows.location.origin wasnt working :/
 * @returns base url
 */
function getBaseUrl() {
    var pathArray = location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var url = protocol + '//' + host + '/';
    return url;
}

/**
 * sends a request for item_id to the parameter url, and attaches the result to the field_id
 * @param {*} url for the request
 * @param {*} item_id that you want to get
 * @param {*} field_id html location where you wanna replace the content
 */
function ajax_request(url, item_id, field_id) {
    $.ajax({
        url: getBaseUrl() + url,
        data: {
            'data_id': item_id
        },
        success: function(data) {
            $(field_id).html(data); // replace the contents
        },
    });
}

/**
 * sends a request for item_id.bid and item_said to the parameter url, and attaches the result to the field_id
 * @param {*} url for the request
 * @param {*} item_id that you want to get
 * @param {*} field_id html location where you wanna replace the content
 */
function ajax_request_with_2_fields(url, item_id, field_id) {
    $.ajax({
        url: getBaseUrl() + url,
        data: {
            'data_id': item_id.bid,
            'sa_id': item_id.said
        },
        success: function(data) {
            $(field_id).html(data); // replace the contents
        },
    });
}

/**
 * sends a request for item_id to the parameter url, and attaches the result to the field_id, it hides the loading screen after its done
 * @param {*} url for the request
 * @param {*} item_id that you want to get
 * @param {*} field_id html location where you wanna replace the content
 */
function ajax_request_with_load(url, item_id, field_id, load_path) {
    $.ajax({ // initialize an AJAX request
        url: getBaseUrl() + url,
        data: {
            'data_id': item_id
        },
        success: function(data) {
            $(field_id).html(data); // replace the contents
        },
        complete: function(data) {
            $(load_path).hide()
        },
    });
}

window.onload = function() {
    var reloading = sessionStorage.getItem("reloading");
    if (reloading) {
        $('#messages-section').html('<div id="message_container" class="alert alert-success" role="alert">Estado da atividade alterado com sucesso</div').delay(5000).fadeOut();
        sessionStorage.removeItem("reloading");

    }
}

/**
 * reloads page loads a message after the reload if messsage exists
 * @param {*} url
 * @param {*} item_id
 * @param {*} status
 * @param {*} local
 */
function change_field_status(url, item_id, status, local) {
    $.ajax({ // initialize an AJAX request
        url: getBaseUrl() + url,
        data: {
            'data_id': item_id,
            'status': status,
            'local': local,
        },
        success: function(data) {
            sessionStorage.setItem("reloading", "true");
            location.reload();
        },
    });
}

/**
 * detects if campus field has changed, if so it will change buildings and location
 */
$(id_forms['campus']).change(function() {
    ajax_request(url_paths['buildings'], $(this).val(), id_forms['buildings'])
    ajax_request(url_paths['location'], $(this).val(), id_forms['location'])
})

/**
 * detects if buildings field has changed, if so it will change location
 */
$(id_forms['buildings']).change(function() {
    ajax_request(url_paths['location'], $(this).val(), id_forms['location'])
})

/**
 * detects if campus field has changed, if so it will change buildings and location
 */
$(id_forms_update['campus']).change(function() {
    ajax_request(url_paths['buildings'], $(this).val(), id_forms_update['buildings'])
    ajax_request(url_paths['location'], $(this).val(), id_forms_update['location'])
})

/**
 * detects if buildings field has changed, if so it will change location
 */
$(id_forms_update['buildings']).change(function() {
    ajax_request_with_load(url_paths['location'], $(this).val(), id_forms_update['location'])
})


/**
 * detects if campus field has changed, if so it will change buildings and location
 * but with will only get the locations that are free
 */
$(forms_coord['campus']).change(function() {
    ajax_request(url_paths['buildings'], $(this).val(), forms_coord['buildings'])
    ajax_request_with_2_fields(url_paths['location_free'], {
        bid: $(this).val(),
        'said': $('#teacher-info-section').attr('name')
    }, forms_coord['location'])
})

/**
 * detects if buildings field has changed, if so it will change location
 * but with will only get the locations that are free
 */
$(forms_coord['buildings']).change(function() {
    ajax_request_with_2_fields(url_paths['location_free'], {
        bid: $(this).val(),
        'said': $('#teacher-info-section').attr('name')
    }, forms_coord['location'])
})


// loads detail in template when click on tr
// i wish i knew what the fuck is this shit tbh
// im too scared to delete and destroy something in the program
// im scared, even tho i believe that this does nothing
$('tr').click(function() {
    let this_tr = $(this);
    let value = $(this).attr('id')
    $(document).ready(function() {
        let this_tr_child = $($(this_tr).next('tr.child'))
        if ($(this_tr_child).length > 0 && $(this_tr_child).find('#activity-day-session-pair').is(':empty')) {
            let load_path = $(this_tr_child).find('#loader')
            ajax_request_with_load(url_paths['session_table'], value, $(this_tr_child).find('#activity-day-session-pair'), load_path)
        }
    })
})


/**
 * validates an activity
 */
function accept_activity() {
    let sa_id = $('#teacher-info-section').attr('name')
    let local = $('#div_location').val()
    if (!local)
        $('#error_location').show().delay(5000).fadeOut();
    else
        change_field_status(url_paths['change_status'], sa_id, 'VÁLIDA', local)
}


/**
 * rejects an ativity
 * @param {*} data_id the id of the activity
 * @param {*} status the status of the activity
 */
function reject_activity(data_id, status) {
    change_field_status(url_paths['change_status'], data_id, status, '')
}

function change_activity_request(url, item_id, text_val) {
    $.ajax({ // initialize an AJAX request
        url: getBaseUrl() + url,
        data: {
            'data_id': item_id,
            'text': text_val,
        },
        success: function(data) {
            sessionStorage.setItem("reloading", "true");
            location.reload();
        },
    });
}

function change_activity() {
    change_activity_request('atividades/ajax/request_change/', $('#changeact').attr('value'), $("#changetext").val())
}

function modal(data_id) {
    $('#changeact').attr('value', data_id)
}

/**
 * get the info for the modal
 * @param {*} url of the info
 * @param {*} item_id fields that require info
 * @param {*} field_id field where the data will be inserted to
 */
function ajax_fill_modal(url, item_id, field_id) {
    $.ajax({ // initialize an AJAX request
        url: getBaseUrl() + url, // set the url of the request
        data: {
            'sa_id': item_id.sa_id, // add the country id to the GET parameters
            'local_desc': item_id.local_desc, // add the country id to the GET parameters
            'location': item_id.location, // add the country id to the GET parameters
        },
        success: function(data) { // `data` is the return of the `load_buildings` view function
            $(field_id).html(data); // replace the contents of the city input with the data that came from the server
        },
    });
}

/**
 * fills the modal with the info necessary to validate the activity
 * @param {*} id activity id
 * @param {*} local_desc description if exists
 * @param {*} location location if exists
 */
function fillmodal(id, local_desc, location) {
    ajax_fill_modal(url_paths['modal_info'], {
                'sa_id': id,
                'local_desc': local_desc,
                'location': location,
            },
            forms_coord['modal_info'])
        // $("#sala_modal").html(location)
}