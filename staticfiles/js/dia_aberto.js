(function (params) {
var message_ele = document.getElementById("message_container");

if (message_ele) {
    setTimeout(function () {
        message_ele.style.display = "none";
    }, 3000);
}

/**
 *
 * @param {*} callback returns function when page is ready (i think)
 */
function onReady(callback) {
    var intervalID = window.setInterval(checkReady, 1000);

    function checkReady() {
        if (document.getElementsByTagName('body')[0] !== undefined) {
            window.clearInterval(intervalID);
            callback.call(this);
        }
    }
}

function show(id, value) {
    const element = document.getElementById(id);
    if (element) {
        element.style.display = value ? 'inline-flex' : 'none';
    }
}

$(document).ready(() => {
        $('.navbar-item.is-tab').each((index, value) => {
            path = window.location.href.replace(window.location.origin, '')
            if (path === $(value).attr('href'))
                $(value).addClass('is-active')
        })
    })
    // $('.navbar-item').click(function(index, value) {
    //     sessionStorage.setItem("active-link", JSON.stringify(index));
    // })



// shows page if its laoded, otherwise it does the loading animation
onReady(function() {
    show('loader', true);
    show('loading', false);
});


})();
