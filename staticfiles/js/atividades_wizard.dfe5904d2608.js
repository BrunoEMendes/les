$(document).ready(function() {

    let step_number = $('.step-number').text()

    // makes progress bar move
    $('#progressbar li').each((i, event) => {
        if (i < step_number)
            $(event).addClass('active')
        console.log(step_number)
    })

    $('.form-step4-onload').hide();

    $('input[type="radio"').click((i, event) => {
            $('.form-step4-onload').show();

            if ($(i)[0].currentTarget.value == 'sala') {
                $('.form-group:not(:last)').show();
                $('.form-group:last').hide();
                $('.form-group:last textarea').val('');
            } else {
                $('.form-group:not(:last)').hide();
                $('.form-group:not(:last) option:selected').prop('selected', false);
                $('.form-group:last').show();
            }
        })
        // add more materials
    $('#addmore').click((event) => {
        let formset = $('fieldset').last().prop('outerHTML')
        var suffix = formset.match(/\d+/)[0];
        let result = formset.split('mform-' + suffix).join('mform-' + (Number(suffix) + 1))

        $(result).appendTo($('#group-fieldset')).hide().show('slow');

        formset = $('fieldset').last().prev()
        formset.append('<a class="button is-outlined is-danger" id="removesession">remover</a></fieldset>')

        let value = $('#id_mform-TOTAL_FORMS').val()
        $('#id_mform-TOTAL_FORMS').val(Number(value) + 1);
    })


    // set inline check forms
    $.each($('.form-check'), (index, value) => {
        $(value).addClass('d-inline-block')
    })

    // add more theme
    $('#id_aform-theme').append('<a id="addmorethemes" class="button d-block is-small mt-2 is-outlined is-link" style="width:fit-content">adicionar tema</a>')

});

// functiont to add a session button
$(document).on('click', '#addmoresession', () => {
    let formset = $('form').find('fieldset').last().prop('outerHTML')
    var suffix = formset.match(/\d+/)[0];
    let result = formset.split('asform-' + suffix).join('asform-' + (Number(suffix) + 1))
    $(result).insertAfter($('form').find('fieldset').last()).hide().show('slow');

    formset = $('#msform').find('fieldset').last().prev()
        // if (result.indexOf('removesession') < 0)
    formset.append('<a class="button mr-3 float-left is-outlined is-danger" id="removesession">remover</a></fieldset>')
    let value = $('#id_asform-TOTAL_FORMS').val()
    if (isNaN(value))
        value = 1;
    $('#id_asform-TOTAL_FORMS').val(Number(value) + 1);
})

// updates on reload
$('fieldset').find('#form-fields:last').each((index, value) => {
    if ($('.form-check-input').length > 0)

        $(value).append('<a class="mr-3 button is-outlined is-link" id="sellectallcheckbox">selecionar todos</a></fieldset>')
    if (index < $('fieldset').find('#form-fields:last').length - 1 && $(value).find('a#removession').length === 0) {
        $(value).append('<a class="button mr-3 float-left is-outlined is-danger" id="removesession">remover</a>')
    }
})

// removes the session
$(document).on('click', '#removesession', (target) => {
    if ($($(target)[0].target).is('a'))
        $($(target)[0].target).parent().remove()
    else
        $($(target)[0].target).parent().parent().remove()

    let value = $('#id_asform-TOTAL_FORMS').val()
    $('#id_asform-TOTAL_FORMS').val(Number(value) - 1);
})


// removes material
$(document).on('click', '#removematerial', (target) => {
    if ($($(target)[0].target).is('a'))
        $($(target)[0].target).parent().remove()
    else
        $($(target)[0].target).parent().parent().remove()

    let value = $('#id_mform-TOTAL_FORMS').val()
    $('#id_mform-TOTAL_FORMS').val(Number(value) - 1);
})

$("#id_aform-duration").change(() => {
    sessionStorage.setItem("duration", $('#id_aform-duration').val());
})

function find_if_value_within_duration(value) {
    var list_of_times = $(value).find('.form-check-input:checked');
    if (list_of_times.length <= 1)
        return null


    var duration = sessionStorage.getItem("duration");
    var time_duration = parseInt(duration.substring(3, 5)) * 60 * 1000
    for (var i = 1; i < list_of_times.length; ++i) {
        var value = $(list_of_times[i]).parent().text().replace(/\s/g, '')
        var value_before = $(list_of_times[i - 1]).parent().text().replace(/\s/g, '')
        var time = new Date('', '', '', value.substring(0, 2), value.substring(3, 5))
        var time_before = new Date('', '', '', value_before.substring(0, 2), value_before.substring(3, 5))
            //console.log(time_duration)
            //console.log('here', time, time_before, value[]), value.substring(2,3))
            //console.log(value.substring(0, 2), value.substring(3, 5))
            //console.log(Math.abs(time_before - time), time_duration)
        if (parseInt(Math.abs(time_before - time)) < time_duration) {
            alert('As sessões têm de ter a pelo menos ' + duration.substring(3, 5) + ' mins de diferença')
            $(list_of_times[i]).prop('checked', false)
        }
    }

}

$('#group-fieldset').change('DOMSubtreeModified', (index, ) => {
    find_if_value_within_duration($(index.target).parent().parent().parent())
})

// remove and add checkboxes
$(document).on('click', '#sellectallcheckbox', (target) => {
    checkbox = $($($(target)[0].target).prev().prev().prev()[0]).find('input:checkbox')
    var duration = sessionStorage.getItem("duration");
    if (!duration)
        duration = "00:05:00"
    var time_duration = parseInt(duration.substring(3, 5)) * 60 * 1000
    var bool = true;
    for (var i = 1; i < checkbox.length; ++i) {
        var value = $(checkbox[i]).parent().text().replace(/\s/g, '')
        var value_before = $(checkbox[i - 1]).parent().text().replace(/\s/g, '')
        var time = new Date('', '', '', value.substring(0, 2), value.substring(3, 5))
        var time_before = new Date('', '', '', value_before.substring(0, 2), value_before.substring(3, 5))
        if ((parseInt(Math.abs(time_before - time)) < time_duration))
            bool = false
    }
    console.log(bool)
    if (bool)
        $($($(target)[0].target).prev().prev().prev()[0]).find('input:checkbox').prop('checked', true)
    else
        alert('As sessões têm de ter a pelo menos ' + duration.substring(3, 5) + ' mins de diferença')
});

$(document).on('click', '#removeallcheckbox', (target) => {
    $($($(target)[0].target).prev().prev().prev().prev()[0]).find('input:checkbox').prop('checked', false)
});