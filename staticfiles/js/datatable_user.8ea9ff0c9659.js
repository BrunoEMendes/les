$(document).ready(function() {

    // collapse group
    var collapsedGroups = {};

    // initiates a datatable
    $('#activitytable').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            search: "_INPUT_",
            searchPlaceholder: "procurar..."
        },
        columnDefs: [{
                "targets": [3],
                "orderable": false
            },
            {
                "width": "30%",
                "targets": [0, 1, 2],
            }
        ],
    });

    var table = $('#activitytable').DataTable();
    // queries
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {

            //status query
            var status = $('#status').val()
            var status_col = data[2]
            if (status_col.toLowerCase().indexOf(status.toLowerCase()) >= 0 || !isNaN(status))
                return true;
            return false;
        }
    );

    // call query
    $('#status').keyup(function() {
        table.draw();
    });
    $('#fromtime').keyup(() => {
        table.draw()
    })
});