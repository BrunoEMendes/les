$(document).ready(() => {

    $.each($('.multiField'), (index, value) => {
        $(value).append('<a class="mr-3 btn btn-outline-info" id="sellectallcheckbox">selecionar todos</a></fieldset>')
    })

    $.each($('.multifield:not(:last)'), (index, value) => {
        $(value).append('<a class="btn mr-3 float-left btn-outline-danger" id="removesession">remover</a></fieldset>')
    })

    $.each($('.form-check'), (index, value) => {
        $(value).addClass('d-inline')
    })


});

$('#addmoresession').click((event) => {
    let formset = $('.multifield').last().prop('outerHTML')

    var suffix = formset.match(/\d+/)[0];
    let result = formset.split('div_id_daysessionpair_set-' + suffix).join('div_id_daysessionpair_set-' + (Number(suffix) + 1))
        // if (result.indexOf('removesession') < 0)
        //     result = result.split('</fieldset>').join('<a class="btn  btn-outline-danger" id="removesession">remover</a></fieldset>')
    $(result).appendTo($('#form-fields')).hide().show('slow');

    formset = $('.multifield').last().prev()

    formset.append('<a class="btn mr-3 float-left btn-outline-danger" id="removesession">remover</a></fieldset>')

    let value = $('#id_daysessionpair_set-TOTAL_FORMS').val()
    console.log(value)
    if (isNaN(value))
        value = 1;
    $('#id_daysessionpair_set-TOTAL_FORMS').val(Number(value) + 1);
    console.log($('#id_daysessionpair_set-TOTAL_FORMS').val())
})

// removes the session
$(document).on('click', '#removesession', (target) => {
    if (window.location.pathname.indexOf('updatematerial') >= 0)
        delete_content(target, '/ajax/delete-material/', '#id_materialactivity_set-TOTAL_FORMS')
    else if (window.location.pathname.indexOf('updatesession') >= 0)
        delete_content(target, '/ajax/delete-session/', '#id_daysessionpair_set-TOTAL_FORMS')

})

function delete_content(target, ajax_url, id_update_form_qtt) {
    if (confirm("Tem a certeza que quer remover?")) {
        var url = ajax_url
        var ds_id = $($(target)[0].target).prev().prev().prev().val(); // get the selected country ID from the HTML input
        $.ajaxSetup({
            url: window.location.hostname
        });
        $.ajax({ // initialize an AJAX request
            url: url, // set the url of the request (= localhost:8000/hr/ajax/load-buildings/)
            data: {
                'ds_id': ds_id // add the country id to the GET parameters
            },
            success: function(data) { // `data` is the return of the `load_buildings` view function
                console.log('deleted :9')
            }
        });
        if ($($(target)[0].target).is('a'))
            $($(target)[0].target).parent().remove()
        else
            $($(target)[0].target).parent().parent().remove()

        let value = $(id_update_form_qtt).val()
        $(id_update_form_qtt).val(Number(value) - 1);
    }
}

$(document).ready(() => {
        if ($('#formlocation').length > 0) {
            $('.form-step4-onload').hide();
            if ($('#id_campus').val() != "") {
                $("input[type=radio]:first").prop('checked', true)
                show_radio('sala')
            } else {
                $("input[type=radio]:last").prop('checked', true)
                show_radio('description')
            }
            $('input[type="radio"').click((i, event) => {
                show_radio($(i)[0].currentTarget.value)
            })
        }
    })
    // codigo redundate fix later
function show_radio(value) {
    $('.form-step4-onload').show();
    if (value == 'sala') {
        $('.form-group:not(:last)').show();
        $('.form-group:last').hide();
        $('.form-group:last textarea').val('');
    } else {
        $('.form-group:not(:last)').hide();
        $('.form-group:not(:last) option:selected').prop('selected', false);
        $('.form-group:last').show();
    }
}


$(document).on('click', '#sellectallcheckbox', (target) => {
    $($($(target)[0].target).prev().prev().prev()[0]).find('input:checkbox').prop('checked', true)
});