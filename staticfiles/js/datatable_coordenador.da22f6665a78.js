$(document).ready(function() {

    // collapse group
    var collapsedGroups = {};

    // initiates a datatable
    $('#tasktable').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            search: "_INPUT_",
            searchPlaceholder: "procurar..."
        },
        columnDefs: [{
                "targets": [4],
                "orderable": false
            },
            {
                "width": "20%",
                "targets": [0, 1, 2, 3],
            }
        ],
    });

    var table = $('#tasktable').DataTable();
    // queries
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {

            //status query
            var status = $('#type').val()
            var status_col = data[2]
            if (status_col.toLowerCase().indexOf(status.toLowerCase()) >= 0 || !isNaN(status))
                return true;
            return false;
        }
    );

    // call query
    $('#tasktable').keyup(function() {
        table.draw();
    });

});