$(document).ready(function() {

    // collapse group
    var collapsedGroups = {};

    // initiates a datatable
    $('#activitytable').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            search: "_INPUT_",
            searchPlaceholder: "procurar..."
        },
        // defines the row group
        rowGroup: {
            // Uses the 'row group' plugin
            dataSrc: 2,
            startRender: function(rows, group) {
                var collapsed = !!collapsedGroups[group];

                rows.nodes().each(function(r) {
                    r.style.display = collapsed ? 'none' : '';
                });

                // Add category name to the <tr>. NOTE: Hardcoded colspan
                return $('<tr/>')
                    .append('<td colspan="8">' + group + ' (' + rows.count() + ')</td>')
                    .attr('data-name', group)
                    .toggleClass('collapsed', collapsed);
            }
        }
    });

    var table = $('#activitytable').DataTable();
    // queries
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {


            //department query
            var dpt = $('#dpt').val()
            var dpt_col = data[2]
            if (dpt_col.toLowerCase().indexOf(dpt.toLowerCase()) >= 0 || !isNaN(dpt))
                return true;
            return false;
        }
    );

    // call query
    $('#dpt').keyup(function() {
        table.draw();
    });

    //collapsed groups
    $('#activitytable tbody').on('click', 'tr.dtrg-start', function() {
        var name = $(this).data('name');
        collapsedGroups[name] = !collapsedGroups[name];
        table.draw(false);
    });


});