$(document).ready(() => {
    if (!window.location.pathname.includes('updatematerial'))
        $.each($('.multiField'), (index, value) => {
            $(value).append('<a class="mr-3 button is-outlined is-link" id="sellectallcheckbox">selecionar todos</a></fieldset>')
        })
    $.each($('.multifield:not(:last)'), (index, value) => {
        if (!window.location.pathname.includes('updatematerial'))
            $(value).append('<a class="button mr-3 float-left is-outlined is-danger" id="removesession">remover</a></fieldset>')
        else
            $(value).append('<a class="button mr-3 float-left is-outlined is-danger" id="removesession">remover</a><br></fieldset>')

    })

    $.each($('.form-check'), (index, value) => {
        $(value).addClass('d-inline')
    })


});

$('#addmoresession').click((event) => {

    //  hard coded , fix later
    if (!window.location.pathname.includes('updatematerial')) {
        let formset = $('.multiField').last().prop('outerHTML')

        var suffix = formset.match(/\d+/)[0];

        let result = formset.split('div_id_daysessionpair_set-' + suffix).join('div_id_daysessionpair_set-' + (Number(suffix) + 1))
            // if (result.indexOf('removesession') < 0)
            //     result = result.split('</fieldset>').join('<a class="btn  btn-outline-danger" id="removesession">remover</a></fieldset>')
        $(result).appendTo($('#form-fields')).hide().show('slow');

        formset = $('.multiField').last().prev()


        formset.append('<a class="button mr-3 float-left is-outlined is-danger" id="removesession">remover</a><br></fieldset>')

        let value = $('#id_daysessionpair_set-TOTAL_FORMS').val()
        if (isNaN(value))
            value = 1;
        $('#id_daysessionpair_set-TOTAL_FORMS').val(Number(value) + 1);
    } else {
        let formset = $('.multiField').last().prop('outerHTML')

        var suffix = formset.match(/\d+/)[0];
        let result = formset.split('materialactivity_set-' + suffix).join('materialactivity_set-' + (Number(suffix) + 1))
            // if (result.indexOf('removesession') < 0)
            //     result = result.split('</fieldset>').join('<a class="btn  btn-outline-danger" id="removesession">remover</a></fieldset>')
        $(result).appendTo($('#form-fields')).hide().show('slow');

        formset = $('.multiField').last().prev()

        formset.append('<a class="button mr-3 float-left is-outlined is-danger" id="removesession">remover</a></fieldset><br>')

        let value = $('#id_materialactivity_set-TOTAL_FORMS').val()
        if (isNaN(value))
            value = 1;
        $('#id_materialactivity_set-TOTAL_FORMS').val(Number(value) + 1);
    }
})

// removes the session
$(document).on('click', '#removesession', (target) => {
    if (window.location.pathname.indexOf('updatematerial') >= 0)
        delete_content_material(target, '/atividades/ajax/delete-material/', '#id_materialactivity_set-TOTAL_FORMS')
    else if (window.location.pathname.indexOf('updatesession') >= 0)
        delete_content_session(target, '/atividades/ajax/delete-session/', '#id_daysessionpair_set-TOTAL_FORMS')

})

function delete_content_material(target, ajax_url, id_update_form_qtt) {
    if (confirm("Tem a certeza que quer remover?")) {
        var url = ajax_url
        var ds_id = $($(target)[0].target).prev().prev().val(); // get the selected ID from the HTML input
        console.log(ds_id)
        $.ajaxSetup({
            url: window.location.hostname
        });
        $.ajax({ // initialize an AJAX request
            url: url, // set the url of the request (= localhost:8000/hr/ajax/load-buildings/)
            data: {
                'ds_id': ds_id // add the country id to the GET parameters
            },
            success: function(data) { // `data` is the return of the `load_buildings` view function
                console.log('deleted :9')
            }
        });
        if ($($(target)[0].target).is('a'))
            $($(target)[0].target).parent().remove()
        else
            $($(target)[0].target).parent().parent().remove()

        // let value = $(id_update_form_qtt).val()
        // $(id_update_form_qtt).val(Number(value) - 1);
    }
}

function delete_content_session(target, ajax_url, id_update_form_qtt) {
    if (confirm("Tem a certeza que quer remover?")) {
        var url = ajax_url
        var ds_id = $($(target)[0].target).prev().prev().prev().val(); // get the selected country ID from the HTML input
        console.log(ds_id)
        $.ajaxSetup({
            url: window.location.hostname
        });
        $.ajax({ // initialize an AJAX request
            url: url, // set the url of the request (= localhost:8000/hr/ajax/load-buildings/)
            data: {
                'ds_id': ds_id // add the country id to the GET parameters
            },
            success: function(data) { // `data` is the return of the `load_buildings` view function
                console.log('deleted :9')
            }
        });
        if ($($(target)[0].target).is('a'))
            $($(target)[0].target).parent().remove()
        else
            $($(target)[0].target).parent().parent().remove()

        // let value = $(id_update_form_qtt).val()
        // $(id_update_form_qtt).val(Number(value) - 1);
    }
}

$(document).ready(() => {
        if ($('#formlocation').length > 0) {
            $('.form-step4-onload').hide();
            if ($('#id_campus').val() != "") {
                $("input[type=radio]:first").prop('checked', true)
                show_radio('sala')
            } else {
                $("input[type=radio]:last").prop('checked', true)
                show_radio('description')
            }
            $('input[type="radio"').click((i, event) => {
                show_radio($(i)[0].currentTarget.value)
            })
        }
    })
    // codigo redundate fix later
function show_radio(value) {
    $('.form-step4-onload').show();
    if (value == 'sala') {
        $('.form-group:not(:last)').show();
        $('.form-group:last').hide();
        $('.form-group:last textarea').val('');
    } else {
        $('.form-group:not(:last)').hide();
        $('.form-group:not(:last) option:selected').prop('selected', false);
        $('.form-group:last').show();
    }
}

function goback() {
    window.history.go(-1);
}
var duration;

$(document).on('click', '#sellectallcheckbox', (target) => {
    if (!duration)
        duration = ajax_request('atividades/ajax/get_duration', window.location.pathname.match(/\d+/)[0])
    checkbox = $($($(target)[0].target).prev().prev().prev()[0]).find('input:checkbox')
        // var duration = sessionStorage.getItem("duration");
    var time_duration = parseInt(duration.substring(3, 5)) * 60 * 1000
    var bool = true;
    for (var i = 1; i < checkbox.length; ++i) {
        var value = $(checkbox[i]).parent().text().replace(/\s/g, '')
        var value_before = $(checkbox[i - 1]).parent().text().replace(/\s/g, '')
        var time = new Date('', '', '', value.substring(0, 2), value.substring(3, 5))
        var time_before = new Date('', '', '', value_before.substring(0, 2), value_before.substring(3, 5))
        if ((parseInt(Math.abs(time_before - time)) < time_duration))
            bool = false
    }
    console.log(bool)
    if (bool)
        $($($(target)[0].target).prev().prev().prev()[0]).find('input:checkbox').prop('checked', true)
    else
        alert('As sessões têm de ter a pelo menos ' + duration.substring(3, 5) + ' mins de diferença')

});


function find_if_value_within_duration(value) {
    var list_of_times = $(value).find('.form-check-input:checked');
    if (list_of_times.length <= 1)
        return null

    if (!duration)
        duration = ajax_request('atividades/ajax/get_duration', window.location.pathname.match(/\d+/)[0])
    var time_duration = parseInt(duration.substring(3, 5)) * 60 * 1000
    for (var i = 1; i < list_of_times.length; ++i) {
        var value = $(list_of_times[i]).parent().text().replace(/\s/g, '')
        var value_before = $(list_of_times[i - 1]).parent().text().replace(/\s/g, '')
        var time = new Date('', '', '', value.substring(0, 2), value.substring(3, 5))
        var time_before = new Date('', '', '', value_before.substring(0, 2), value_before.substring(3, 5))
            //console.log(time_duration)
            //console.log('here', time, time_before, value[]), value.substring(2,3))
            //console.log(value.substring(0, 2), value.substring(3, 5))
            //console.log(Math.abs(time_before - time), time_duration)
        if (parseInt(Math.abs(time_before - time)) < time_duration) {
            alert('As sessões têm de ter a pelo menos ' + duration.substring(3, 5) + ' mins de diferença')
            $(list_of_times[i]).prop('checked', false)
        }
    }

}
$('#group-fieldset').change('DOMSubtreeModified', (index, ) => {
    find_if_value_within_duration($(index.target).parent().parent().parent())
})


function getBaseUrl() {
    var pathArray = location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var url = protocol + '//' + host + '/';
    return url;
}


function ajax_request(url, item_id) {
    var res;
    $.ajax({
        async: false,
        url: getBaseUrl() + url,
        data: {
            'data_id': item_id
        },
        success: function(data) {
            res = data
        },
    });
    return res;
}