setTimeout(function () {

const ntfCountDisplay = document.getElementById('ntfCountDisplay');
const dropdownList = document.getElementById('notificationList');

const ring = new Audio(RING_SRC_PATH);

const loadTime = Date.now();


function updateCountDisplay(count) {
  const parsedCount = Number(count);
  if (parsedCount > 0) {
    ntfCountDisplay.hidden = false;
    ntfCountDisplay.innerText = parsedCount >= 100 ? "!!" : count;
  } else {
    ntfCountDisplay.hidden = true;
    ntfCountDisplay.innerText = "0";
  }
}

function displayNotificationsData(data) {
  dropdownList.innerText = "";
  data.data.forEach(ntf => {
    if (ntf.subject.length >= 38) {
      ntf.subject = ntf.subject.substr(0, 38) + "...";
    }
    if (ntf.sender.length >= 32) {
      ntf.sender = ntf.sender.substr(0, 32) + "...";
    }

    const href = NOTIFICATION_DETAIL_URL_PATH.replace("12345", ntf.id);

    dropdownList.innerHTML += `
      <a href="${href}" data-id="${ntf.id}"
      class="dropdown-item ${ntf.seen ? "" : "unseen" }" href="#">
        <strong>${ntf.subject}</strong><br>${ntf.sender}
      </a>`;
  });
}

function sendDataFetchRequest() {
  const ntfDataFetchRequest = new XMLHttpRequest();
  ntfDataFetchRequest.open('GET', DATA_FETCH_URL_PATH)

  ntfDataFetchRequest.onreadystatechange = function () {
    if (ntfDataFetchRequest.readyState === XMLHttpRequest.DONE
      && ntfDataFetchRequest.status === 200) {
      displayNotificationsData(JSON.parse(ntfDataFetchRequest.responseText));
    }
  };
  ntfDataFetchRequest.send();
}

const ntfRefreshRequest = new XMLHttpRequest();
ntfRefreshRequest.open('GET', GET_COUNT_URL_PATH);

ntfRefreshRequest.onreadystatechange = function () {
  if (ntfRefreshRequest.readyState === XMLHttpRequest.DONE
    && ntfRefreshRequest.status === 200
    && ntfRefreshRequest.responseText !== ntfCountDisplay.innerText) {
    updateCountDisplay(ntfRefreshRequest.responseText);
    sendDataFetchRequest();
    if (Date.now() - loadTime >= 1000) {
      ring.play().catch(function () {
        console.warn("needs user iteraction to play ring");
      });
    }
  }
};

ntfRefreshRequest.send();

// pull request new message count every 5 seconds
setInterval(function () {
  ntfRefreshRequest.open('GET', GET_COUNT_URL_PATH);
  ntfRefreshRequest.send();
}, 5000);

// fetch data when page loads
sendDataFetchRequest();

$('#notificationBtn').on('show.bs.dropdown', function () {
  if (!ntfCountDisplay.hidden) {
    const ntfUpdateRequest = new XMLHttpRequest();
    ntfUpdateRequest.open('GET', UPDATE_LAST_FETCH_URL_PATH);

    ntfUpdateRequest.onreadystatechange = function () {
      if (ntfUpdateRequest.readyState === XMLHttpRequest.DONE) {
        if (ntfUpdateRequest.status === 200) {
          updateCountDisplay(0);
        } else {
          console.error("failed to update notifications", ntfUpdateRequest.status, ntfUpdateRequest.responseText);
        }
      }
    };

    ntfUpdateRequest.send();
  }
});


}, 0);