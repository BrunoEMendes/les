$(document).ready(function() {

    let step_number = $('.step-number').text()

    // makes progress bar move
    $('#progressbar li').each((i, event) => {
        if (i < step_number)
            $(event).addClass('active')
        console.log(step_number)
    })

    $('.form-step4-onload').hide();

    $('input[type="radio"').click((i, event) => {
            $('.form-step4-onload').show();

            if ($(i)[0].currentTarget.value == 'sala') {
                $('.form-group:not(:last)').show();
                $('.form-group:last').hide();
                $('.form-group:last textarea').val('');
            } else {
                $('.form-group:not(:last)').hide();
                $('.form-group:not(:last) option:selected').prop('selected', false);
                $('.form-group:last').show();
            }
        })
        // add more materials
    $('#addmore').click((event) => {
        let formset = $('fieldset').last().prop('outerHTML')
        var suffix = formset.match(/\d+/)[0];
        let result = formset.split('mform-' + suffix).join('mform-' + (Number(suffix) + 1))

        $(result).appendTo($('#group-fieldset')).hide().show('slow');

        formset = $('fieldset').last().prev()
        formset.append('<a class="btn btn-outline-danger" id="removesession">remover</a></fieldset>')

        let value = $('#id_mform-TOTAL_FORMS').val()
        $('#id_mform-TOTAL_FORMS').val(Number(value) + 1);
    })


    // set inline check forms
    $.each($('.form-check'), (index, value) => {
        $(value).addClass('d-inline')
    })

    // add more theme
    $('#id_aform-theme').append('<a id="addmorethemes"><i class="text-success fas fa-plus fa-xs"></i></a>')
});

// functiont to add a session button
$(document).on('click', '#addmoresession', () => {
    let formset = $('form').find('fieldset').last().prop('outerHTML')
    var suffix = formset.match(/\d+/)[0];
    let result = formset.split('asform-' + suffix).join('asform-' + (Number(suffix) + 1))
    $(result).insertAfter($('form').find('fieldset').last()).hide().show('slow');

    formset = $('#msform').find('fieldset').last().prev()
        // if (result.indexOf('removesession') < 0)
    formset.append('<a class="btn mr-3 float-left btn-outline-danger" id="removesession">remover</a></fieldset>')
    let value = $('#id_asform-TOTAL_FORMS').val()
    if (isNaN(value))
        value = 1;
    $('#id_asform-TOTAL_FORMS').val(Number(value) + 1);
})

// updates on reload
$('fieldset').find('#form-fields:last').each((index, value) => {
    if ($('.form-check-input').length > 0)

        $(value).append('<a class="mr-3 btn btn-outline-info" id="sellectallcheckbox">selecionar todos</a></fieldset>')
    if (index < $('fieldset').find('#form-fields:last').length - 1 && $(value).find('a#removession').length === 0) {
        $(value).append('<a class="btn mr-3 float-left btn-outline-danger" id="removesession">remover</a>')
    }
})

// removes the session
$(document).on('click', '#removesession', (target) => {
    if ($($(target)[0].target).is('a'))
        $($(target)[0].target).parent().remove()
    else
        $($(target)[0].target).parent().parent().remove()

    let value = $('#id_asform-TOTAL_FORMS').val()
    $('#id_asform-TOTAL_FORMS').val(Number(value) - 1);
})


// removes material
$(document).on('click', '#removematerial', (target) => {
    if ($($(target)[0].target).is('a'))
        $($(target)[0].target).parent().remove()
    else
        $($(target)[0].target).parent().parent().remove()

    let value = $('#id_mform-TOTAL_FORMS').val()
    $('#id_mform-TOTAL_FORMS').val(Number(value) - 1);
})


// remove and add checkboxes
$(document).on('click', '#sellectallcheckbox', (target) => {
    $($($(target)[0].target).prev().prev().prev()[0]).find('input:checkbox').prop('checked', true)
});

$(document).on('click', '#removeallcheckbox', (target) => {
    $($($(target)[0].target).prev().prev().prev().prev()[0]).find('input:checkbox').prop('checked', false)
});