# Project LES

Bachelor Degree in computer science final project

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

```diff
- IMPORTANT
This Setup was made for windows 10
Python version needs to be 64 bits otherwise you won't be abble to connect it to mysqlclient
Some of this links might be outdated
Some steps might be missing, any questions feel free to ask
```

##### 1. Install [Python 3.8.* (64 bits)](https://www.python.org/downloads/release/python-381/)

##### 2. Check if you can use pip command in your command line, if you can't, then just add C:\Users\<name>\AppData\Roaming\Python\Python<python_version>\Scripts to your environment variables

##### 3. Install [MySQL Workbench](https://www.mysql.com/products/workbench/)(can't remember if this installs MySQL aswell but if it doesnt install it from here [MySQL](https://dev.mysql.com/downloads/installer/))

##### 4. Install [Git](https://git-scm.com/downloads)

##### 5. Install virtualenv
```bash
$ pip install virtualenv
```

##### 5. Create a folder for the project and create a new virtualenv
```bash
$ virtualenv env
```

##### 6. Activate the environment (this command will be used everytime you pretend to work on the project)
```bash
$ .\env\Scripts\activate
```

##### 7. Clone the project

###### 7.1. with https

```
(env)$ git clone https://gitlab.com/BrunoEMendes/les.git
```
###### 7.2. with ssh
7.2.1 generate a ssh key
```
$ ssh-keygen -t ed25519 -C "email@example.com"
```
7.2.2 copy the key from
```
C:\Users\<pcname>\.ssh\id_ed25519.pub
```
7.2.3. go to <strong>settings > ssh</strong> and paste the key

7.2.4 clone the project
```
$ (env)git clone git@gitlab.com:BrunoEMendes/les.git
```
##### 8. Install all the dependecies
```bash
(env)$ cd les
(env)$ pip install -r requirements.txt
```

##### 9. Open MySQL workbench and sep up a new schema with the default settings

</br>

![new schema](/docs/create_a_schema.png)

##### 10. Create a file in ./dia_aberto/sensitive_data.json with your database info
```json
{
    "DB_NAME": "examplename",
    "DB_USER": "exampleuser",
    "DB_PASSWORD": "examplepassword"
}
```

##### 11. Migrate the database
```bash
(env)$ python manage.py migrate
```

##### 12. Check if the changes have been made to your database

</br>

![schema](/docs/schema.png)

##### 13. Create a superuser
```bash
(env)$ python manage.py createsuperuser
```

##### 14. Start the server
```bash
(env)$ python manage.py runserver
```

##### 15. Server will be open on [127.0.0.1:8000](127.0.0.1:8000)

##### 16. Enjoy!


## Deploy to Heroku

Some steps might be missing, please infor me if that's the case

##### 1. Install django-heroku
```bash
(env)$ python manage.py django-heroku
```
##### 2. In your setting.py file
* import django_heroku
* set
```django
ALLOWED_HOSTS = ['YOURHEROKUAPP.herokuapp.com']
```
* add
```django
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
django_heroku.settings(locals())
```

##### 4. Create a ProcFile in the main folder and add

```bash
web: gunicorn YOURPROJECTNAME.wsgi
```

##### 5. Create a file with all the installed apps
```bash
(env)$ pip freeze > requirements.txt
```

##### 6. Collect all the static files
```bash
(env)$ python manage.py collectstatic
```

##### 7. Create an account on heroku and push into Heroku master

##### 8. YOUR APP HAS BEEN DEPLOYED!!!!

### More Comands
##### Migrate the databses on heroku
```bash
(env)$ heroku run python manage.py migrate
```
if you don't have [heroku](https://devcenter.heroku.com/articles/heroku-cli) installed on your cmd please do so

##### Create a Superuser
```bash
(env)$ heroku run python manage.py createsuperuser
```
##### Open your Project shell
```bash
(env)$ heroku run python manage.py shell
```
#### Open Heroku bash
```bash
(env)$ heroku run bash
```

## Database

### 1. makemigrations and migrate
```bash
(env)$ redo_migrations
```

### 2. how to save your data before reset
```bash
(env)$ python manage.py dumpdata --format=json --all > fixtures/dummy_data.json
```
### 3. load your saved data
```bash
(env)$ python manage.py loaddata fixtures/dummy_data.json
```

### 4. reset db and keep the data
```bash
(env)$ redo_migrations.bat
```

//TODO
load fixtures when using /migrate


## Documentation

### Class Diagram

</br>

![dclasses](/docs/dclasses.png)

### Functional Requirements
[functional requirements](/docs/grupo8_ES_2018_20219.xlsx)

## Resources
* [django api](https://www.djangoproject.com/)
* [django models](https://docs.djangoproject.com/en/3.0/topics/db/models/)
* [heroku](https://devcenter.heroku.com/articles/heroku-cl)
* [datatables](https://datatables.net/)

## Acknowledgments
* [Website Design](https://github.com/tiagonuneslx/dia_aberto_frontend)
* [Django tutorial](https://www.youtube.com/watch?v=UmljXZIypDc&list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p)