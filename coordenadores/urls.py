from django.contrib import admin
from django.urls import path

from . import views
	
urlpatterns = [
    #Tasks
    path('tarefas/', views.TaskListView.as_view(), name = 'task'),
    path('tarefas/nova/', views.task_create_view, name = 'task-create'),

    #TaskActivity
    path('tarefas-atividade/<int:pk>/', views.task_activity_detail_view, name='task-activity-detail'),
    path('tarefas-actividade/<int:pk>/update/', views.task_update_view, name='task-activity-update'),
    path('tarefas-atividade/<int:pk>/delete/', views.TaskActivityDeleteView.as_view(), name='task-activity-delete'),

    #TaskTransport
    path('tarefas-transporte/<int:pk>/', views.task_transport_detail_view, name='task-transport-detail'),
    path('tarefas-transporte/<int:pk>/update/', views.task_transport_update_view, name='task-transport-update'),
    path('tarefas-transporte/<int:pk>/delete/', views.TaskTransportDeleteView.as_view(), name='task-transport-delete'),

    #TaskOther
    path('tarefas-outras/<int:pk>/', views.task_other_detail_view, name='task-other-detail'),
    path('tarefas-outras/<int:pk>/update/', views.task_other_update_view, name='task-other-update'),
    path('tarefas-outras/<int:pk>/delete/', views.TaskOtherDeleteView.as_view(), name='task-other-delete'),

    #Ajax
    path('ajax/load-days/', views.load_days, name='ajax_load_days'),  
    path('ajax/load-activities/', views.load_activities, name='ajax_load_activities'), 
    path('ajax/load-destiny/', views.load_destiny, name='ajax_load_destiny'), 
]
