from django.contrib import admin
from .models import TaskActivity, TaskTransport, TaskOther, Task, DaySessionActivity

# Register your models here.

admin.site.register(TaskActivity)
admin.site.register(TaskTransport)
admin.site.register(TaskOther)
admin.site.register(DaySessionActivity)
admin.site.register(Task)
