from datetime import datetime, timedelta

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from atividades.models import DaySessionActivity, SessionActivity, Session, SessionDay, Activity
from config_dia_aberto.models import Config
from inscricao.models import SubscriptionSessionAtivitiy
from users.models import Coordinator, Colaborator


class Task(models.Model):
    class EstadoTarefa(models.TextChoices):
        POR_ATRIBUIR = 'Por atribuir', _('por_atribuir')    # Empty or not fully assigned
        ATRIBUIDA = 'Atribuida', _('atribuida') # Fully assigned

    config = models.ForeignKey(Config, on_delete=models.CASCADE, verbose_name='Dia Aberto')
    title = models.CharField(max_length = 50, verbose_name = 'título')
    description = models.TextField(max_length = 500, verbose_name = 'descrição')
    assign_to = models.ManyToManyField(Colaborator, blank=True, verbose_name = 'colaborador a atribuir')
    created_by = models.ForeignKey(Coordinator, on_delete=models.CASCADE, default=None, blank=None)

    number_of_colaborators = models.IntegerField(
        verbose_name = 'número de colaboradores',
        default = 1,
        validators =
        [
            MaxValueValidator(20),
            MinValueValidator(1),
        ])

    status = models.CharField(
        max_length=15,
        choices=EstadoTarefa.choices,
        default=EstadoTarefa.POR_ATRIBUIR,
    )

    def __str__(self):
        return f'{self.title}|Atribuida ?{self.status}'

    def save(self, *args, **kwargs):
        self.config = Config.objects.first()
        super().save(*args, **kwargs)

    def is_task_activity(self):
        return hasattr(self, 'taskactivity')

    def is_task_transport(self):
        return hasattr(self, 'tasktransport')

    def is_task_other(self):
        return hasattr(self, 'taskother')

    def get_num_colaborators_left(self):
        return self.number_of_colaborators - self.assign_to.count()

    def assign_full(self):
        return self.get_num_colaborators_left() == 0

    def get_derived_task(self):
        if self.is_task_activity():
            return self.taskactivity
        elif self.is_task_transport():
            return self.tasktransport
        elif self.is_task_other():
            return self.taskother
        return self

    class Meta:
        verbose_name_plural = 'Tarefas'


class TaskActivity(Task):
    s_a = models.OneToOneField(DaySessionActivity, on_delete=models.CASCADE, related_name='sessão da atividade+', verbose_name = 'sessão atividade dia')
    def __str__(self):
        return f'{self.title}|{self.s_a}'

    def save(self, *args, **kwargs):
        self.number_of_colaborators = self.s_a.activity.n_colab
        super().save(*args, **kwargs)

    def get_day(self):
        """Returns day of the session activity of TaskActivity."""
        return self.s_a.day

    def get_start_time(self):
        """Returns starting time of the session activity of TaskActivity."""
        return self.s_a.session.start_time

    def get_end_time(self):
        """Returns end time of the session activity of TaskActivity."""
        start_time = self.s_a.session.start_time
        start_time_datetime = datetime(year=1,month=1,day=1,hour=start_time.hour, minute=start_time.minute,second=start_time.second,microsecond=start_time.microsecond,tzinfo=start_time.tzinfo)
        duration = self.s_a.activity.duration
        end_time = start_time_datetime + timedelta(hours=duration.hour, minutes=duration.minute)
        return end_time.time()

    def get_format_end_time(self):
        return self.get_end_time().strftime('%H:%M')

    class Meta:
        verbose_name_plural = 'Tarefas de Atividades'


class TaskTransport(Task):
    day = models.ForeignKey(SessionDay, on_delete = models.CASCADE, verbose_name = 'dia')
    from_activity = models.ForeignKey(DaySessionActivity, on_delete=models.CASCADE, related_name='Task_Transport.from_activity+', verbose_name = 'origem')
    to_activity = models.ForeignKey(DaySessionActivity, on_delete=models.CASCADE, related_name='Task_Transport.to_activity+', verbose_name='destino')
    ssas = models.ManyToManyField(SubscriptionSessionAtivitiy, verbose_name='inscrições')

    def save(self, *args, **kwargs):
        subs_from = self.from_activity.subscriptionsessionativitiy_set.values_list('sub', flat=True)
        subs_to = self.to_activity.subscriptionsessionativitiy_set.values_list('sub', flat=True)
        intersect_set = set(subs_from) & set(subs_to)
        super().save(*args, **kwargs)

        self.ssas.set(self.from_activity.subscriptionsessionativitiy_set.filter(sub__pk__in=intersect_set))  
        super().save(*args, **kwargs)


    def __str__(self):
        return f'Transport from {self.from_activity.activity.name} \
            to {self.to_activity.activity.name}'

    class Meta:
        verbose_name_plural = 'Tarefas de Transporte'
        constraints = [
            models.UniqueConstraint(fields=['from_activity', 'to_activity'], name='unique_route')
        ]

    def get_day(self):
        """Returns day of the origin session activity of TaskTransport."""
        return self.from_activity.day

    def get_start_time(self):
        """Returns starting time of the destiny session activity of TaskTransport."""
        return self.to_activity.session.start_time

    def get_end_time(self):
        """Returns starting time of the destiny session activity of TaskTransport minus 10 minutes."""
        start_time = self.to_activity.session.start_time
        start_time_datetime = datetime(year=1,month=1,day=1,hour=start_time.hour, minute=start_time.minute,second=start_time.second,microsecond=start_time.microsecond,tzinfo=start_time.tzinfo)
        end_time = start_time_datetime - timedelta(minutes=10)
        return end_time.time()

    def get_format_end_time(self):
        return self.get_end_time().strftime('%H:%M')

class TaskOther(Task):
    localization = models.CharField(max_length = 100, verbose_name = 'localização')
    day_other = models.ForeignKey(SessionDay, on_delete = models.CASCADE, verbose_name = 'dia')
    start_time = models.TimeField(verbose_name='hora de início')
    duration = models.IntegerField(
                        verbose_name='duração',
                        default=30, 
                        validators=[
                            MinValueValidator(5),
                            MaxValueValidator(120),
                        ])

    def __str__(self):
        return f'{self.title}'

    def get_day(self):
        """Returns day of TaskOther."""
        return self.day_other

    def get_start_time(self):
        """Returns start time of TaskOther."""
        return self.start_time

    def get_end_time(self):
        """Returns end time of TaskOther."""
        start_time = self.start_time
        start_time_datetime = datetime(year=1,month=1,day=1,hour=start_time.hour, minute=start_time.minute, second=start_time.second,microsecond=start_time.microsecond,tzinfo=start_time.tzinfo)
        duration = self.duration
        end_time = start_time_datetime + timedelta(minutes=duration)
        return end_time.time()

    def get_format_end_time(self):
        return self.get_end_time().strftime('%H:%M')

    class Meta:
        verbose_name_plural = 'Outras Tarefas'
