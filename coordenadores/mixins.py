from utils.mixins import UserTypesTestMixin
from users.models import Coordinator

class CoordinatorTestMixin(UserTypesTestMixin):
    """Makes sure the user accessing the view is a Coordinator."""
    user_types = (Coordinator,)
    

	