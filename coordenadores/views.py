import datetime

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import (
	ListView, 
	DetailView, 
	CreateView, 
	UpdateView, 
	DeleteView
)

from atividades.models import SessionActivity, Session, DaySessionPair, SessionDay, Activity, DaySessionActivity
from config_dia_aberto.models import Config
from inscricao.models import SubscriptionSessionAtivitiy
from users.models import Coordinator
from utils.decorators import user_types_test

from .forms import TaskForm, TaskChooseActivityForm, TaskTransportDayForm, TaskOtherForm, TaskUpdateForm, TaskChooseColaboratorsForm
from .mixins import CoordinatorTestMixin
from .models import Task, TaskActivity, TaskTransport, TaskOther


############################## Task Views ##############################


class TaskListView(CoordinatorTestMixin, ListView):
    """
    Renders list of Task objects. Only Coordinators can access it.
    """
    model = Task
    context_object_name = 'tasks'
    extra_context = {}

    def get_queryset(self):
        """
        Returns tasks that have the same OU as the Coordinator.
        """
        return self.model.objects.filter(created_by__ou=self.request.user.coordinator.ou)

    def get(self, request, *args, **kwargs):
        self.extra_context['start_date'] = Config.objects.first().start_datetime.strftime('%Y-%m-%d')
        self.extra_context['end_date'] = Config.objects.first().end_datetime.strftime('%Y-%m-%d')
        self.extra_context['config'] = Config.objects.first()
        return super().get(request, *args, **kwargs)

@user_types_test(Coordinator)
def task_create_view(request):
    """
    Creates a new instance of TaskActivity, TaskTransport or TaskOther model. Only coordinators can create a Task.
    """
    if Config.objects.first().has_dia_aberto_started(): # Coordinator can only create tasks if dia aberto has not yet started.
        raise PermissionDenied("Não é permitido criar uma tarefa após o começo do Dia Aberto.")

    ou = request.user.coordinator.ou
    task_form = TaskForm(request.POST or None)
    task_choose_activity_form = TaskChooseActivityForm(ou, request.POST or None)
    task_transport_day_form = TaskTransportDayForm(ou, request.POST or None)
    task_other_form = TaskOtherForm(request.POST or None)

    context = {
        'task_form': task_form,
        'task_choose_activity_form' : task_choose_activity_form,
        'task_transport_day_form': task_transport_day_form,
        'task_other_form': task_other_form,
    }

    if task_form.is_valid():
        task_form.instance.created_by = Coordinator.objects.get(user=request.user) # Associates the user with the Task "created_by" attribute.
        if task_form.cleaned_data['task_type'] == TaskForm.TaskType.ACTIVITY and task_choose_activity_form.is_valid(): # TaskActivity creation
            for i in request.POST:
                try:
                    days_list = request.POST.getlist(i)
                    day = SessionDay.objects.get(date=datetime.datetime.strptime(i, '%Y-%m-%d'))
                    activity = Activity.objects.get(pk=request.POST['activity'])
                    for session in days_list:
                        session = Session.objects.get(start_time=session)
                        dsa = DaySessionActivity.objects.get(day=day, session=session, activity=activity)
                        tsk = TaskActivity()
                        tsk.__dict__.update(task_form.instance.__dict__)
                        tsk.s_a = dsa
                        tsk.save()
                except ValueError:
                    pass
            messages.success(request, 'Tarefa criada com sucesso!')
            return redirect('task')
        elif task_form.cleaned_data['task_type'] == TaskForm.TaskType.TRANSPORT and task_transport_day_form.is_valid(): # TaskTransport creation
            from_pk = request.POST.get('from')
            to_pk = request.POST.get('to')
            day_pk = request.POST.get('day')
            dsa_from = DaySessionActivity.objects.get(pk=from_pk)
            dsa_to = DaySessionActivity.objects.get(pk=to_pk)
            day = SessionDay.objects.get(pk=day_pk)
            tsk = TaskTransport()
            tsk.__dict__.update(task_form.instance.__dict__)
            tsk.day = day
            tsk.from_activity = dsa_from
            tsk.to_activity = dsa_to
            tsk.save()
            messages.success(request, 'Tarefa criada com sucesso!')
            return redirect('task')
        elif task_form.cleaned_data['task_type'] == TaskForm.TaskType.OTHER and task_other_form.is_valid(): # TaskOther creation
            actual_colab_num = task_other_form.instance.number_of_colaborators
            task_other_form.instance.__dict__.update(task_form.instance.__dict__)
            task_other_form.instance.number_of_colaborators = actual_colab_num
            task_other_form.save()
            messages.success(request, 'Tarefa criada com sucesso!')
            return redirect('task')
    return render(request, 'coordenadores/task_form.html', context)

@user_types_test(Coordinator)
def load_days(request):
    """Ajax view that loads the available days and sessions when the desired activity is selected on the TaskActivity form."""
    activity_id = request.GET.get('activity')
    days = SessionActivity.objects.get(activity__pk=activity_id).session.all()
    sessions = {}
    for day in days:
        task_activities = TaskActivity.objects.filter(s_a__activity__pk=activity_id, s_a__day=day.day)
        session_pks = set()
        for task_activity in task_activities:
            session_pks.add(task_activity.s_a.session.pk)

        available_sessions = days.get(session_activity__activity__pk=activity_id, day=day.day).session.exclude(pk__in=session_pks)
        if available_sessions:
            sessions[day.day] = available_sessions

        # sessions[day.day] = days.get(session_activity__activity__pk=activity_id, day=day.day).session.all()

    return render(request, 'coordenadores/ajax/days_dropdown_list_options.html', {'sessions': sessions})

@user_types_test(Coordinator)
def load_activities(request):
    """Ajax view that loads the available activity-sessions for transport origin when the desired day is selected on the TaskTransport form."""
    user_id = request.GET.get('user')
    day_id = request.GET.get('day')
    user_ou = Coordinator.objects.get(user__pk=user_id).ou

    dsas = DaySessionActivity.get_dsas_with_subs(organic_unit=user_ou) \
        .filter(day=SessionDay.objects.get(pk=day_id)) \
        .order_by('session__start_time')
    for dsa in dsas:
        destination_dsas = DaySessionActivity.get_dsa_destinations(organic_unit=user_ou, source_dsa=dsa)
        
        task_transports = TaskTransport.objects.filter(from_activity=dsa, to_activity__in=destination_dsas)
        to_activity_pks = set()
        for task_transport in task_transports:
            to_activity_pks.add(task_transport.to_activity.pk)

        destination_dsas = destination_dsas.exclude(pk__in=to_activity_pks)

        if not destination_dsas:
            dsas = dsas.exclude(pk=dsa.pk)

    return render(request, 'coordenadores/ajax/activities_dropdown_list_options.html', {'activities': dsas})

@user_types_test(Coordinator)
def load_destiny(request):
    """Ajax view that loads the available activity-sessions for transport destiny when the origin is selected on the TaskTransport form."""
    user_id = request.GET.get('user')
    session_id = request.GET.get('session')
    user_ou = Coordinator.objects.get(user__pk=user_id).ou
    origin_dsa = DaySessionActivity.objects.get(pk=session_id)

    dsas = DaySessionActivity.get_dsa_destinations(organic_unit=user_ou, source_dsa=origin_dsa) \
        .order_by('session__start_time')

    task_transports = TaskTransport.objects.filter(from_activity=origin_dsa, to_activity__in=dsas)
    to_activity_pks = set()
    for task_transport in task_transports:
        to_activity_pks.add(task_transport.to_activity.pk)

    dsas = dsas.exclude(pk__in=to_activity_pks)

    return render(request, 'coordenadores/ajax/destiny_dropdown_list_options.html', {'sessions': dsas})

@user_types_test(Coordinator)
def task_activity_detail_view(request, pk):
    """Renders the detail of a TaskActivity object. User can also associate Collaborators to the Task object"""
    ou = request.user.coordinator.ou
    task = TaskActivity.objects.get(pk=pk)
    task_choose_colaborators = TaskChooseColaboratorsForm(ou, request.POST or None, instance=task)
    config = Config.objects.first()

    context = {
        'object': task,
        'task_choose_colaborators': task_choose_colaborators,
        'config': config,
    }

    if task_choose_colaborators.is_valid():
        task = task_choose_colaborators.save()
        if task.assign_to.count() == task.number_of_colaborators:
            task.status = task.EstadoTarefa.ATRIBUIDA
        else:
            task.status = task.EstadoTarefa.POR_ATRIBUIR
        task.save()
        messages.success(request, 'Alteração de colaboradores efetuada com sucesso!') # Returns a success message if Collaborators are assigned or removed.
        return redirect('task-activity-detail', pk=pk)

    return render(request, 'coordenadores/taskactivity_detail.html', context)
 
@user_types_test(Coordinator)
def task_transport_detail_view(request, pk):
    """Renders the detail of a TaskTransport object. User can also associate one Collaborator to the Task object"""
    ou = request.user.coordinator.ou
    task = TaskTransport.objects.get(pk=pk)
    task_choose_colaborators = TaskChooseColaboratorsForm(ou, request.POST or None, instance=task)
    config = Config.objects.first()

    context = {
        'object': task,
        'task_choose_colaborators': task_choose_colaborators,
        'config': config,
    }

    if task_choose_colaborators.is_valid():
        task = task_choose_colaborators.save()
        if task.assign_to.count() == task.number_of_colaborators:
            task.status = task.EstadoTarefa.ATRIBUIDA
        else:
            task.status = task.EstadoTarefa.POR_ATRIBUIR
        task.save()
        messages.success(request, 'Alteração de colaboradores efetuada com sucesso!') # Returns a success message if Collaborators are assigned or removed.
        return redirect('task-transport-detail', pk=pk)

    return render(request, 'coordenadores/tasktransport_detail.html', context)

@user_types_test(Coordinator)
def task_other_detail_view(request, pk):
    """Renders the detail of a TaskOther object. Coordinator can also associate Collaborators to the TaskOther object"""
    ou = request.user.coordinator.ou
    task = TaskOther.objects.get(pk=pk)
    task_choose_colaborators = TaskChooseColaboratorsForm(ou, request.POST or None, instance=task)
    config = Config.objects.first()

    context = {
        'object': task,
        'task_choose_colaborators': task_choose_colaborators,
        'config': config,
    }
    
    if task_choose_colaborators.is_valid():
        task = task_choose_colaborators.save()
        if task.assign_to.count() == task.number_of_colaborators:
            task.status = task.EstadoTarefa.ATRIBUIDA
        else:
            task.status = task.EstadoTarefa.POR_ATRIBUIR
        task.save()
        messages.success(request, 'Alteração de colaboradores efetuada com sucesso!') # Returns a success message if Collaborators are assigned or removed.
        return redirect('task-other-detail', pk=pk)

    return render(request, 'coordenadores/taskother_detail.html', context)

@user_types_test(Coordinator)
def task_update_view(request, pk):
    """Update view for updating TaskActivity object. Only coordinators of the same OU can update the object."""
    if Config.objects.first().has_dia_aberto_started(): # Coordinator can only delete TaskActivity if dia aberto has not yet started.
        raise PermissionDenied("Não é permitido alterar a tarefa após o começo do Dia Aberto.")

    task = TaskActivity.objects.get(pk=pk)
    task_update_form = TaskUpdateForm(request.POST or None, instance=task)

    context = {
        'task': task,
        'task_update_form': task_update_form,
    }

    if task_update_form.is_valid():
        created_task = task_update_form.save(commit=False)
        # created_task.assign_to.clear()
        created_task.save()
        messages.success(request, 'Tarefa alterada com sucesso!')
        next_url = request.GET.get('next', '/')
        if next_url != '/':
            return HttpResponseRedirect(next_url)
        return redirect('task')
        
    return render(request, 'coordenadores/task_activity_update_form.html', context)

    def get_sucess_url(self):
        return self.request.GET.get('next', '/')

    def post(self, request, *args, **kwargs):
        """Returns a success message if TaskActivity is successfully updated."""
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)    

@user_types_test(Coordinator)
def task_transport_update_view(request, pk):
    """Update view for updating TaskTransport object. Only coordinators of the same OU can update the object."""
    if Config.objects.first().has_dia_aberto_started(): # Coordinator can only delete TaskTransport if dia aberto has not yet started.
        raise PermissionDenied("Não é permitido alterar a tarefa após o começo do Dia Aberto.")

    task = TaskTransport.objects.get(pk=pk)
    task_update_form = TaskUpdateForm(request.POST or None, instance=task)

    context = {
        'task': task,
        'task_update_form': task_update_form,
    }

    if task_update_form.is_valid():
        created_task = task_update_form.save(commit=False)
        # created_task.assign_to.clear()
        created_task.save()
        messages.success(request, 'Tarefa alterada com sucesso!')
        next_url = request.GET.get('next', '/')
        if next_url != '/':
            return HttpResponseRedirect(next_url)
        return redirect('task')
        
    return render(request, 'coordenadores/task_transport_update_form.html', context)

    def get_sucess_url(self):
        return self.request.GET.get('next', '/')

    def post(self, request, *args, **kwargs):
        """Returns a success message if TaskTransport is successfully updated."""
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)   

@user_types_test(Coordinator)
def task_other_update_view(request, pk):
    """Update view for updating TaskOther object. Only coordinators of the same OU can update the object."""
    if Config.objects.first().has_dia_aberto_started(): # Coordinator can only update TaskOther if dia aberto has not yet started.
        raise PermissionDenied("Não é permitido alterar a tarefa após o começo do Dia Aberto.")

    task = TaskOther.objects.get(pk=pk)
    task_update_form = TaskUpdateForm(request.POST or None, instance=task)
    task_other_update_form = TaskOtherForm(request.POST or None, instance=task)

    context = {
        'task': task,
        'task_update_form': task_update_form,
        'task_other_update_form': task_other_update_form,
    }

    if task_update_form.is_valid():
        created_task = task_update_form.save(commit=False)
        if task_other_update_form.is_valid():
            created_task.assign_to.clear() # If number of collaborators in TaskOther is updated, all Collaborators previously associated are dissociated.
            created_task.save()
            task_other_update_form.save()
            messages.success(request, 'Tarefa alterada com sucesso!')
            next_url = request.GET.get('next', '/')
            if next_url != '/':
                    return HttpResponseRedirect(next_url)
            return redirect('task')

    return render(request, 'coordenadores/task_other_update_form.html', context)

    def get_sucess_url(self):
        return self.request.GET.get('next', '/')

    def post(self, request, *args, **kwargs):
        """Returns a success message if TaskOther is successfully updated."""
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)

class TaskActivityDeleteView(CoordinatorTestMixin, DeleteView):
    """View for deleting TaskActivity object. Only coordinators of the same OU can delete the object."""
    model = TaskActivity
    success_url = '/tarefas'
    success_message = 'Tarefa eliminada com sucesso!'

    def get(self, request, *args, **kwargs):
        if Config.objects.first().has_dia_aberto_started(): # Coordinator can only delete TaskActivity if dia aberto has not yet started.
            raise PermissionDenied("Não é permitido alterar a tarefa após o começo do Dia Aberto.")
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Returns a success message if TaskActivity is successfully deleted."""
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)

class TaskTransportDeleteView(CoordinatorTestMixin, DeleteView):
    """View for deleting TaskTransport object. Only coordinators of the same OU can delete the object."""
    model = TaskTransport
    success_url = '/tarefas'
    success_message = 'Tarefa eliminada com sucesso!'

    def get(self, request, *args, **kwargs): # Coordinator can only delete TaskTransport if dia aberto has not yet started.
        if Config.objects.first().has_dia_aberto_started():
            raise PermissionDenied("Não é permitido alterar a tarefa após o começo do Dia Aberto.")

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Returns a success message if TaskTransport is successfully deleted."""
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)


class TaskOtherDeleteView(CoordinatorTestMixin, DeleteView):
    """View for deleting TaskOther object. Only coordinators of the same OU can delete the object."""
    model = TaskOther
    success_url = '/tarefas'
    success_message = 'Tarefa eliminada com sucesso!'

    def get(self, request, *args, **kwargs):
        if Config.objects.first().has_dia_aberto_started(): # Coordinator can only delete TaskOther if dia aberto has not yet started.
            raise PermissionDenied("Não é permitido alterar a tarefa após o começo do Dia Aberto.")
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Returns a success message if TaskOther is successfully deleted."""
        messages.success(self.request, self.success_message)
        return super().post(request, *args, **kwargs)

