from django import forms
from django.core.exceptions import ValidationError
from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _
from bootstrap_datepicker_plus import DateTimePickerInput
from intl_tel_input.widgets import IntlTelInputWidget

from .models import Task, TaskActivity, TaskTransport, TaskOther 
from atividades.models import Activity, DaySessionActivity, SessionActivity, SessionDay
from users.models import Colaborator
from config_dia_aberto.models import Config

class TaskForm(forms.ModelForm):
    class TaskType(TextChoices):
        ACTIVITY = 'ACTIVITY', _('Tarefa atividade')
        TRANSPORT = 'TRANSPORT', _('Tarefa percurso')
        OTHER = 'OTHER', _('Outra')

    task_type = forms.ChoiceField(choices=TaskType.choices, label='Tipo de tarefa')

    class Meta:
        model = Task
        fields = ['title' , 'description', 'task_type']
    
class TaskUpdateForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description']

class TaskChooseActivityForm(forms.ModelForm):
    class Meta:
        model = DaySessionActivity
        fields = ['activity']

    def __init__(self, organic_unit, *args, **kwargs):
        super().__init__(*args, **kwargs)
        activities = Activity.objects.filter(teacher__department__ou=organic_unit, n_colab__gt=0, status="VÁLIDA") # Activity only appears if the Teacher is from the same OU as the Coordinator, the number of Collaborators is greater than 0 and if it's valid.
        full_activities_pks = set()
        for activity in activities:
            sessions = {}
            days = SessionActivity.objects.get(activity=activity).session.all()
            for day in days:
                task_activities = TaskActivity.objects.filter(s_a__activity=activity, s_a__day=day.day)
                session_pks = set()
                for task_activity in task_activities:
                    session_pks.add(task_activity.s_a.session.pk)

                available_sessions = days.get(session_activity__activity=activity, day=day.day).session.exclude(pk__in=session_pks)
                if available_sessions:
                    sessions[day.day] = available_sessions


            check_full = True
            for day, session_set in sessions.items():
                if session_set:
                    check_full = False

            if check_full:
                full_activities_pks.add(activity.pk)

        activities = activities.exclude(pk__in=full_activities_pks)

        self.fields['activity'].queryset = activities

        # self.fields['activity'].queryset = Activity.objects.filter(teacher__department__ou=organic_unit, n_colab__gt=0, status="VÁLIDA")



class TaskTransportDayForm(forms.ModelForm):
    class Meta:
        model = DaySessionActivity
        fields = ['day']

    def __init__(self, organic_unit, *args, **kwargs):
        super().__init__(*args, **kwargs)
        dsas = DaySessionActivity.get_dsas_with_subs(organic_unit=organic_unit)
        days = dsas.values_list('day', flat=True)

        # TaskTransport.objects.filter(from_activity__in=dsas)

        self.fields['day'].queryset = SessionDay.objects.filter(pk__in=days).order_by('date')


class TaskOtherForm(forms.ModelForm):
    class Meta:
        model = TaskOther
        fields = ['number_of_colaborators','localization', 'day_other', 'start_time', 'duration']
        widgets = {
            'start_time': forms.TextInput(attrs={'autocomplete': 'off', 'style':'width: 35%'}),
        }
        
class TaskChooseColaboratorsForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['assign_to']
        widgets = {
            'assign_to' : forms.CheckboxSelectMultiple()
        }
        labels = {
            'assign_to' : ''
        }

    def __init__(self, organic_unit, *args, **kwargs):
        super().__init__(*args, **kwargs)

        this_task = self.instance.get_derived_task()

        colabs = Colaborator.objects.filter(department__ou=organic_unit, schedule__date=this_task.get_day().date, schedule__start_hour__lt=this_task.get_start_time(), schedule__end_hour__gt=this_task.get_end_time())
        colab_exclude_pks = set()

        for colab in colabs:
            for task in colab.task_set.all():
                derived_task = task.get_derived_task()
                if derived_task.get_day() == this_task.get_day(): 
                    if (this_task.get_start_time() > derived_task.get_start_time() and this_task.get_start_time() < derived_task.get_end_time()) \
                    or (this_task.get_end_time() < derived_task.get_end_time() and this_task.get_end_time() > derived_task.get_start_time()):
                        colab_exclude_pks.add(colab.pk)

        self.fields['assign_to'].queryset = colabs.exclude(pk__in=colab_exclude_pks)


    def clean(self):
        if Config.objects.first().has_dia_aberto_started(): # Coordinator can only assign and remove Collaborators if dia aberto has not yet started.
            raise ValidationError(_('Não é possível atribuir colaboradores após o começo do Dia Aberto.'))

        return super().clean()
        
    def clean_assign_to(self):
        data = self.cleaned_data['assign_to']

        if data.count() > self.instance.number_of_colaborators:
            raise ValidationError(_('Número de colaboradores excedido.'))

        return data
