:: deletes all the migrations related files
ECHO Deleting all migration folders
FOR /d /r . %%d IN ("migrations") DO @IF EXIST "%%d" rd /s /q "%%d"