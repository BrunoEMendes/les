from django.test import TestCase

from django.utils import timezone

import time as py_time

from notificacoes.models import *
from users.models import User

class NotificationTests(TestCase):

    def test_getAllSentByUser(self):
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf1 = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1
        )
        ntf1.save()

        ntf2 = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user2
        )
        ntf2.save()

        # user1 sent only ntf1
        self.assertTrue(ntf1 in Notification.getAllSentByUser(user1))
        self.assertFalse(ntf2 in Notification.getAllSentByUser(user1))

        # user2 sent only ntf2
        self.assertFalse(ntf1 in Notification.getAllSentByUser(user2))
        self.assertTrue(ntf2 in Notification.getAllSentByUser(user2))


    def test_getAllReceivedByUser(self):
        user1 = User(username="user1 (sender)")
        user1.save()
        user2 = User(username="user2 (receiver)")
        user2.save()
        user3 = User(username="user3 (receiver)")
        user3.save()

        ntf1 = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1
        )
        ntf1.save()

        ntf2 = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1
        )
        ntf2.save()

        # add receivers
        rec1 = NotificationReceiver(receiver=user2, notification=ntf1)
        rec1.save()
        rec2 = NotificationReceiver(receiver=user2, notification=ntf2)
        rec2.save()
        rec3 = NotificationReceiver(receiver=user3, notification=ntf1)
        rec3.save()

        # user2 receives the 2 notifications
        self.assertTrue(ntf1 in Notification.getAllReceivedByUser(user2))
        self.assertTrue(ntf2 in Notification.getAllReceivedByUser(user2))

        # user3 only receives ntf1
        self.assertTrue(ntf1 in Notification.getAllReceivedByUser(user3))
        self.assertFalse(ntf2 in Notification.getAllReceivedByUser(user3))


    def test_updateUserLastFetchTime(self):
        """
          Note: contains explicit sleep that causes time delay
        """

        try:
            user1 = User.objects.get(username="user1")
        except User.DoesNotExist:
            user1 = User(username="user1")
            user1.save()

        ntf_user = NotificationUser.objects.get(user=user1)
        if not ntf_user:
            ntf_user = NotificationUser(user=user1)
        ntf_user.lastUpdateFetchTime = timezone.now()
        ntf_user.save()

        py_time.sleep(0.2)
        time1 = timezone.now()

        # time 1 is more recent than the
        # last time so it should updated
        py_time.sleep(0.2)
        Notification.updateUserLastFetchTime(user1, time1)

        py_time.sleep(0.2)
        time2 = timezone.now()

        Notification.updateUserLastFetchTime(user1, time2)

        # last time can't be equal
        with self.assertRaises(ValueError):
            Notification.updateUserLastFetchTime(user1, time2)


        with self.assertRaises(ValueError):
            Notification.updateUserLastFetchTime(user1, time1)


        time3 = timezone.now()

        # time 1
        Notification.updateUserLastFetchTime(user1, time3)

    def test_updateUserLastFetchTimeToNow(self):
        """
          Note: contains explicit sleep that causes time delay
        """
        user1 = User(username="user1")
        user1.save()

        user2 = User(username="user2")
        user2.save()

        py_time.sleep(0.02)
        time1 = timezone.now()

        py_time.sleep(0.02)
        Notification.updateUserLastFetchTimeToNow(user1)

        py_time.sleep(0.02)
        time2 = timezone.now()

        stuff1 = NotificationUser.objects.get(user=user1)
        stuff2 = NotificationUser.objects.get(user=user2)

        # last time fetched by user1 was between time1 and time2
        self.assertGreater(stuff1.lastUpdateFetchTime, time1)
        self.assertLess(stuff1.lastUpdateFetchTime, time2)

        # fetch again for user1
        Notification.updateUserLastFetchTimeToNow(user1)
        stuff1 = NotificationUser.objects.get(user=user1)


        self.assertGreater(stuff1.lastUpdateFetchTime, time1)
        self.assertGreater(stuff1.lastUpdateFetchTime, time2)

        self.assertLess(stuff2.lastUpdateFetchTime, time1)
        self.assertLess(stuff2.lastUpdateFetchTime, time2)


    def test_countUserNewNotifications(self):
        """
            Depends of test_updateUserLastFetchTimeToNow
        """
        user1 = User(username="user1")
        user1.save()

        # user recieves a welcome notification when the acount is created
        self.assertEquals(Notification.countUserNewNotifications(user1), 1)

        # fetch
        Notification.updateUserLastFetchTimeToNow(user1)

        # after fetch there will be no more notifications to receive
        self.assertEquals(Notification.countUserNewNotifications(user1), 0)


        ntf1 = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf1.save()
        receiver1 = NotificationReceiver(notification=ntf1, receiver=user1)
        receiver1.save()

        # expects ntf1 to be counted
        self.assertEquals(Notification.countUserNewNotifications(user1), 1)

        ntf2 = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf2.save()
        receiver2 = NotificationReceiver(notification=ntf2, receiver=user1)
        receiver2.save()

        # now expects both ntf1 and ntf2 to be counted
        self.assertEquals(Notification.countUserNewNotifications(user1), 2)

        # fetch again
        Notification.updateUserLastFetchTimeToNow(user1)

        # both ntf1 and ntf2 are received
        self.assertEquals(Notification.countUserNewNotifications(user1), 0)


    def test_getReceivers(self):
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()
        user3 = User(username="user3")
        user3.save()

        user_x = User(username="user x")
        user_x.save()

        ntf = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()

        # at this point there are no recievers
        self.assertEqual(len(ntf.getReceivers()), 0)

        # add some receivers
        rec1 = NotificationReceiver(receiver=user1, notification=ntf)
        rec1.save()
        rec2 = NotificationReceiver(receiver=user2, notification=ntf)
        rec2.save()
        rec3 = NotificationReceiver(receiver=user3, notification=ntf)
        rec3.save()

        # there are 3 receivers expected
        receivers = ntf.getReceivers()
        self.assertTrue(user2 in receivers)
        self.assertTrue(user3 in receivers)
        self.assertTrue(user1 in receivers)
        self.assertEqual(len(ntf.getReceivers()), 3)

        # user x is not a receiver
        self.assertFalse(user_x in receivers)


    def test_markAsSeen(self):
        # create users
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver = NotificationReceiver(pk=42, notification=ntf, receiver=user1,)
        receiver.save()
        receiver2 = NotificationReceiver(pk=77, notification=ntf, receiver=user2,)
        receiver2.save()

        # mark user1 as seen
        self.assertTrue(ntf.markAsSeen(user1))
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user1).seen)

        # notification still unseen for user2
        self.assertFalse(NotificationReceiver.objects.get(notification=ntf, receiver=user2).seen)

        # mark user1 as seen again, nothing should happen
        self.assertTrue(ntf.markAsSeen(user1))
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user1).seen)

        # mark user2 as seen, now both users have seen the notification
        self.assertTrue(ntf.markAsSeen(user2))
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user1).seen)
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user2).seen)


    def test_isPriorityToUser(self):
        # create users
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf = Notification(
            pk=77,
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver1 = NotificationReceiver(notification=ntf, receiver=user1)
        receiver1.save()
        receiver2 = NotificationReceiver(notification=ntf, receiver=user2)
        receiver2.priority = True
        receiver2.save()


        self.assertFalse(ntf.isPriorityToUser(user1))
        self.assertTrue(ntf.isPriorityToUser(user2))

        # user1 marks ntf as priority
        receiver1.priority = True
        receiver1.save()

        self.assertTrue(ntf.isPriorityToUser(user1))


    def test_setPriority_and_resetPriority(self):
        # create users
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()
        userX = User(username="userX")
        userX.save()

        ntf = Notification(
            pk=77,
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver1 = NotificationReceiver(notification=ntf, receiver=user1)
        receiver1.save()
        receiver2 = NotificationReceiver(notification=ntf, receiver=user2)
        receiver2.priority = True
        receiver2.save()


        self.assertFalse(ntf.isPriorityToUser(user1))
        self.assertTrue(ntf.isPriorityToUser(user2))

        # user1 marks ntf as priority
        self.assertTrue(ntf.setPriority(user1))

        # user2 dismarks nft as priority
        self.assertTrue(ntf.resetPriority(user2))


        # they repeat
        self.assertTrue(ntf.setPriority(user1))
        self.assertTrue(ntf.resetPriority(user2))

        self.assertTrue(ntf.isPriorityToUser(user1))
        self.assertFalse(ntf.isPriorityToUser(user2))

        # userX has nothing to do with ntf
        # so it should return false
        self.assertFalse(ntf.setPriority(userX))
        self.assertFalse(ntf.resetPriority(userX))



    def test_wasDeletedByUser(self):
        # create users
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf = Notification(
            pk=77,
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver1 = NotificationReceiver(notification=ntf, receiver=user1)
        receiver1.save()
        receiver2 = NotificationReceiver(notification=ntf, receiver=user2)
        receiver2.deleted = True
        receiver2.save()


        self.assertFalse(ntf.wasDeletedByUser(user1))
        self.assertTrue(ntf.wasDeletedByUser(user2))

        # user1 deletes ntf
        receiver1.deleted = True
        receiver1.save()

        self.assertTrue(ntf.wasDeletedByUser(user1))

    def test_deleteNtf(self):
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf = Notification(
            pk=77,
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver1 = NotificationReceiver(notification=ntf, receiver=user2)
        receiver1.save()

        # notification exists here
        Notification.objects.get(pk=77)

        # delete now
        ntf.delete()

        # notification exists here
        with self.assertRaises(Notification.DoesNotExist):
            Notification.objects.get(pk=77)

    def test_deleteForUser(self):
        # create users
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver = NotificationReceiver(pk=42, notification=ntf, receiver=user1,)
        receiver.save()
        receiver2 = NotificationReceiver(pk=77, notification=ntf, receiver=user2,)
        receiver2.save()

        # user1 deletes ntf
        self.assertTrue(ntf.deleteForUser(user1))
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user1).deleted)

        # user2 still can be see ntf
        self.assertFalse(NotificationReceiver.objects.get(notification=ntf, receiver=user2).deleted)

        # marzk user1 deletes again, nothing should happen
        self.assertTrue(ntf.deleteForUser(user1))
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user1).deleted)

        # mark user2 deletes ntf, now both users have the notification deleted
        self.assertTrue(ntf.deleteForUser(user2))
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user1).deleted)
        self.assertTrue(NotificationReceiver.objects.get(notification=ntf, receiver=user2).deleted)

    def test_wasSeenByUser(self):
        # create users
        user1 = User(username="user1")
        user1.save()
        user2 = User(username="user2")
        user2.save()

        ntf = Notification(
            pk=77,
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )
        ntf.save()
        receiver1 = NotificationReceiver(notification=ntf, receiver=user1)
        receiver1.save()
        receiver2 = NotificationReceiver(notification=ntf, receiver=user2)
        receiver2.seen = True
        receiver2.save()


        self.assertFalse(ntf.wasSeenByUser(user1))
        self.assertTrue(ntf.wasSeenByUser(user2))

        # mark user1 as seen
        receiver1.seen = True
        receiver1.save()

        self.assertTrue(ntf.wasSeenByUser(user1))


    def test_canSee(self):
        """
          Depends of test_getReceivers
        """
        #sender
        user1 = User(username="user1")
        user1.save()

        # receivers
        user2 = User(username="user2")
        user2.save()
        user3 = User(username="user3")
        user3.save()

        # other user
        user_x = User(username="user x")
        user_x.save()

        ntf = Notification(
            subject="subject",
            content="content",
            sent_time=timezone.now(),
            sender=user1,
        )

        ntf.save()

        # add receivers
        rec2 = NotificationReceiver(receiver=user2, notification=ntf)
        rec2.save()
        rec3 = NotificationReceiver(receiver=user3, notification=ntf)
        rec3.save()


        # all receivers can see message
        self.assertTrue(ntf.canSee(user2))
        self.assertTrue(ntf.canSee(user3))

        # and also the sender
        self.assertTrue(ntf.canSee(user1))

        # but any other user can't!
        self.assertFalse(ntf.canSee(user_x))
