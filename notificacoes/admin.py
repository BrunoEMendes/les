from django.contrib import admin
from .models import *


class NotificationReceiverInline(admin.TabularInline):
    model = NotificationReceiver
    extra = 1

class NotificationAdmin(admin.ModelAdmin):
    inlines = [
      NotificationReceiverInline,
    ]

admin.site.register(Notification, NotificationAdmin)
admin.site.register(NotificationUser)
