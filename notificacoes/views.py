from django.shortcuts import render, redirect, reverse, HttpResponseRedirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils import timezone

# from django.db.models import Q
from django.http import HttpResponse, JsonResponse

# from django.views.generic import (
#     CreateView,
#     UpdateView,
#     DeleteView,
# )

from datetime import (
    datetime,
    timedelta,
)

from .models import *

from users.models import *

@login_required
def inboxView(request):
    return render(request, template_name='notificacoes/notification_list.html')


def markAsSeen(request, **kwargs):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    try:
        ntf = Notification.objects.get(pk=kwargs['pk'])

        if not ntf.canSee(request.user):
            return HttpResponse("forbidden", status=403)

        if ntf.markAsSeen(request.user):
            return HttpResponse("marked as seen", status=200)

        return HttpResponse("something went wrong on the server", status=500)
    except Notification.DoesNotExist:
        return HttpResponse("notification not found", status=404)



def deleteNtf(request, **kwargs):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    try:
        ntf = Notification.objects.get(pk=kwargs['pk'])

        if not ntf.canSee(request.user):
            return HttpResponse("forbidden", status=403)

        # test if is author
        if ntf.sender and ntf.sender.pk == request.user.pk:
            ntf.deleteNtf()
            return HttpResponse("deleted for all", status=200)
        else:
            if ntf.deleteForUser(request.user):
                return HttpResponse("deleted for me", status=200)

        return HttpResponse("something went wrong on the server", status=500)
    except Notification.DoesNotExist:
        return HttpResponse("notification not found", status=404)


def setPriority(request, **kwargs):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    try:
        ntf = Notification.objects.get(pk=kwargs['pk'])

        if not ntf.canSee(request.user):
            return HttpResponse("forbidden", status=403)

        if ntf.setPriority(request.user):
            return HttpResponse("set priority", status=200)

        return HttpResponse("something went wrong on the server", status=500)
    except Notification.DoesNotExist:
        return HttpResponse("notification not found", status=404)

def resetPriority(request, **kwargs):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    try:
        ntf = Notification.objects.get(pk=kwargs['pk'])

        if not ntf.canSee(request.user):
            return HttpResponse("forbidden", status=403)

        if ntf.resetPriority(request.user):
            return HttpResponse("reset priority", status=200)

        return HttpResponse("something went wrong on the server", status=500)
    except Notification.DoesNotExist:
        return HttpResponse("notification not found", status=404)



def getCountNew(request):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    count = Notification.countUserNewNotifications(request.user)

    return HttpResponse(count, status=200)



def getNotificationData(request, pk):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    try:
        ntf = Notification.objects.get(pk=pk)

        if not ntf.canSee(request.user):
            return HttpResponse(status=403)

        ntf_dict = ntf.createNotificationDict()

        if ntf.sender and not ntf.sender.pk == request.user.pk:
            ntf_dict['seen'] = ntf.wasSeenByUser(request.user)

        ntf_dict['priority'] = ntf.isPriorityToUser(request.user)

        return JsonResponse(ntf_dict)

    except Notification.DoesNotExist:
        return HttpResponse("no notification found", status=404)



def getLastNotifications(request):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    all_ntf = Notification.getAllReceivedByUser(request.user)

    data = []
    count_unseen = 0
    for ntf in all_ntf[:5]:
        unseen = ntf.wasSeenByUser(request.user),

        if unseen:
            count_unseen += 1

        if ntf.sender:
            sender_name = ntf.sender.username
        else:
            sender_name = "Sistema"

        data.append({
            "id": ntf.pk,
            "subject": ntf.subject,
            "seen": unseen[0],
            "sender": sender_name,
            "sent_time": ntf.sent_time,
        })


    return JsonResponse({
        "unseenCount": count_unseen,
        "data": data,
    })


def getAllRecievedNotifications(request):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    all_ntf = Notification.getAllReceivedByUser(request.user)

    data = []
    for ntf in all_ntf:
        ntf_dict = ntf.createNotificationDict()

        ntf_dict['seen'] = ntf.wasSeenByUser(request.user)
        ntf_dict['priority'] = ntf.isPriorityToUser(request.user)

        data.append(ntf_dict)


    return JsonResponse({
        "data": data,
    })



def getAllSentNotifications(request):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    all_ntf = Notification.getAllSentByUser(request.user)

    data = []
    for ntf in all_ntf:
        unseen = ntf.wasSeenByUser(request.user),

        ntf_dict = ntf.createNotificationDict()

        ntf_dict['priority'] = ntf.isPriorityToUser(request.user)

        data.append(ntf_dict)


    return JsonResponse({
        "data": data,
    })




def updateLastFetch(request):
    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)


    # TODO: add for custom time/mute
    # time = request.GET.get('time', '')
    # if time:
        # try:

        #     timeInt = int(time)

        #     timeParsed = datetime(year=1970, month=1, day=1) + timedelta(microseconds=timeInt*1000)

        #     Notification.updateUserLastFetchTime(request.user, timeParsed)
        #     return HttpResponse(f"updated to " + timeParsed.__str__(), status=200)
        # except (ValueError, OverflowError):
        #     return HttpResponse("invalid time", status=400)

    Notification.updateUserLastFetchTimeToNow(request.user)
    return HttpResponse("updated", status=200)




from users.models import User
def sendToJesus(request):
    teste = Notification.objects.create(
        subject="Title goes here",
        content="and here the body",
        sent_time=timezone.now(),
        sender=request.user
    )

    NotificationReceiver.objects.create(
        notification=teste,
        receiver=request.user
    )

    return HttpResponse("created", status=200)



def sendMessage(request):
    listUsers = User.objects.all()
    return render(request,
                template_name='notificacoes/sendMessage.html',
                context = {'listaUtilizadores':listUsers}
                )

def sendMessageForm(request, **kwargs):
    listUsers = User.objects.all()
    subject = request.POST["subject"]
    content = request.POST["content"]
    sent_time = timezone.now()
    current_user = request.user
    receiver = request.POST['receiver']
    receiverfinal = User.objects.get(username=receiver)


    notification = Notification(subject=subject, content=content, sent_time=sent_time, sender=current_user)
    notificationReceiver = NotificationReceiver(receiver = receiverfinal, notification = notification)

    notification.save()
    notificationReceiver.save()


    return redirect(reverse('notificacoes:viewInbox') + "?view=sent")

def responseMessage(request, pk):

    return render(request,
                template_name='notificacoes/responseMessage.html',
                context={'Pk':pk}
                )

def responseMessageForm(request, **kwargs):
    pk = request.POST["pk"]
    subject = request.POST["subject"]
    content = request.POST["content"]
    sent_time = timezone.now()
    current_user = request.user
    getNot = Notification.objects.get(id = pk)
    receiver = getNot.sender


    notification = Notification(subject=subject, content=content, sent_time=sent_time, sender=current_user)
    notificationReceiver = NotificationReceiver(receiver = receiver, notification = notification)

    notification.save()
    notificationReceiver.save()


    return redirect(reverse('notificacoes:viewInbox') + "?view=sent")




def notificacoes1(request):

    return render(request,
                template_name='notificacoes/notificacoeslist.html',
                )

def seeAllSended(request):
    utilizador = request.user
    listNotis = Notification.objects.filter(sender = utilizador)

    return render(request,
                 template_name = 'notificacoes/seeSended.html',
                 context = {'listAll' : listNotis})

def delete2020(request, id):
    obj = get_object_or_404(Notification, id=id)

    obj.delete()

    return render(request, "notificacoes/seeSended2.html")