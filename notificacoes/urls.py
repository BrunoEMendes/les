from django.contrib import admin
from django.urls import path

from .views import *

app_name = "notificacoes"
urlpatterns = [
    path('inbox', inboxView, name="viewInbox"),
    path('<int:pk>/markAsSeen', markAsSeen, name='markAsSeen'),
    path('<int:pk>/setPriority', setPriority, name='setPriority'),
    path('<int:pk>/resetPriority', resetPriority, name='resetPriority'),
    path('<int:pk>/delete', deleteNtf, name='deleteNtf'),
    path('sendMessage', sendMessage, name="sendMessage"),
    path('notificacoeslist2', notificacoes1, name='notificacoes'),
    path('countNew', getCountNew, name='getCountNew'),
    path('updateLastFetch', updateLastFetch, name='updateLastFetch'),
    path('getLastNotifications', getLastNotifications, name='getLastNotifications'),
    path('getAllRecievedNotifications', getAllRecievedNotifications, name='getAllRecievedNotifications'),
    path('getAllSentNotifications', getAllSentNotifications, name='getAllSentNotifications'),
    path('getNotificationData/<int:pk>', getNotificationData, name='getNotificationData'),
    path('sendMessageForm', sendMessageForm, name="sendMessageForm"),
    path('responseMessage/<int:pk>', responseMessage, name="responseMessage"),
    path('responseMessageForm', responseMessageForm, name="responseMessageForm"),
    path('Sended', seeAllSended, name='Sended'),
    path('DeleteNotifications/<int:id>', delete2020, name="delete20"),

    path('sendToJesus', sendToJesus, name='sendToJesus'),
]
