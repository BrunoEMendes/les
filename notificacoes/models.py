from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.db import models
from django.db.models.signals import *
from django.dispatch import *

from inscricao.models import *
from atividades.models import *
from config_dia_aberto.models import *
from coordenadores.models import *

# Create your models here.
class Notification(models.Model):
    subject = models.CharField(max_length=64)
    content = models.TextField()
    sent_time = models.DateTimeField(verbose_name="enviado")
    sender = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='sender',
        blank = True,
        null = True
    )

    #TODO (Rafael)
    # not sure if should be "create"
    @staticmethod
    def create(self):
        #create notification
        #create receivers
        #create unseens
        raise NotImplementedError

    # TODO (Rafael ou Andre)
    # return true if sender have permission to
    # send a notification to reciever
    @staticmethod
    def canSendTo(sender, reciever):
        raise NotImplementedError

    @staticmethod
    def getAllSentByUser(user):
        return Notification.objects.filter(sender=user).order_by('-sent_time')

    @staticmethod
    def getAllReceivedByUser(user):
        userNtfIds = NotificationReceiver.objects.filter(receiver=user, deleted=False).values_list('notification_id')
        return Notification.objects.filter(id__in=userNtfIds).order_by('-sent_time')



    @staticmethod
    def updateUserLastFetchTime(user, new_time):
        """
          update last time since user fetched notifications
          new_time must be a datetime later than last time
        """
        ntf_user = NotificationUser.objects.get(user=user)

        if ntf_user.lastUpdateFetchTime >= new_time:
            raise ValueError("new_time must be a datetime later than lastUpdateFetchTime")

        ntf_user.lastUpdateFetchTime = new_time
        ntf_user.save()


    @staticmethod
    def updateUserLastFetchTimeToNow(user):
        """
          update last time since user fetched notifications to now
        """
        ntf_user = NotificationUser.objects.get(user=user)
        ntf_user.lastUpdateFetchTime = timezone.now()
        ntf_user.save()


    @staticmethod
    def countUserNewNotifications(user):
        """
          return the number of new notifications that user have
          since last fetch time util now
        """
        last_recived_time = NotificationUser.objects.get(user=user).lastUpdateFetchTime
        all_user_ntfs = NotificationReceiver.objects.filter(receiver=user)
        count = 0


        for user_ntf in all_user_ntfs:
            if not user_ntf.seen and user_ntf.notification.sent_time > last_recived_time:
                count += 1

        return count



    #TODO (Rafael)
    # create a clone of the notification(pretty much)
    # also check what happens if user don't permission to send to the sender
    def resend(self):
        raise NotImplementedError


    #TODO (Rafael)
    # check what happens for multiple receivers
    def reply(self, content):
        raise NotImplementedError


    #TODO (Andre)
    # forward message to another user
    def forward(self, content):
        raise NotImplementedError


    def getReceivers(self):
        """
          Return list with all users that receive the notification
        """
        return list(map(
            lambda notificationReceiver: notificationReceiver.receiver,
            NotificationReceiver.objects.filter(notification=self)
        ))


    def isPriorityToUser(self, user):
        """
          Return true if this notification is marked
          as priority by user, false otherwise
          Does not test if user have permission to read the notification
        """
        try:
            return NotificationReceiver.objects.get(notification=self, receiver_id=user.pk).priority
        except NotificationReceiver.DoesNotExist:
            return False

    def setPriority(self, user):
        """
          Marks a notification as priority
          Return false if notification does not exist, true otherwise
        """
        try:
            ntf_receiver = NotificationReceiver.objects.get(notification=self, receiver=user)
            if not ntf_receiver:
                return False
        except NotificationReceiver.DoesNotExist:
            return False

        ntf_receiver.priority = True
        ntf_receiver.save()
        return True

    def resetPriority(self, user):
        """
          Dismarks a notification as priority
          Return false if notification does not exist, true otherwise
        """
        try:
            ntf_receiver = NotificationReceiver.objects.get(notification=self, receiver=user)
            if not ntf_receiver:
                return False
        except NotificationReceiver.DoesNotExist:
            return False

        ntf_receiver.priority = False
        ntf_receiver.save()
        return True


    def wasDeletedByUser(self, user):
        """
          Return true if user deleted this notification, false otherwise
        """
        try:
            return NotificationReceiver.objects.get(notification=self, receiver_id=user.pk).deleted
        except NotificationReceiver.DoesNotExist:
            return False

    def deleteNtf(self):
        """
          Deletes this notification for all users
          Does not check for permition
        """
        super().delete()

    def deleteForUser(self, user):
        """
          Return true if user deleted this notification, false otherwise
          Does not check for permition
        """
        ntf_receiver = NotificationReceiver.objects.get(notification=self, receiver=user)
        if not ntf_receiver:
            return False

        ntf_receiver.deleted = True
        ntf_receiver.save()
        return True


    def wasSeenByUser(self, user):
        """
          Return true if user saw notification, false otherwise
        """
        try:
            return NotificationReceiver.objects.get(notification=self, receiver_id=user.pk).seen
        except NotificationReceiver.DoesNotExist:
            return False

    def markAsSeen(self, user):
        """
          Marks a notification as seen
          Return false if notification does not exist, true otherwise
        """
        ntf_receiver = NotificationReceiver.objects.get(notification=self, receiver=user)
        if not ntf_receiver:
            return False

        ntf_receiver.seen = True
        ntf_receiver.save()
        return True


    def canSee(self, user):
        """
          Return true if user have permission to see this notification, false otherwise
        """
        return user in self.getReceivers() or user == self.sender

    #TODO (Andre ou Rafael)
    def edit(self, content):
        raise NotImplementedError


    def createNotificationDict(self):
        receivers = []
        for receiver in self.getReceivers():
            receivers.append({
                "name": receiver.username,
                "id": receiver.pk,
            })

        if self.sender:
            sender = {
                "name": self.sender.username,
                "id": self.sender.pk,
            }
        else:
            sender = {
                "name": "Sistema",
                "id": -1,
            }

        return {
            "id": self.pk,
            "sent_time": self.sent_time,
            "sender": sender,
            "receivers": receivers,
            "subject": self.subject,
            "content": self.content,
        }


    def __str__(self):
        #todo fix reciever
        receiversString = ""
        for user in self.getReceivers():
            receiversString += user.__str__() + " "

        return f'De: {self.sender}| Para: {receiversString}| {self.sent_time.strftime("%d-%b-%Y")}'

    class Meta:
        verbose_name = 'Notificação'
        verbose_name_plural = 'Notificações'



class NotificationUser(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        unique=True,
    )
    lastNotificationTime = models.DateTimeField(default=timezone.now(), null=False)
    lastUpdateFetchTime = models.DateTimeField(default=timezone.now(), null=False)

    def __str__(self):
        return f'NotificationUser {self.user} at {self.lastUpdateFetchTime} > {self.lastNotificationTime}'


class NotificationReceiver(models.Model):
    notification = models.ForeignKey(
        Notification,
        on_delete=models.CASCADE,
    )
    receiver = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='receiver'
    )

    seen = models.BooleanField(default=False)
    priority = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'NotificationReceiver {self.notification.pk} user:{self.receiver} seen:{self.seen} priority:{self.priority} deleted:{self.deleted}'




@receiver(post_save, sender=User)
def initLastUserUpdate(sender, **kwargs):
    """
      init NotificationLastUserUpdate for a the new user created
    """
    try:
        # if no DoesNotExist raised then is login, just ignore
        NotificationUser.objects.get(user=kwargs['instance'])

    # register user
    except NotificationUser.DoesNotExist:
        last_user_update = NotificationUser(user=kwargs['instance'])
        last_user_update.save()




#
#  Send notification when a acc is created
#
@receiver(post_save, sender=User)
def create_welcome_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            teste = Notification.objects.create(subject="Conta criada",
                                        content="A sua conta foi criada com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste,
                                                receiver=kwargs.get('instance'))
    except:
        pass


@receiver(post_save, sender=Subscription)
def create_subscription_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testesubs = Subscription.objects.last()
            teste1 = Notification.objects.create(subject="Inscrição",
                                        content="A sua inscrição foi efetuada com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste1,
                                                receiver=testesubs.participant.user)
    except:
        pass

@receiver(post_save, sender=Activity)
def create_activity_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testeact = Activity.objects.last()
            teste2 = Notification.objects.create(subject="Atividade",
                                        content="A sua atividade foi inserida com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste2,
                                                receiver=testeact.teacher.user)
    except:
        pass



@receiver(post_save, sender=Material)
def create_material_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testeact1 = Material.objects.last()
            testeactivity = Activity.objects.get(id=testeact1.id)
            teste3 = Notification.objects.create(subject="Material",
                                        content="O material foi inserido com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste3,
                                                receiver=testeactivity.teacher.user)
    except:
        pass


@receiver(post_save, sender=MaterialActivity)
def create_materialactivity_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testeact2 = MaterialActivity.objects.last()
            testeactivity = Activity.objects.get(id=testeact2.id)
            teste4 = Notification.objects.create(subject="Material associado à atividade",
                                        content="O material foi associado à atividade com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste4,
                                                receiver=testeactivity.teacher.user)
    except:
        pass

@receiver(post_save, sender=SessionActivity)
def create_sessionactivity_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testeact3 = SessionActivity.objects.last()
            testeactivity = Activity.objects.get(id=testeact3.id)
            teste5 = Notification.objects.create(subject="Sessão de Atividade",
                                        content="A sua sessão foi associada à atividade com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste5,
                                                receiver=testeactivity.teacher.user)
    except:
        pass

@receiver(post_save, sender=Theme)
def create_theme_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testeact4 = Theme.objects.last()
            testeactivity = Activity.objects.get(id=testeact4.id)
            teste6 = Notification.objects.create(subject="Sessão de Atividade",
                                        content="A sua sessão foi associada à atividade com sucesso",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste6,
                                                receiver=testeactivity.teacher.user)
    except:
        pass

@receiver(post_save, sender=Task)
def create_contact_message(sender, **kwargs):
    try:
        if kwargs.get('created', False):
            testenew01 = Task.objects.last()
            testColab = Colaborator.objects.get(id=testenew01.id)
            teste01 = Notification.objects.create(subject="Tarefa",
                                        content="Tarefa inserida com sucesso.",
                                        sent_time= timezone.now(),
                                        sender=None
                                        )

            NotificationReceiver.objects.create(notification=teste01,
                                                receiver=testeColab.user)
    except:
        pass
