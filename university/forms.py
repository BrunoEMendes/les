from django import forms
import datetime

from bootstrap_datepicker_plus import DatePickerInput
from dateutil.relativedelta import relativedelta
from intl_tel_input.widgets import IntlTelInputWidget


from .models import Campus, Building, Location

class LocationForm(forms.Form):
    campus = forms.ModelChoiceField(
        label = u'Campus',
        queryset = Campus.objects.all(),
        required = False,
    )

    building = forms.ModelChoiceField(
        label = u'Edifício',
        queryset = Building.objects.all(),
        required = False,
    )

    location = forms.ModelChoiceField(
        label = u'Sala',
        queryset = Location.objects.all(),
        required = False,
    )

    description = forms.CharField(
        label = u'Descrição',
        widget=forms.Textarea,
        required = False,
    )

class CampusForm(forms.ModelForm):
    class Meta:
        model = Campus
        fields = ['name', 'location', 'phone_number', 'email']
        widgets = {
            'phone_number' : IntlTelInputWidget(preferred_countries = ['pt', 'es', 'gb'], default_code = 'pt'),
        }
