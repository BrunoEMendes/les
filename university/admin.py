from django.contrib import admin
from .models import Campus, OrganicUnit, Building, Department, Location, LocationOther
# Register your models here.
admin.site.register(Campus)
admin.site.register(OrganicUnit)
admin.site.register(Building)
admin.site.register(Department)
admin.site.register(Location)
admin.site.register(LocationOther)


