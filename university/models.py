from django.db import models

# Create your models here.

class Campus(models.Model):
    name = models.CharField(max_length = 32, verbose_name = 'campus')
    location = models.CharField(max_length = 64, verbose_name = 'localização')
    phone_number = models.CharField(max_length = 64, verbose_name = 'contacto', blank = True)
    email = models.EmailField(blank = True, default = None)

    def chained_relation(self):
        return self.building_set.filter(is_present=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'Campus'
        ordering = ['name']

class OrganicUnit(models.Model):
    name = models.CharField(max_length = 128, verbose_name = 'Unidade Orgânica')
    acronym = models.CharField(max_length = 10, verbose_name = 'Acrónimo')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'Unidades Orgânicas'
        ordering = ['name']

class Department(models.Model):
    name = models.CharField(max_length = 128, verbose_name = 'Departamento')
    acronym = models.CharField(max_length = 10, verbose_name = 'Acrónimo')
    ou = models.ForeignKey(OrganicUnit, on_delete = models.CASCADE)

    class Meta:
        verbose_name_plural = 'Departamentos'
        ordering = ['name']

    def __str__(self):
        return f'{self.name}|{self.ou}'


class Building(models.Model):
    number = models.IntegerField(verbose_name = 'número do edifício')
    name = models.CharField(max_length = 32, verbose_name = 'nome do edifício', blank = True)
    campus = models.ForeignKey(Campus, on_delete = models.CASCADE, related_name = 'campus')

    def __str__(self):
        return f'{self.number} {self.name}'

    class Meta:
        verbose_name = 'Edifício'
        ordering = ['campus','number']
        unique_together = ('number', 'campus')



class Location(models.Model):
    floor = models.IntegerField(verbose_name = 'andar')
    sala = models.IntegerField(verbose_name = 'sala')
    identification = models.CharField(max_length = 64, verbose_name = 'identificação')
    description = models.TextField(verbose_name = 'descrição')
    building = models.ForeignKey(Building, on_delete = models.CASCADE)

    def __str__(self):
        return f'{self.floor}.{self.sala}'

    def get_full_location_path(self):
        return f'Sala {self.floor}.{self.sala}, Edifício {self.building.number}, Campus {self.building.campus.name}'

    class Meta:
        verbose_name = 'Localização'
        verbose_name_plural = 'Localizações'
        unique_together = ('floor', 'sala', 'building')
        ordering = ['floor', 'sala']


class LocationOther(models.Model):
    identification = models.CharField(max_length = 64, verbose_name = 'identificação')
    description = models.TextField(verbose_name = 'descrição')

    def __str__(self):
        return f'{self.identification}'

    class Meta:
        verbose_name_plural = 'Outras Localizações'
