from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.exceptions import ValidationError

# Mixin taken from https://stackoverflow.com/questions/2138408/limit-number-of-model-instances-to-be-created-django
class SingleInstanceAdminMixin:
    """Hides the "Add" button when there is already an instance."""
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        return super(SingleInstanceAdminMixin, self).has_add_permission(request)


# Mixin taken from https://stackoverflow.com/questions/2138408/limit-number-of-model-instances-to-be-created-django
class SingleInstanceMixin:
    """Makes sure that no more than one instance of a given model is created."""

    def clean(self):
        model = self.__class__
        if (model.objects.count() > 0 and self.pk != model.objects.get().pk):
            raise ValidationError('Can only create 1 %s instance' % model.__name__)
        super(SingleInstanceMixin, self).clean()


class UserTypesTestMixin(LoginRequiredMixin, UserPassesTestMixin):
    """Makes sure the logged in user accessing the view has the correct user type."""
    user_types = None

    def test_func(self):
        for user_type in self.user_types:
            try:
                user_type.objects.get(user=self.request.user)
            except user_type.DoesNotExist:
                pass
            else:
                break
        else:
            return False
        return True


# Check config_dia_aberto.mixins for a mixin example that uses this mixin

