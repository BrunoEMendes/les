from functools import wraps

from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.exceptions import PermissionDenied


def user_types_test(*args, login_url=None, raise_exception=True):
    """
        Decorator to test if user has a user type with permission to the view.
    """
    def check_user_types(*args, user=None):
        for user_type in args:
            try:
                user_type.objects.get(user=user)
            except user_type.DoesNotExist:
                pass
            else:
                break
        else:
            if raise_exception:
                raise PermissionDenied
            return False
        return True


    actual_decorator = user_passes_test(
        lambda u : u.is_authenticated and check_user_types(user=u, *args), 
        login_url=login_url
    )

    return actual_decorator