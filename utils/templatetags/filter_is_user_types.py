from django import template
from users.models import (
    Participant,
    Teacher,
    Coordinator,
    Colaborator,
    Administrator
)

register = template.Library()

@register.filter(name='is_user_types')
def is_user_types(user, user_types_str):
    """ Template tag to show parts of the template for only certain users """
    if not user.is_authenticated:
        return False

    user_types_cls = {
        "Participant": Participant,
        "Teacher": Teacher,
        "Coordinator": Coordinator,
        "Colaborator": Colaborator,
        "Administrator": Administrator
    }
    user_types = list(map(lambda a: a.strip(), user_types_str.split(',')))

    for user_type in user_types:
        try:
            user_types_cls[user_type].objects.get(user=user)
        except user_types_cls[user_type].DoesNotExist:
            pass
        except KeyError:
            pass
        else:
            break
    else:
        return False
    return True
