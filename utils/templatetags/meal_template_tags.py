from django import template
from config_dia_aberto.models import (
    Menu,
    Meal,
)

register = template.Library()

@register.filter()
def meal_type_to_fa_icon(meal_type):
    all_types = Meal.MealType

    if meal_type == all_types.SOUP:
        return "fas fa-utensil-spoon"
    elif meal_type == all_types.CARNE:
        return "fas fa-drumstick-bite"
    elif meal_type == all_types.PEIXE:
        return "fas fa-fish"
    elif meal_type == all_types.VEGETARIAN:
        return "fas fa-leaf"
    elif meal_type == all_types.SPECIAL:
        return "fas fa-star"
    elif meal_type == all_types.DESSERT:
        return "fas fa-ice-cream"
    elif meal_type == all_types.DRINK:
        return "fas fa-coffee"
    else:
        return ''

@register.filter()
def meal_type_to_label(meal_type):
    all_types = Meal.MealType

    if meal_type == all_types.SOUP:
        return "Sopas"
    elif meal_type == all_types.CARNE:
        return "Pratos de carne"
    elif meal_type == all_types.PEIXE:
        return "Pratos de Peixe"
    elif meal_type == all_types.VEGETARIAN:
        return "Pratos Vegetarianos"
    elif meal_type == all_types.SPECIAL:
        return "Pratos Especiais"
    elif meal_type == all_types.DESSERT:
        return "Sobremesas"
    elif meal_type == all_types.DRINK:
        return "Bebidas"
    else:
        return ''

@register.filter()
def contains_meal_type(meals, meal_type):
    for meal in meals:
        if meal.meal_type == meal_type:
            return True
    return False

