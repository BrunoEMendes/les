from django import template
from atividades.models import (
    DaySessionPair,
)

register = template.Library()

@register.simple_tag
def has_session_activity(id):
    return DaySessionPair.objects.filter(session_activity = id).exists()

