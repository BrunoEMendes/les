from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
	path('listacolaboradores',views.colaboradores,name="listacolaboradores"),
	path('colaborador/<int:pk_test>/',views.viewTarefas,name="colaborador")
]
