from django.shortcuts import render
from django.http import HttpResponseRedirect
from users.models import Colaborator
from university.models import Department, OrganicUnit
from .filters import  ColaboratorFilter
from coordenadores.models import *
from django.shortcuts import Http404

# Create your views here.
def colaboradores(request):
    is_admin=False
    is_coord=False
    list_colaboradores = Colaborator.objects.all()
    tasks=Task.objects.all()
    try:
        is_admin=request.user.administrator
    except:
         pass
    try:
        is_coord=request.user.coordinator
        list_colaboradores=Colaborator.objects.get(department__ou=request.user.coordinator.ou)
    except:
        pass
    
    if is_admin or is_coord:
        myFilter=ColaboratorFilter(request.GET)
        list_colaboradores=myFilter.qs
        return render(request=request,
                template_name='colaboradores/colaboratorlist.html', 
                context = {'list_colaboradores': list_colaboradores,'myFilter':myFilter,'tasks':tasks })
    else:
        raise Http404
def viewTarefas(request,pk_test):
    is_admin=False
    is_coord=False
    is_colab=False
    colaborador=Colaborator.objects.get(id=pk_test)
    try:
        is_admin=request.user.administrator
    except:
         pass
    try:
        is_coord=request.user.coordinator and request.user.coordinator.ou == colaborador.department.ou
    except:
        pass
    try:
        is_colab=request.user.colaborator and request.user.colaborator.id == colaborador.id
    except:
        pass
    if is_admin or is_coord or is_colab:
        return render(request=request,template_name='colaboradores/colaboradortarefas.html', context={'colaborador':colaborador})
    else:
        raise Http404
    
 
