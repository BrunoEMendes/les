import django_filters
from django_filters import DateFilter
from django.db.utils import ProgrammingError
from users.models import *
from coordenadores.models import *
from django.forms import *
from .models import *

def get_schedule_choices():
    try:
        choices = [(schedule.date,schedule.date) for schedule in Schedule_Colaborator.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

def get_department_choices():
    try:
        choices = [(dept.id,dept.name) for dept in Department.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

def get_department_ou_choices():
    try:
        choices = [(dept.id,dept.ou.name) for dept in Department.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

class ColaboratorFilter(Form):
	datas =[('','Dia')] + get_schedule_choices()
	datas_unique = []
	for key, value in datas:
		tuple_var = (key,value)
		if tuple_var not in datas_unique:
			datas_unique.append(tuple_var)
	schedule=ChoiceField(label='Dia',choices=datas_unique, required=False, widget=Select(attrs={'class':'input'}))
	ou=ChoiceField(label='Unidade Orgânica', 
			choices =[('','Unidade Orgânica')] + get_department_ou_choices(),
			required=False,
			widget=Select(attrs={'class':'input'}))
	department=ChoiceField(label='Departamento', 
			choices =[('','Departamento')] + get_department_choices(),
			required=False,
			widget=Select(attrs={'class':'input'}))
	def qs(self):
		#parent = super().qs
		data = self.data.get('schedule')
		if data == '' or data is None:
			data = Colaborator.objects.all()
		else:
			data = Colaborator.objects.filter(schedule__date=data)
		dep = self.data.get('department')
		if dep == '' or dep is None:
			dep = Colaborator.objects.all()
		else:
			dep = Colaborator.objects.filter(department=dep)
		ou=self.data.get('ou')
		if ou =='' or ou is None:
			ou=Colaborator.objects.all()
		else:
			ou=Colaborator.objects.filter(department__ou=ou)
		return data & dep & ou
	#





