from django.test import TestCase
from django.test import TestCase, SimpleTestCase
from colaboradores.views import *
from django.urls import reverse,resolve


class TestUrls(SimpleTestCase):

    def test_consultar_lista_colaboradores(self):
        url = reverse('listacolaboradores')
        self.assertEquals(resolve(url).func, colaboradores)
    def test_consultar_tarefas_colaboradores(self):
        url = reverse('colaborador',args=[1])
        self.assertEquals(resolve(url).func, viewTarefas)