import unittest
from users.models import *
from university.models import *
from coordenadores.models import *
from django.test import TestCase

class TestModels(TestCase):
    def test_Colaborador(self):
        ou1=OrganicUnit.objects.create(name="Faculdade de Ciências e Tecnologias", acronym="FCT")
        dep=Department.objects.create(name="Departamento de Engenharia Eletrónica e Informática",acronym="DEEI",ou=ou1)
        sched=Schedule_Colaborator.objects.create(date='2020-07-01',start_hour='09:00:00',end_hour='18:00:00')
        user1=User.objects.create(username="Richie",password="Teste12345", email="naosei@naosei.com", first_name="Richard", last_name="Turpin")
        colab=Colaborator.objects.create(department=dep,schedule=sched,user=user1)
        self.assertEquals(Colaborator.objects.filter(department=dep,schedule=sched,user=user1,valid=False).exists(),True)
        ou1.delete()
        dep.delete()
        sched.delete()
        user1.delete()
        colab.delete()
       