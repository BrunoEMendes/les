import django_filters
from django_filters import DateFilter
from django.db.utils import ProgrammingError
from users.models import *
from coordenadores.models import *
from django.forms import *
from .models import *
from university.models import Department


def get_schedule_choices():
    try:
        choices = [(schedule.id,schedule.date) for schedule in Schedule_Colaborator.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

def get_department_choices():
    try:
        choices = [(dept.id,dept.name) for dept in Department.objects.all()]
    except ProgrammingError:
        choices = []
    return choices


class ColaboratorFilter(Form):
	dia=ChoiceField(choices=[('','---------')] + get_schedule_choices(), widget=Select(),required=False)
	department=ChoiceField(choices=[('','---------')] + get_department_choices(), widget=Select(),required=False)
	