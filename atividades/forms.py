from datetime import datetime, date, timedelta

from django import forms
from django.forms import formset_factory
from django.forms.widgets import Select
from django.forms.models import inlineformset_factory, modelformset_factory
from django.db.utils import ProgrammingError
from django.urls import reverse_lazy

from django_addanother.widgets import AddAnotherWidgetWrapper
from bootstrap_datepicker_plus import DatePickerInput



from config_dia_aberto.models import Config
from .models import (Activity, SessionActivity, MaterialActivity,
                    Material, Theme, DaySessionPair, SessionDay, ConfigActivity, Session)




class ConfigActivityForm(forms.ModelForm):
    class Meta:
        model = ConfigActivity
        fields = ['start_time', 'end_time', 'session_interval']

    def clean(self):
        cleaned_data = super().clean()
        start_time = cleaned_data.get("start_time")
        end_time = cleaned_data.get("end_time")
        if end_time < start_time:
            msg = u"A hora de término tem de ser superior à hora de iniciação"
            self._errors["end_time"] = self.error_class([msg])

def get_choices_time():
    return [('00:'+str(t),t) for t in range(5, 61, 5)][:-1]

class ActivityForm(forms.ModelForm):

    class Meta:
        model = Activity

        fields = ['name', 'description', 'duration', 'max_participants', 'theme', 'target', 'n_colab']
        widgets = {
            'theme':  forms.CheckboxSelectMultiple(),
            'duration':Select(choices = get_choices_time()),
            'max_participants': forms.NumberInput(attrs={
                'min': 1,
                'max': 120,
            }),
            'n_colab': forms.NumberInput(attrs={
                'min': 0,
                'max': 100,
            })
        }

    def __init__(self, *args, **kwargs):
        super(ActivityForm, self).__init__(*args, **kwargs)
        self.fields['theme'].label = "Tema"

class ThemeForm(forms.ModelForm):
    class Meta:
        model = Theme
        fields = ['name', 'color']

class MaterialSimpleForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['name']

class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['name']

class MaterialSessionForm(forms.ModelForm):
    class Meta:
        model = MaterialActivity
        fields = ['material', 'quantity']



# formset para o material
MaterialFormSetFinal = inlineformset_factory(SessionActivity,
                                            MaterialActivity,
                                            fields = ['material', 'quantity'],
                                            can_delete = False,
                                            extra = 1,
                                            widgets = {
                                            'quantity': forms.NumberInput(attrs = {
                                                'min': 1,
                                                'max': 100,
                                            })
                                            },)

def get_day_choices():
    try:
        choices = [(str(t),t) for t in SessionDay.objects.all()]
    except ProgrammingError:
        choices = []
    return choices



class DaySessionPairForm(forms.ModelForm):
    class Meta:
        model = DaySessionPair
        fields = ['day', 'session']
        fields_required = ['day', 'session']
        widgets = {
            'day': Select(choices = get_day_choices()),
            'session':  forms.CheckboxSelectMultiple(),
        }




def get_choices_session():
    res = []
    try:
        [(t.start_time, t.start_time) for t in Session.objects.all()]
        return res
    except:
        return res

DaySessionPairFormSet = inlineformset_factory(SessionActivity,
                                            DaySessionPair,
                                            fields = ['day','session'],
                                            widgets = {

                                                'session': forms.CheckboxSelectMultiple(),
                                            },
                                            can_delete = False,
                                             extra = 1)



