from utils.mixins import UserTypesTestMixin
from users.models import (Teacher, Coordinator, Administrator,
                        Colaborator, Participant, Participant_Group,
                        Participant_Individual)

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import ConfigActivity
from django.utils import timezone
from django.shortcuts import Http404, HttpResponse
from config_dia_aberto.models import Config
from django.core.exceptions import PermissionDenied

class ActivityUserTestMixin(UserTypesTestMixin):
    user_types = [Teacher]

class ThemeMaterialTestMixing(UserPassesTestMixin):
    def test_func(self):
        try:
            start_time = Config.objects.first().activity_limit_subscription_start_datetime
            end_time = Config.objects.first().activity_limit_subscription_end_datetime
            return start_time < timezone.now() < end_time and self.request.user.teacher and ConfigActivity.objects.first() and self.request.user.teacher
        except:
            return False

class UserCanCreateActivity(UserPassesTestMixin):
  def test_func(self):
        try:
            start_time = Config.objects.first().activity_limit_subscription_start_datetime
            end_time = Config.objects.first().activity_limit_subscription_end_datetime
            return start_time < timezone.now() < end_time and self.request.user.teacher and ConfigActivity.objects.first()
        except:
            return False

class AdminCanChangeConfigActivity(UserPassesTestMixin):
    def test_func(self):
        try:
            config =  ConfigActivity.objects.first()
            return config.config.activity_limit_subscription_start_datetime > timezone.now() and self.request.user.administrator
        except:
            raise PermissionDenied()

class ConfigActivityExists:
    def dispatch(self, request, *args, **kwargs):
        if ConfigActivity.objects.first():
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied()

