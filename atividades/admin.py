from django.contrib import admin

from utils.mixins import SingleInstanceAdminMixin
from .models import Theme, Activity, Session, Material, SessionActivity, MaterialActivity, SessionDay, DaySessionPair, ConfigActivity
# Register your models here.

class ConfigActivityAdmin(SingleInstanceAdminMixin, admin.ModelAdmin):
    model = ConfigActivity

admin.site.register(ConfigActivity, ConfigActivityAdmin)
admin.site.register(Theme)
admin.site.register(Activity)
admin.site.register(Session)
admin.site.register(Material)
admin.site.register(SessionActivity)
admin.site.register(MaterialActivity)
admin.site.register(SessionDay)
admin.site.register(DaySessionPair)
