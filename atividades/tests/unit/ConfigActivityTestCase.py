from django.test import TestCase

from django.contrib.auth.models import User
from atividades.models import ConfigActivity, Session
from users.models import Teacher
from university.models import (OrganicUnit, Department)
from config_dia_aberto.models import Config
import datetime as dt
import pytz


class ConfigActivityTest(TestCase):

    fixtures = ['fixtures/activity/university_data.json','fixtures/activity/sys_users_data.json','fixtures/activity/users_data.json','fixtures/activity/config_data.json','fixtures/activity/activity_data.json']

    # def test_activity_get_author(self):

    def setUp(self):
        self.config = ConfigActivity.objects.first()

    def test_config_121_field(self):
        self.assertEqual(Config.objects.first(), self.config.config)


    def test_end_time_lt_start_time(self):
        self.assertTrue(self.config.start_time < self.config.end_time)

    def test_session_table(self):
        compare_res = []
        result = []
        result.append(Session.objects.first().start_time)
        # duration = datetime.time(0, self.config.session_interval, 0)
        # dt.datetime.combine(dt.date(1,1,1), s.start_time )
        [result.append((dt.datetime.combine(dt.date(1,1,1), s.start_time) + dt.timedelta(minutes = self.config.session_interval)).time()) for s in Session.objects.all()]
        [compare_res.append(s.start_time) for s in Session.objects.all()]
        self.assertEqual(result[:-1], compare_res)

    def test_sub_activity_started(self):
        self.assertTrue(self.config.has_sub_activity_started(dt.datetime(year = 2020, month = 1, day = 1, tzinfo=pytz.UTC)))

    def test_sub_activity_over(self):
        self.assertTrue(self.config.is_sub_activity_over(dt.datetime(year = 2050, month = 1, day = 1, tzinfo=pytz.UTC)))

    def test_update_activity(self):
        self.config.session_interval = 15
        self.config.save()
        self.test_session_table()