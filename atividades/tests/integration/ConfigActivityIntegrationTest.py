import time
import json
from django.test import TestCase

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.action_chains import ActionChains
from django.test import Client
from django.test import LiveServerTestCase
from django.test.utils import override_settings

from atividades.models import ConfigActivity

@override_settings(DEBUG=True)
class ConfigActivityCreateTestCase(LiveServerTestCase):
    fixtures = ['fixtures/activity/university_data.json','fixtures/activity/sys_users_data.json','fixtures/activity/users_data.json','fixtures/activity/config_data.json']

    def setUp(self):
        super().setUp()

        client = Client()
        client.login(username='TestAdmin', password='fdsafdsa') #Native django test client
        cookie = client.cookies['sessionid']

        self.vars = {}
        self.driver = webdriver.Chrome(executable_path='fixtures/webdrivers/chromedriver.exe')
        self.driver.set_window_size(1360, 1000)
        self.driver.implicitly_wait(10)
        self.driver.get(self.live_server_url)  #selenium will set cookie domain based on current page domain
        self.driver.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.driver.refresh() #need to update page for logged in user

    def tearDown(self):
        self.driver.quit()
        super().tearDown()


    def testConfigActivityCreate(self):
        self.driver.get('%s%s' % (self.live_server_url, '/atividades/config/'))

        # .setAttribute('value',"Atividade Dia Aberto")
        btn = self.driver.find_element(By.CSS_SELECTOR, ".is-success")
        self.driver.execute_script("arguments[0].click();", btn)

        self.driver.execute_script('$("#id_start_time").val("09:00")')
        self.driver.execute_script('$("#id_end_time").val("16:00")')
        self.driver.execute_script('$("#id_session_interval").val("30")')

        btn = self.driver.find_element(By.CSS_SELECTOR, ".is-link")
        self.driver.execute_script("arguments[0].click();", btn)

        value = ConfigActivity.objects.first()
        self.assertTrue(value)




@override_settings(DEBUG=True)
class ConfigActivityEditTestCase(LiveServerTestCase):
    fixtures = ['fixtures/activity/university_data.json','fixtures/activity/sys_users_data.json','fixtures/activity/users_data.json','fixtures/activity/config_data.json', 'fixtures/activity/activity_data.json']

    def setUp(self):
        super().setUp()

        client = Client()
        client.login(username='TestAdmin', password='fdsafdsa') #Native django test client
        cookie = client.cookies['sessionid']

        self.vars = {}
        self.driver = webdriver.Chrome(executable_path='fixtures/webdrivers/chromedriver.exe')
        self.driver.set_window_size(1360, 1000)
        self.driver.implicitly_wait(10)
        self.driver.get(self.live_server_url)  #selenium will set cookie domain based on current page domain
        self.driver.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.driver.refresh() #need to update page for logged in user

    def tearDown(self):
        self.driver.quit()
        super().tearDown()


    def testConfigActivityEdit(self):
        self.driver.get('%s%s' % (self.live_server_url, '/atividades/config/'))
        btn = self.driver.find_element(By.CSS_SELECTOR, ".is-link")
        self.driver.execute_script("arguments[0].click();", btn)
        self.driver.execute_script('$("#id_session_interval").val("20")')

        btn = self.driver.find_element(By.CSS_SELECTOR, ".is-link")
        self.driver.execute_script("arguments[0].click();", btn)

        value = ConfigActivity.objects.first()
        print(value.session_interval)
        self.assertEquals(value.session_interval, 20)


@override_settings(DEBUG=True)
class ConfigActivityCantOpenTeacherTestCase(LiveServerTestCase):
    fixtures = ['fixtures/activity/university_data.json','fixtures/activity/sys_users_data.json','fixtures/activity/users_data.json','fixtures/activity/config_data.json', 'fixtures/activity/activity_data.json']

    def setUp(self):
        super().setUp()

        client = Client()
        client.login(username='TestTeacher1', password='fdsafdsa') #Native django test client
        cookie = client.cookies['sessionid']

        self.vars = {}
        self.driver = webdriver.Chrome(executable_path='fixtures/webdrivers/chromedriver.exe')
        self.driver.set_window_size(1360, 1000)
        self.driver.implicitly_wait(10)
        self.driver.get(self.live_server_url)  #selenium will set cookie domain based on current page domain
        self.driver.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.driver.refresh() #need to update page for logged in user

    def tearDown(self):
        self.driver.quit()
        super().tearDown()


    def testUserTeacher(self):
        self.driver.get('%s%s' % (self.live_server_url, '/atividades/config/'))
        response = self.client.get( self.live_server_url)
        res = self.driver.find_element(By.CSS_SELECTOR, "h1").text
        self.assertEqual(res, "403 Forbidden")


@override_settings(DEBUG=True)
class ConfigActivityCantOpenCoordTestCase(LiveServerTestCase):
    fixtures = ['fixtures/activity/university_data.json','fixtures/activity/sys_users_data.json','fixtures/activity/users_data.json','fixtures/activity/config_data.json', 'fixtures/activity/activity_data.json']

    def setUp(self):
        super().setUp()

        client = Client()
        client.login(username='TestCoord', password='fdsafdsa') #Native django test client
        cookie = client.cookies['sessionid']

        self.vars = {}
        self.driver = webdriver.Chrome(executable_path='fixtures/webdrivers/chromedriver.exe')
        self.driver.set_window_size(1360, 1000)
        self.driver.implicitly_wait(10)
        self.driver.get(self.live_server_url)  #selenium will set cookie domain based on current page domain
        self.driver.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.driver.refresh() #need to update page for logged in user

    def tearDown(self):
        self.driver.quit()
        super().tearDown()


    def testUserTeacher(self):
        self.driver.get('%s%s' % (self.live_server_url, '/atividades/config/'))
        response = self.client.get( self.live_server_url)
        res = self.driver.find_element(By.CSS_SELECTOR, "h1").text
        self.assertEqual(res, "403 Forbidden")



# @override_settings(DEBUG=True)
# class ConfigActivityAdminCanSeeTestCase(LiveServerTestCase):
#     fixtures = ['fixtures/activity/university_data.json','fixtures/activity/sys_users_data.json','fixtures/activity/users_data.json','fixtures/activity/config_data.json', 'fixtures/activity/activity_data.json']

#     def setUp(self):
#         super().setUp()

#         client = Client()
#         client.login(username='TestAdmin', password='fdsafdsa') #Native django test client
#         cookie = client.cookies['sessionid']

#         self.vars = {}
#         self.driver = webdriver.Chrome(executable_path='fixtures/webdrivers/chromedriver.exe')
#         self.driver.set_window_size(1360, 1000)
#         self.driver.implicitly_wait(10)
#         self.driver.get(self.live_server_url)  #selenium will set cookie domain based on current page domain
#         self.driver.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
#         self.driver.refresh() #need to update page for logged in user

#     def tearDown(self):
#         self.driver.quit()
#         super().tearDown()


#     def testUserAdmin(self):
#         self.driver.get('%s%s' % (self.live_server_url, '/atividades/config/'))
#         response = self.client.get( self.live_server_url)
#         self.assertEqual(response.status_code, 200)
