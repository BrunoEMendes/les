#django imports
from django.shortcuts import (render,
                            redirect,
                            reverse,
                            HttpResponseRedirect,
                            HttpResponse,
                            Http404)

from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.db.models.signals import post_save
from django.core import serializers
from django.views.generic import (ListView,
                                UpdateView,
                                DetailView,
                                CreateView,
                                DeleteView,
                                TemplateView)
from django.forms.models import inlineformset_factory

# formtools
from formtools.wizard.views import SessionWizardView

# mixins
#django mixins
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

#custom mixins
from .mixins import (ActivityUserTestMixin,
                    UserCanCreateActivity,
                    ConfigActivityExists,
                    ThemeMaterialTestMixing,
                    AdminCanChangeConfigActivity)
from config_dia_aberto.mixins import ConfigDiaAbertoExists, AdministratorTestMixin

# models
from users.models import (Teacher,
                        Coordinator,
                        Colaborator,
                        Administrator,
                        Participant_Group,
                        Participant_Individual)

from university.models import (Department,
                            Building,
                            Campus,
                            Location,
                            OrganicUnit)

from config_dia_aberto.models import Config
from .models import (Activity,
                    Session,
                    SessionActivity,
                    MaterialActivity,
                    Material,
                    SessionDay,
                    Theme,
                    DaySessionPair,
                    ConfigActivity)

# forms
from university.forms import LocationForm
from .forms import (ActivityForm,
                    MaterialFormSetFinal,
                    MaterialForm,
                    MaterialSessionForm,
                    ThemeForm,
                    MaterialSimpleForm,
                    DaySessionPairForm,
                    DaySessionPairFormSet,
                    ConfigActivityForm )


#form array for session wizard
FORMS = [('aform', ActivityForm),
        ('asform', DaySessionPairFormSet),
        ('mform', MaterialFormSetFinal),
        ('lform', LocationForm)]


#############################################################
#################  Configurações de Atividade ###############
#############################################################


class ConfigActivityView(AdministratorTestMixin, ListView):
    """
    Creates a View with the details of the ConfigActivity model. \r\n

    **Models** \r\n
        - :model:`atividades.ConfigActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/config_activity/config_activity.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Administrator`
    """
    template_name = 'atividades/config_activity/config_activity.html'
    context_object_name = 'config'

    def get_queryset(self, **kwargs):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return ConfigActivity.objects.first()


class ConfigActivityCreateView(AdministratorTestMixin , CreateView):
    """
    Creates a new instance of ConfigActivity model. \r\n

    **Models** \r\n
        - :model:`atividades.ConfigActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/config_activity/config_activity_form.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Administrator`
    """

    template_name = 'atividades/config_activity/config_activity_form.html'
    form_class = ConfigActivityForm
    success_message = 'Configuração de atividades criada!'

    def get_success_url(self, **kwargs):
        """
        Returns the success url
        """
        return reverse('activity:config-activity')

    def form_valid(self, form):
        """
        if form is valid_then the config field will be the only :model:`config_dia_aberto.Config` object and then returns this new form
        """
        result = form.save(commit = False)
        result.config = Config.objects.first()
        result.save()
        messages.success(self.request, self.success_message)
        return super(ConfigActivityCreateView, self).form_valid(result)

    def dispatch(self, request, *args, **kwargs):
        if not Config.objects.first():
            return redirect('config_dia_aberto:config')
        else:
            return super().dispatch(request, *args, **kwargs)

    def get_queryset(self, **kwargs):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return ConfigActivity.objects.first()

class ConfigActivityUpdateView(AdminCanChangeConfigActivity, SuccessMessageMixin, UpdateView):
    """
    Updates the only ConfigActivity model instance. \r\n

    **Models** \r\n
        - :model:`atividades.ConfigActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/config_activity/config_activity_form_update.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Administrator`\r\n
        - an ConfigActivity object needs to exist
    """


    template_name = 'atividades/config_activity/config_activity_form_update.html'
    success_message = 'Configuração de atividades atualizada!'
    form_class = ConfigActivityForm

    def get_success_url(self, **kwargs):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return reverse('activity:config-activity')

    def get_object(self):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return ConfigActivity.objects.first()


#############################################################
#################  Temas ####################################
#############################################################


class ThemeListView(ListView):
    """
    Shows all the themes for Theme model. \r\n

    **Models** \r\n
        - :model:`atividades.Theme` \r\n

    **Templates** \r\n
        - :template:`atividades/theme/theme_list.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
    """
    model = Theme
    template_name = 'atividades/theme/theme_list.html'
    context_object_name = 'themes'

    def get_context_data(self, **kwargs):
        context = super(ThemeListView, self).get_context_data(**kwargs)
        context['can_change'] = Config.objects.first().is_within_activity_datetime_limit()
        return context



class ThemeDetailView(ThemeMaterialTestMixing, DetailView):
    """
    Shows the detail of a theme. \r\n

    **Models** \r\n
        - :model:`atividades.Theme` \r\n

    **Templates** \r\n
        - :template:`atividades/theme/theme_detail.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher`\r\n
    """
    model = Theme
    template_name = 'atividades/theme/theme_detail.html'


class ThemeUpdateView(ThemeMaterialTestMixing, SuccessMessageMixin, UpdateView):
    """
    Updates an instance of model Theme. \r\n

    **Models** \r\n
        - :model:`atividades.Theme` \r\n

    **Templates** \r\n
        - :template:`atividades/theme/theme_update.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Theme
    form_class = ThemeForm
    template_name = 'atividades/theme/theme_update.html'
    success_message = 'Tema atualizado!'

    def get_success_url(self, **kwargs):
        """
        Returns the url when update is successeful
        """
        return reverse("activity:theme-list")


class ThemeDeleteView(ThemeMaterialTestMixing, DeleteView):
    """
    Deletes an instance of model Theme. \r\n

    **Models** \r\n
        - :model:`atividades.Theme` \r\n

    **Templates** \r\n
        - :template:`atividades/theme/theme_update.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Theme
    template_name = 'atividades/theme/theme_delete.html'
    success_message = 'Tema apagado com sucesso!'

    def get_success_url(self, **kwargs):
        """
        Returns the url when update is successeful
        """
        return reverse("activity:theme-list")

    def delete(self, request, *args, **kwargs):
        """
        now returns success message when a theme is deleted
        """
        messages.success(self.request, self.success_message)
        return super(ThemeDeleteView, self).delete(request, *args, **kwargs)


class ThemeCreateView(ThemeMaterialTestMixing, SuccessMessageMixin, CreateView):
    """
    Creates an instance of model Theme. \r\n
    This one works only for ajax request, cause returns a javascript window close.\r\n
    Not proud of this, but whatever :) \r\n
    Forgot to mention, its used as a POP UP window \r\n
    **Models** \r\n
        - :model:`atividades.Theme` \r\n

    **Templates** \r\n
        - :template:`atividades/theme/theme_form.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Theme
    form_class = ThemeForm
    template_name = 'atividades/theme/theme_form.html'
    success_message = 'Tema criado com sucesso!'

    def post(self, request, **kwargs):
        """
        if the form is valid returns a javascript window close,
        otherwise it comes back to the same page
        """
        tform = ThemeForm(self.request.POST)
        if tform.is_valid():
            tform.save()
            messages.success(self.request, self.success_message)
            return HttpResponse('<script type="text/javascript">window.close(); window.parent.location.href = "/";</script>')
        else:
            return redirect('activity:theme-create')


class ThemeCreateView2(ThemeMaterialTestMixing, SuccessMessageMixin, CreateView):
    """
    Creates an instance of model Theme. \r\n
    **Models** \r\n
        - :model:`atividades.Theme` \r\n

    **Templates** \r\n
        - :template:`atividades/theme/theme_update.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Theme
    form_class = ThemeForm
    template_name = 'atividades/theme/theme_form.html'
    success_message = 'Tema criado com sucesso!'

    def get_success_url(self, **kwargs):
        """
        Returns the url when create is successeful
        """
        return reverse("activity:theme-list")

#############################################################
#################  Material #################################
#############################################################
class MaterialListView(ListView):
    """
    Shows all the themes for Material model. \r\n

    **Models** \r\n
        - :model:`atividades.Material` \r\n

    **Templates** \r\n
        - :template:`atividades/material/material_list.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
    """

    model = Material
    template_name = 'atividades/material/material_list.html'
    context_object_name = 'material'

    def get_context_data(self, **kwargs):
        context = super(MaterialListView, self).get_context_data(**kwargs)
        context['can_change'] = Config.objects.first().is_within_activity_datetime_limit()
        return context


class MaterialDeleteView(ThemeMaterialTestMixing, DeleteView):
    """
    Deletes an instance of model Material. \r\n

    **Models** \r\n
        - :model:`atividades.Material` \r\n

    **Templates** \r\n
        - :template:`atividades/material/material_update.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Material
    template_name = 'atividades/material/material_delete.html'
    success_message = 'Material apagado com sucesso!'

    def get_success_url(self, **kwargs):
        """
        Returns the url when update is successeful
        """
        return reverse("activity:material-list")

    def delete(self, request, *args, **kwargs):
        """
        now returns success message when a material is deleted
        """
        return super(MaterialDeleteView, self).delete(request, *args, **kwargs)


class MaterialUpdateView(ThemeMaterialTestMixing, SuccessMessageMixin, UpdateView):
    """
    Updates an instance of model Material. \r\n

    **Models** \r\n
        - :model:`atividades.Material` \r\n

    **Templates** \r\n
        - :template:`atividades/material/material.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Material
    form_class = MaterialForm
    template_name = 'atividades/material/material_update.html'
    success_message = 'Material atualizado!'

    def get_success_url(self, **kwargs):
        """
        Returns the url when update is successeful
        """
        return reverse("activity:material-list")



class MaterialCreateView2(ThemeMaterialTestMixing, SuccessMessageMixin, CreateView):
    """
    Creates an instance of model Material. \r\n
    **Models** \r\n
        - :model:`atividades.Material` \r\n

    **Templates** \r\n
        - :template:`atividades/material/material_form.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    model = Material
    form_class = MaterialSimpleForm
    template_name = 'atividades/material/material_form.html'
    success_message = 'Material criado com sucesso!'

    def get_success_url(self, **kwargs):
        """
        Returns the url when create is successeful
        """
        return reverse("activity:material-list")



class MaterialCreateView(ThemeMaterialTestMixing, SuccessMessageMixin, CreateView):
    """
    Creates an instance of model Material. \r\n
    This one works only for ajax request, cause returns a javascript window close.\r\n
    Not proud of this, but whatever :) \r\n
    Forgot to mention, its used as a POP UP window \r\n
    **Models** \r\n
        - :model:`atividades.Material` \r\n

    **Templates** \r\n
        - :template:`atividades/material/material_form.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` or :model:`users.Coordinator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """

    model = Material
    form_class = MaterialSimpleForm
    template_name = 'atividades/material/material_form.html'
    success_message = 'Material criado com sucesso!'

    def post(self, request, **kwargs):
        """
        if the form is valid returns a javascript window close,
        otherwise it comes back to the same page
        """
        tform = MaterialForm(self.request.POST)
        if tform.is_valid():
            tform.save()
            messages.success(self.request, self.success_message)
            return HttpResponse('<script type="text/javascript">window.close(); window.parent.location.href = "/";</script>')
        else:
            return redirect('activity:material-create')



#############################################################
#################  Activity #################################
#############################################################

# some functions for user types
def isCoordinator(request):
    return Coordinator.objects.filter(user = request.user).count() > 0

def isTeacher(request):
    return Teacher.objects.filter(user = request.user).count() > 0

def isAdministrator(request):
    return Administrator.objects.filter(user = request.user).count() > 0

# returns the users with highest priority
def get_priority_user(request):
    priority_array = [
        Administrator,
        Coordinator,
        Teacher,
        Colaborator,
    ]

    result = []

    [result.append(x) for x in priority_array if x.objects.filter(user = request.user).count() > 0 ]
    if result:
        return result[0].__name__
    else:
        return

class ActivityListView(TemplateView):
    """
    Lists a view for any user whichs alows them to see activities
    and act over them depending on the kind of user.\r\n

    ``Administrator``
        - can see all the valid/pending/invalid activities\n
        - can accept or deny any activity\n
    ``Coordinator``
        - can see all the valid/pending/invalid activities from its Organic Unit\n
        - can accept or reject any activity from its Organic unit\n
    ``Teacher``
        - can see all the valid/pending/invalid/unfinished activities that he proposed
        - can propose activties
        - can delete its own activities
        - can update its own activities
    ``Everyone``
        - can see all the valid activities

    **Models** \r\n
        - :model:`atividades.Theme` \r\n
        - :model:`atividades.DaySessionPair` \r\n
        - :model:`atividades.Session` \r\n
        - :model:`atividades.SessionDay` \r\n
        - :model:`atividades.Config` \r\n
        - :model:`atividades.Campus` \r\n
        - :model:`atividades.SessionActivity` \r\n
        - :model:`atividades.OrganicUnit` \r\n
        - :model:`atividades.Teacher` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_not_started.html`.
        - :template:`atividades/activity/activity_list_all.html`.
        - :template:`atividades/activity/activity_list_teacher.html`.
        - :template:`atividades/activity/activity_list_coord.html`.
        - :template:`atividades/activity/activity_list_administrator.html`.
    \r\n

    **Requires**\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config` to create an activity
        - must exists a Config and a ConfigActivity for any of this to be available
    """
    # template_name = 'atividades/activity/activity_list_teacher.html'

    def get_template_names(self, **kwargs):
        # print(Config.objects.first().has_activity_sub_started())
        if not self.request.user.is_authenticated:
            raise Http404

        if not Config.objects.first():
            return 'atividades/activity/activity_not_started.html'
        if not Config.objects.first().has_activity_sub_started():
            return 'atividades/activity/activity_not_started.html'
        try:
            template_pk = self.request.GET['view']
            if template_pk == 'all':
                return 'atividades/activity/activity_list_all.html'
        except:
            pass

        user_status = get_priority_user(self.request)
        if user_status == 'Teacher':
            return 'atividades/activity/activity_list_teacher.html'
        elif user_status == 'Coordinator':
            return 'atividades/activity/activity_list_coord.html'
        elif user_status == 'Administrator':
            return 'atividades/activity/activity_list_administrator.html'
        else:
            return 'atividades/activity/activity_not_started.html'

    def get_context_data(self, **kwargs):
        context = super(ActivityListView, self).get_context_data(**kwargs)
        if ConfigActivity.objects.first() is not None:
            if not Config.objects.first().has_activity_sub_started():
                return context
            # context['department'] = Department.objects.all()
            # context['teacher']: Teacher.objects.all()
            result = []
            # from itertools import chain
            # for i in range(1, 10):
            # 	temp = SessionActivity.objects.filter(activity__teacher__id = 1)
            # 	result = list(chain(temp, result))
            # print(len(result))
            # context['session_activity'] = result
            context['dspair'] = DaySessionPair.objects.all()
            context['session_list'] = Session.objects.all()
            context['days'] = serializers.serialize('json', SessionDay.objects.all().order_by('date'))
            config = Config.objects.first()
            context['sub_time'] = serializers.serialize('json', [config])
            context['campus'] = Campus.objects.all().order_by('name')
            context['config'] = Config.objects.first()
            if not self.request.user.is_authenticated:
                context['session_activity'] = SessionActivity.objects.filter(activity__status = Activity.EstadoAtividade.VÁLIDA)
            else:
                try:
                    template_pk = self.request.GET['view']
                    if (not self.request.user.is_authenticated) or template_pk == 'all':
                        context['session_activity'] = SessionActivity.objects.filter(activity__status = Activity.EstadoAtividade.VÁLIDA)
                        context['o_unit'] = OrganicUnit.objects.all().order_by('name')
                        context['theme_list'] = Theme.objects.all().order_by('name')
                except:
                    if self.request.user.is_authenticated:
                        user_status = get_priority_user(self.request)
                        if user_status == 'Teacher':
                            context['session_activity'] = SessionActivity.objects.filter(activity__teacher__user = self.request.user)
                        elif user_status == 'Coordinator':
                            context['session_activity'] = SessionActivity.objects.filter(activity__teacher__department__ou = self.request.user.coordinator.ou)
                            context['t_list'] = Teacher.objects.filter(department__ou = self.request.user.coordinator.ou)
                        elif user_status == 'Administrator':
                            context['session_activity'] = SessionActivity.objects.all()
                            context['o_unit'] = OrganicUnit.objects.all().order_by('name')
                            context['t_list'] = Teacher.objects.all()
                    else:
                        context['session_activity'] = SessionActivity.objects.filter(activity_status = 'VÁLIDA')
                        context['o_unit'] = OrganicUnit.objects.all().order_by('name')
                        context['theme_list'] = Theme.objects.all().order_by('name')
        return context


class ActivityListAllView(TemplateView):
    """
    Mostra as Sessões de atividades criadas.

    **Context**

    ``SessionActivity``
            Instância do modelo :model:`atividades.SessionActivity`

    ``Department``
            Instância do modelo :model:`atividades.Department`

    ``Teacher``
            Instância do modelo :model:`atividades.Teacher`

    **Template**
            :template:`atividades/activity/activity_list.html`
    """
    template_name = 'atividades/activity/activity_list.html'

    def get_context_data(self, **kwargs):
        context = super(ActivityListAllView, self).get_context_data(**kwargs)
        context['department'] = Department.objects.all()
        context['teacher']: Teacher.objects.all()
        context['session_activity'] = SessionActivity.objects.all()
        context['dspair'] = DaySessionPair.objects.all()
        return context


class ActivityDeleteView(ActivityUserTestMixin, SuccessMessageMixin,  DeleteView):
    """
    Deletes an instance of model SessionActivity. \r\n

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_delete.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` and the creator\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """

    model = SessionActivity
    template_name = 'atividades/activity/activity_delete.html'
    success_url = '/atividades'
    success_message = 'Atividade apagada com sucesso'

    def delete(self, request, *args, **kwargs):
        """
        now returns success message when a SessionActivity is deleted
        """
        messages.success(self.request, self.success_message)
        return super(ActivityDeleteView, self).delete(request, *args, **kwargs)


class ActivityDetailView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Shows the detail of a SessionActivity. \r\n

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n
        - :model:`atividades.DaySessionPair` \r\n
        - :model:`atividades.Session` \r\n
        - :model:`atividades.Config` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_detail.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` and the creator\r\n
    """
    template_name = 'atividades/activity/activity_detail.html'

    def get(self, request, pk, *args,**kwargs):
        context = super(ActivityDetailView, self).get_context_data(**kwargs)
        context['object'] = SessionActivity.objects.get(id = pk)
        context['dspair'] = list(DaySessionPair.objects.filter(session_activity__id = pk))
        context['session_list'] = Session.objects.all()
        context['config'] = Config.objects.first()
        return self.render_to_response(context)

    def test_func(self, **kwargs):
        return SessionActivity.objects.get(id = self.kwargs['pk']).activity.teacher.user == self.request.user

class ActivityCreateView(UserCanCreateActivity, SuccessMessageMixin, SessionWizardView):
    """
    Creates a new Activity using a SessionWizard

    **Context**

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_form.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher`\r\n
    """

    # define a lista de forms utilizados
    form_list = FORMS
    template_name = 'atividades/activity/activity_form.html'
    success_message = 'Atividade proposta com sucesso!'

    # def get_context_data(self, **kwargs):
    # 	context = super().get_context_data(**kwargs)
    # 	# context['momentjs'] = DaySessionPairFormSet.media._js[0]
    # 	# context['bootstrap_datepicker'] = DaySessionPairFormSet.media._js[1]
    # 	# context['datepickerwid'] = static(DaySessionPairFormSet.media._js[2])
    # 	return context

    # no fim dos passos
    def done(self, form_list, **kwargs):
        # recolhe cada form sendo
        # aform -> formulario sobre a informação da atividade
        # asform -> fomrulário sobre o conjunto de dias e sessoes nos quais a atividade se vai realizar
        # mform -> material necessário para a atividade
        # lform -> localizaçao de preferência da atividade
        aform = list(form_list)[0]
        asform = list(form_list)[1]
        mform = list(form_list)[2]
        lform = list(form_list)[3]
        #cria a atividade

        # se todos os forms forem válidos
        if asform.is_valid() and aform.is_valid() and mform.is_valid() and lform.is_valid():

            # saves activitity
            activity_form = aform.save(commit = False)
            activity_form.teacher = Teacher.objects.get(user = self.request.user)
            activity_form.save()
            activity = Activity.objects.filter(teacher = Teacher.objects.get(user = self.request.user)).last()

            # add themes to activity
            theme = aform.cleaned_data['theme']
            [activity.theme.add(t) for t in theme]

            #createSessionActivity
            sss_act = SessionActivity(activity = activity).save()
            sss_act = SessionActivity.objects.get(activity = activity)

            # save material
            for form in mform:
                try:
                    if form.is_valid():
                        material = form.cleaned_data['material']
                        qtt = form.cleaned_data['quantity']
                        if qtt > 0:
                            MaterialActivity(material =Material.objects.get(name = material), quantity = qtt, session_activity = sss_act).save()
                except:
                        pass

            # guarda os pares de dia + sessões
            for form in asform:
                from datetime import date, datetime
                try:
                    day = form.cleaned_data['day']
                    DaySessionPair(day = day, session_activity = sss_act).save()
                    dsp = DaySessionPair.objects.filter(session_activity = sss_act).last()
                    for s in form.cleaned_data['session']:
                            dsp.session.add(s)
                    sss_act.session.add(dsp)
                except:
                        pass

            sss_act.location = lform.cleaned_data['location']
            sss_act.location_desc = lform.cleaned_data['description']
            sss_act.save()
            messages.success(self.request, self.success_message)
            return redirect('activity:activity')



class ActivityDefaultUpdateView(UserCanCreateActivity, SuccessMessageMixin,  TemplateView):
    """
    Updates a part of an instance of model SessionActivity. \r\n

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_update_default.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` and the creator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    template_name = 'atividades/activity/activity_update_default.html'
    success_message = 'Atividade atualizada com sucesso!'

    def get_success_url(self, **kwargs):
        return reverse('activity-detail', kwargs={'pk': self.object.pk})

    def get(self, request, pk, *args,**kwargs):
        context = super(ActivityDefaultUpdateView, self).get_context_data(**kwargs)
        context['aform'] = ActivityForm(instance = SessionActivity.objects.get(id = pk).activity)
        return self.render_to_response(context)

    def post(self, request,pk,  *args, **kwargs):
        form = ActivityForm(data = self.request.POST, instance = SessionActivity.objects.get(id = pk).activity)
        # fix later
        if form.is_valid():
            try:
                form.save()
                sa = SessionActivity.objects.get(id = pk).activity
                sa.status = Activity.EstadoAtividade.PENDENTE
                sa.save();
                sa = SessionActivity.objects.get(id = pk)
                sa.change_request = ""
                sa.save()
            except:
                pass
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(reverse('activity:activity-detail', kwargs={'pk': pk}))


class ActivitySessionUpdateView(UserCanCreateActivity,  SuccessMessageMixin, TemplateView):
    """
    Updates a part of an instance of model SessionActivity. \r\n

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_update_session.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` and the creator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    template_name = 'atividades/activity/activity_update_session.html'
    success_message = 'Atividade atualizada com sucesso!'

    def get(self, request, pk, *args,**kwargs):
        context = super(ActivitySessionUpdateView, self).get_context_data(**kwargs)
        context['asform'] = DaySessionPairFormSet(instance = SessionActivity.objects.get(id = pk))
        return self.render_to_response(context)

    def post(self, request,pk,  *args, **kwargs):
        asform = DaySessionPairFormSet(data = self.request.POST, instance = SessionActivity.objects.get(id = pk))
        asform = list(asform)
        # fix later
        if asform:
            for form in asform:
                if form.is_valid():
                    try:
                        form.save()
                    except:
                        pass
                else:
                    print(form.errors.as_data())
            sa = SessionActivity.objects.get(id = pk).activity
            sa.status = Activity.EstadoAtividade.PENDENTE
            sa.save();
            sa = SessionActivity.objects.get(id = pk)
            sa.change_request = ""
            sa.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(reverse('activity:activity-detail', kwargs={'pk': pk}))

class ActivityMaterialUpdateView(UserCanCreateActivity, SuccessMessageMixin, TemplateView):
    """
    Updates a part of an instance of model SessionActivity. \r\n

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_update_material.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` and the creator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """

    template_name = 'atividades/activity/activity_update_material.html'
    success_message = 'Atividade atualizada com sucesso!'

    def get(self, request, pk, *args,**kwargs):
        context = super(ActivityMaterialUpdateView, self).get_context_data(**kwargs)
        context['mform'] = MaterialFormSetFinal(instance = SessionActivity.objects.get(id = pk))
        return self.render_to_response(context)

    def post(self, request,pk,  *args, **kwargs):
        asform = MaterialFormSetFinal(data = self.request.POST, instance = SessionActivity.objects.get(id = pk))
        asform = list(asform)
        # fix later
        if asform:
            for form in asform:
                # print(form.is_valid())
                if form.is_valid():
                    try:
                        form.save()
                    except:
                        pass
                else:
                    print(form.errors.as_data())

            sa = SessionActivity.objects.get(id = pk).activity
            sa.status = Activity.EstadoAtividade.PENDENTE
            sa.save()
            sa = SessionActivity.objects.get(id = pk)
            sa.change_request = ""
            sa.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(reverse('activity:activity-detail', kwargs={'pk': pk}))

class ActivityLocationUpdateView(UserCanCreateActivity, SuccessMessageMixin, TemplateView):
    """
    Updates a part of an instance of model SessionActivity. \r\n

    **Models** \r\n
        - :model:`atividades.SessionActivity` \r\n

    **Templates** \r\n
        - :template:`atividades/activity/activity_update_location.html`.
    \r\n

    **Requires**\r\n
        - user to be an :model:`users.Teacher` and the creator`\r\n
        - must be within activity subscription limit defined by :model:`config_dia_aberto.Config`
    """
    template_name = 'atividades/activity/activity_update_location.html'
    success_message = 'Atividade atualizada com sucesso!'

    def get(self, request, pk, *args,**kwargs):
        context = super(ActivityLocationUpdateView, self).get_context_data(**kwargs)
        try:
            data = {
                'campus': SessionActivity.objects.get(id = pk).location.building.campus,
                'building': SessionActivity.objects.get(id = pk).location.building,
                'location': SessionActivity.objects.get(id = pk).location,
                'description': SessionActivity.objects.get(id = pk).location_desc,
            }
        except:
            data = {}
        context['lroomform'] = LocationForm(data = data)
        return self.render_to_response(context)

    def post(self, request,pk,  *args, **kwargs):
        form = LocationForm(data = self.request.POST)
        if form.is_valid():
            sss_act = SessionActivity.objects.get(id = pk)
            sss_act.change_request = ""

            sss_act.location = form.cleaned_data['location']
            sss_act.location_desc = form.cleaned_data['description']
            sss_act.save()
            sa = SessionActivity.objects.get(id = pk).activity
            sa.status = Activity.EstadoAtividade.PENDENTE
            sa.save()
            messages.success(self.request, self.success_message)
        else:
            context = super(ActivityLocationUpdateView, self).get_context_data(**kwargs)
            try:
                data = {
                    'campus': SessionActivity.objects.get(id = pk).location.building.campus,
                    'building': SessionActivity.objects.get(id = pk).location.building,
                    'location': SessionActivity.objects.get(id = pk).location,
                    'description': SessionActivity.objects.get(id = pk).location_desc,
                }
            except:
                data = {}
            return redirect(request, 'activity:ctivity-location-update', context)
        return HttpResponseRedirect(reverse('activity:activity-detail', kwargs={'pk': pk}))



############################################################
#################  AJAX SECTION  ###########################
############################################################
def load_buildings(request):
    """
    View utilizada para uma função ajax em que imprime todos os edificios do campus associados ao campus escolhido.

    **Context**

    ``Building``
        Instância do modelo :model:`atividades.Building`.

    **Template**
        :template:`atividades/activity/buildings_dropdown_list_options.html`

    """
    campus_id = request.GET.get('data_id')
    if campus_id:
        bd = Building.objects.filter(campus_id = campus_id).order_by('id')
    else:
        bd = Building.objects.all().order_by('id')
    return render(request, 'atividades/ajax/buildings_dropdown_list_options.html', {'buildings': bd})


def load_free_location(request):
    b_id = request.GET.get('data_id')
    sa_id = request.GET.get('sa_id')
    res = []
    if b_id:
        sala = Location.objects.filter(building_id = b_id).order_by('floor', 'sala')
        sa = SessionActivity.objects.get(id = sa_id)
        session_days = []
        [session_days.append(s.day) for s in sa.session.all()]
        # not proud of this thing
        for room in sala:
            flag = True
            for ses in room.sessionactivity_set.all():
                for i in ses.session.all():
                    if (i.day in session_days) and ses.activity.status == Activity.EstadoAtividade.VÁLIDA:
                        flag = False
                        break;
            if flag:
                res.append(room)
    return render(request, 'atividades/ajax/location_dropdown_list_options.html', {'location': res})


def load_location(request):
    """
    View utilizada para uma função ajax em que imprime todos as salas do campus associados ao edificio escolhido.

    **Context**

    ``Location``
        Instância do modelo :model:`atividades.Location`.

    **Template**
        :template:`atividades/location_dropdown_list_options.html`
    """
    b_id = request.GET.get('data_id')
    if b_id:
        sala = Location.objects.filter(building_id = b_id).order_by('floor', 'sala')
    elif not b_id == '':
        sala = []
    else:
        sala = Location.objects.all()
    return render(request, 'atividades/ajax/location_dropdown_list_options.html', {'location': sala})



def activity_detail_info(request):
    """
    View utilizada para imprimir a informação  referente a atividade na lista.

    **Context**

    ``Location``
        Instância do modelo :model:`SessionActivity`.

    **Template**
        :template:`atividades/ajax/activity_detail_info.html`
    """
    a_id = request.GET.get('data_id')
    activity = SessionActivity.objects.get(id = a_id)
    # if activity.activity.teacher.user is request.user:
    return render(request, 'atividades/ajax/activity_detail_info.html', {'dspair': DaySessionPair.objects.all(), 'sa': activity, 'session_list': Session.objects.all()})


def delete_session(request):
	try:
		ds_id = request.GET.get('ds_id')
		if DaySessionPair.objects.get(id = ds_id).session_activity.activity.teacher.user == request.user:
			DaySessionPair.objects.get(id = ds_id).delete()
	except:
		pass
	return HttpResponse('<script type="text/javascript">window.close(); window.parent.location.href = "/";</script>')

def delete_material(request):
	ds_id = request.GET.get('ds_id')
	try:
		if MaterialActivity.objects.get(id = ds_id).session_activity.activity.teacher.user == request.user:
			MaterialActivity.objects.get(id = ds_id).delete()
	except:
		pass
	return HttpResponse('<script type="text/javascript">window.close(); window.parent.location.href = "/";</script>')

def request_change(request):
    is_admin = False
    is_coord = False
    data  = request.GET.get('data_id')
    text =  request.GET.get('text')
    sa = SessionActivity.objects.get(id = data)
    try:
        is_admin = request.user.administrator
    except:
        pass
    try:
        is_coord = request.user.coordinator and sa.activity.teacher.department.ou == request.user.coordinator.ou
    except:
        pass
    if is_admin or is_coord:
        try:
            sa = SessionActivity.objects.get(id = data)
            a = sa.activity
            a.status = Activity.EstadoAtividade.REJEITADA
            a.save()
            sa.change_request = text
            sa.save()
        except:
            pass
    return HttpResponse()


def change_status(request):
    data  = request.GET.get('data_id')
    sa = SessionActivity.objects.get(id = data)
    status = request.GET.get('status')
    location = request.GET.get('local')
    is_admin = False
    is_coord = False
    try:
        is_admin = request.user.administrator
    except:
        pass
    try:
        is_coord = request.user.coordinator and sa.activity.teacher.department.ou == request.user.coordinator.ou
    except:
        pass
    if is_admin or is_coord:
        if data and status:
            act = SessionActivity.objects.get(id = data).activity
            act.status = getattr(Activity.EstadoAtividade, status)
            act.save()
            if status == 'VÁLIDA':
                sa.location = Location.objects.get(id = location)
                sa.location_desc = ''
            elif status == 'REJEITADA':
                sa.location = None
                sa.location_desc = 'atividade rejeitada'
            sa.save()
            # print(sa.location, sa.location_desc)
    return HttpResponse('')

def modal_fill(request):
    sa_id = request.GET.get('sa_id')
    local_desc = request.GET.get('local_desc')
    location = request.GET.get('location')
    context = {}
    if location:
        context['location'] = Location.objects.get(id = location).get_full_location_path()
    else:
        context['location'] = local_desc
    sa = SessionActivity.objects.get(id = sa_id)
    context['sa'] = sa
    if not sa.materialactivity_set.all():
        context['no_mat'] = 'não foram definidos materiais para a atividade'
    return render(request, 'atividades/ajax/modal_info.html', context)

def get_duration(request):
    sa_value = request.GET.get('data_id')
    if sa_value:
        return HttpResponse(SessionActivity.objects.get(id = sa_value).activity.duration)
    else:
        return
############################################################
################# END AJAX SECTION  ########################
############################################################
