from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext_lazy as _

import json
from datetime import datetime, date, time, timedelta
from colorfield.fields import ColorField

from university.models import Department, OrganicUnit, Campus, Building, Campus, Location
from config_dia_aberto.models import Config
from users.models import Teacher
from university.models import Location
from utils.mixins import SingleInstanceMixin
from django.utils import timezone

# Create your models here.

class ConfigActivity(SingleInstanceMixin, models.Model):
    config = models.OneToOneField(Config, on_delete=models.CASCADE, verbose_name='Dia Aberto')
    start_time = models.TimeField(verbose_name='hora de início das sessões')
    end_time = models.TimeField(verbose_name='hora de fim das sessões')
    session_interval = models.IntegerField(
                        verbose_name='intervalo de tempo entre sessões',
                        default=30,
                        validators=[
                            MinValueValidator(1),
                            MaxValueValidator(120),
                        ])

    def is_sub_activity_over(self, dt_arg):
        return self.config.activity_limit_subscription_start_datetime < dt_arg

    def has_sub_activity_started(self, dt_arg):
        return self.config.activity_limit_subscription_start_datetime > dt_arg

    def sub_activity_over(self):
        return self.config.activity_limit_subscription_start_datetime < timezone.now()

    def sub_activity_started(self):
        return self.config.activity_limit_subscription_start_datetime > timezone.now()

    def time_range_gen(self):
        today = date.today()
        start_datetime = datetime.combine(today, self.start_time)
        end_datetime = datetime.combine(today, self.end_time)
        delta_datetime = end_datetime - start_datetime
        for i in range(0, delta_datetime.seconds, self.session_interval*60):
            yield (start_datetime + timedelta(seconds=i)).time()
        return

    def save_sessions(self):
        for start_time in self.time_range_gen():
            try:
                self.session_set.get(start_time=start_time)
            except Session.DoesNotExist:
                session = Session.create(start_time=start_time, config_activity=self)
                session.save()

    def date_range_gen(self):
        start_date = self.config.start_datetime.date()
        delta_date = self.config.end_datetime.date() - start_date
        for i in range(delta_date.days + 1):
            yield start_date + timedelta(days=i)
        return

    def save_sessiondays(self):
        for date in self.date_range_gen():
            try:
                self.sessionday_set.get(date=date)
            except SessionDay.DoesNotExist:
                session_day = SessionDay.create(date=date, config_activity=self)
                session_day.save()
                session_day.sessions.set(self.session_set.all())

    def delete_sessions(self):
        self.session_set.filter(start_time__lt=self.start_time).delete()
        self.session_set.filter(start_time__gt=self.end_time).delete()
        for session in self.session_set.all():
            if session.start_time.hour*60 + session.start_time.minute % self.session_interval != 0:
                session.delete()

    def delete_sessiondays(self):
        self.sessionday_set.filter(date__lt=self.config.start_datetime).delete()
        self.sessionday_set.filter(date__gt=self.config.end_datetime).delete()

    def update_sessions(self):
        self.delete_sessions()
        self.save_sessions()

    def update_sessiondays(self):
        self.delete_sessiondays()
        self.save_sessiondays()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.update_sessions()
        self.update_sessiondays()

    class Meta:
        verbose_name = 'Configuração das Atividades'


class Session(models.Model):
    start_time = models.TimeField(verbose_name = 'data de início de sessão')
    config_activity = models.ForeignKey(ConfigActivity, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.start_time.strftime("%H:%M")}'

    @classmethod
    def create(cls, start_time, config_activity):
        sessionday = cls(start_time=start_time, config_activity=config_activity)
        return sessionday

    class Meta:
        ordering = ['start_time']
        verbose_name_plural = 'Sessões'


class SessionDay(models.Model):
    date = models.DateField()
    config_activity = models.ForeignKey(ConfigActivity, on_delete=models.CASCADE)
    sessions = models.ManyToManyField(Session)


    class Meta:
        ordering = ['date']

    def __str__(self):
        return f'{self.date}'

    @classmethod
    def create(cls, date, config_activity):
        sessionday = cls(date=date, config_activity=config_activity)
        return sessionday

    def get_year(self):
        return self.date.strftime("%Y")


class Material(models.Model):
    name = models.CharField(max_length = 32, verbose_name = 'nome')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Materiais'


class Theme(models.Model):
    name = models.CharField(max_length = 20, verbose_name = 'tema', unique=True)
    color = ColorField(default='#FF0000', verbose_name = 'cor')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Tema'


class Activity(models.Model):
    class EstadoAtividade(models.TextChoices):
        REJEITADA = 'REJEITADA', _('rejeitada')
        PENDENTE = 'PENDENTE', _('pendente')
        INCOMPLETA = 'INCOMPLETA', _('incompleta')
        VÁLIDA = 'VÁLIDA', _('válida')


    name = models.CharField(max_length = 32, verbose_name = 'nome', unique = True)
    description = models.TextField(verbose_name = 'descrição')
    duration = models.TimeField(verbose_name = 'duração')
    max_participants = models.IntegerField(
        verbose_name = 'número máximo de participantes',
        default = 1,
        validators=
        [
            MinValueValidator(1),
            MaxValueValidator(120),
        ])
    theme  = models.ManyToManyField(Theme, related_name = 'tema')
    teacher = models.ForeignKey(Teacher, on_delete = models.CASCADE)
    target  = models.TextField(verbose_name = 'Público alvo')
    n_colab = models.IntegerField(
        verbose_name = 'número de colaboradores',
        default = 0,
        validators =
        [
            MinValueValidator(0),
            MaxValueValidator(20),
        ]
    )

    status = models.CharField(
        max_length=10,
        choices=EstadoAtividade.choices,
        default=EstadoAtividade.PENDENTE,
    )

    def get_teacher(self):
        return self.teacher

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'Atividades'


from django.core.exceptions import ValidationError

class MaterialActivity(models.Model):
    material = models.ForeignKey(Material, on_delete = models.CASCADE)
    quantity = models.IntegerField(verbose_name = 'quantidade')
    session_activity = models.ForeignKey('SessionActivity', on_delete = models.CASCADE)

    def __str__(self):
        return f'Material {self.material} | Quantidade {self.quantity}'

    # def clean(self):
    #     if not (self.material or self.quantity):
    #         raise ValidationError('Requer que escolha o material e a respectiva quantidade')

    class Meta:
        verbose_name_plural = 'Materiais para Atividade'
        unique_together = ('session_activity', 'material')


class DaySessionPair(models.Model):
    session = models.ManyToManyField(Session, verbose_name = 'sessão')
    day = models.ForeignKey(SessionDay, on_delete = models.CASCADE, verbose_name = 'dia')
    session_activity = models.ForeignKey('SessionActivity', on_delete = models.CASCADE)

    def __str__(self):
        return f'{self.day} | {self.session}'

    class Meta:
        ordering = ['day']
        verbose_name = 'Par Sessão-Dia'
        unique_together = ('session_activity', 'day')



class DaySessionActivity(models.Model):
    day = models.ForeignKey(SessionDay, on_delete = models.CASCADE, verbose_name = 'dia')
    session = models.ForeignKey(Session, on_delete = models.CASCADE, verbose_name = 'sessão')
    activity = models.ForeignKey(Activity, on_delete = models.CASCADE, verbose_name = 'atividade')

    def __str__(self):
        return f'{self.activity} | {self.day} | {self.session}'

    def get_localization(self):
        session = SessionActivity.objects.get(activity=self.activity)
        return session.location.get_full_location_path()

    @classmethod
    def get_dsas_with_subs(cls, organic_unit=None):
        """ Returns a Queryset of DaySessionActivity instances that
            has participants subscribed to them """
        dsas = cls.objects.filter(activity__teacher__department__ou=organic_unit)

        dsa_pks = set()
        for dsa in dsas:
            if dsa.subscriptionsessionativitiy_set.all():
                dsa_pks.add(dsa.pk)

        return cls.objects.filter(pk__in=dsa_pks)

    @classmethod
    def get_dsa_destinations(cls, organic_unit=None, source_dsa=None):
        """ Returns a Queryset of DaySessionActivity instances that are possible destinations i.e.
        they share organic unit, day, and at least 1 subscription"""
        dsas = cls.get_dsas_with_subs(organic_unit=organic_unit) \
            .filter(day=SessionDay.objects.get(pk=source_dsa.day.pk)) \
            .exclude(pk=source_dsa.pk).exclude(session__start_time__lte=source_dsa.session.start_time) \
            .order_by('session__start_time')

        ssa_1 = source_dsa.subscriptionsessionativitiy_set.values_list('sub', flat=True)
        dsa_pks = set()
        for dsa in dsas:
            ssa_2 = dsa.subscriptionsessionativitiy_set.values_list('sub', flat=True)
            intersect_set = set(ssa_1) & set(ssa_2)

            if intersect_set:
                dsa_pks.add(dsa.pk)

        return cls.objects.filter(pk__in=dsa_pks)

    class Meta:
        verbose_name = 'Holy DSA'
        constraints = [
            models.UniqueConstraint(fields=['day', 'session', 'activity'], name='unique_dsa')
        ]


class SessionActivity(models.Model):
    activity = models.OneToOneField(Activity, on_delete = models.CASCADE, verbose_name = 'atividade')
    session = models.ManyToManyField(DaySessionPair, verbose_name = 'sessão')
    material_qtt = models.ManyToManyField(Material, through = MaterialActivity, blank = True)
    location = models.ForeignKey(Location, on_delete = models.CASCADE, verbose_name = 'localização', blank = True, null = True)
    location_desc = models.TextField(blank = True, null = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    change_request = models.TextField(blank = True, null = True)

    def get_status(self):
        if self.activity and  DaySessionPair.objects.filter(session_activity__id = self.id).count() > 0 and (self.location or self.location_desc):
            return self.activity.status
        else:
            return 'INCOMPLETA'

    def __str__(self):
        return f'{self.session}|{self.activity}'


    def delete(self, *args, **kwargs):
        self.activity.delete()
        return super(self.__class__, self).delete(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Sessões-Atividades'



    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        for dsp in self.session.all():
            for session in dsp.session.all():
                try:
                    DaySessionActivity.objects.get(day=dsp.day, session=session, activity=self.activity)
                except DaySessionActivity.DoesNotExist:
                    dsa = DaySessionActivity.objects.create(day=dsp.day, session=session, activity=self.activity)
                    dsa.save()


    # def get_first_session(self):
    #     try:
    #     result = day.objects.first().session.first()
    #     return result = ''.join((result = (first_session.first()) if result < first_session.first() else result)) for first_session in day.objects.all())
    # except:
    #     return ''


    # def get_first_session_of_day(self, day):
    #     try:
    #         return DaySessionPair.objects.get(session_activity = self.id, day__date = day).session.first()
    #     except:
    #         return ''

    # def get_last_session_of_day(self, day):
    #     try:
    #         return DaySessionPair.objects.get(session_activity = self.id, day__date = day).session.last()
    #     except:
    #         return ''
