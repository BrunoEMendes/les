from django.contrib import admin
from django.urls import path
# from .views import ActivityListView
# from .views import activity_list, activity_create
from .views import (ActivityCreateView,
                    ActivityDetailView,
                    ActivityDeleteView,
                    ActivityListView,
                    ActivityListAllView,
                    ActivityDefaultUpdateView,
                    ActivitySessionUpdateView,
                    ActivityMaterialUpdateView,
                    ActivityLocationUpdateView,
                    ConfigActivityView,
                    ConfigActivityCreateView,
                    ConfigActivityUpdateView,
                    MaterialListView,
                    MaterialUpdateView,
                    MaterialCreateView,
                    MaterialCreateView2,
                    MaterialDeleteView,
                    ThemeCreateView,
                    ThemeCreateView2,
                    ThemeListView,
                    ThemeDetailView,
                    ThemeUpdateView,
                    ThemeDeleteView,
                    load_buildings,
                    load_location,
                    delete_session,
                    delete_material,
                    activity_detail_info,
                    get_duration,
                    change_status,
                    modal_fill,
                    load_free_location,
                    request_change,
                    FORMS, )

from .forms import MaterialForm

app_name = 'activity'


urlpatterns = [
    # atividades
    path('', ActivityListView.as_view(), name = 'activity'),
    path('todas/', ActivityListAllView.as_view(), name = 'activity-all'),
    path('propor/', ActivityCreateView.as_view(FORMS), name = 'activity-create'),
    path('<int:pk>/', ActivityDetailView.as_view(), name='activity-detail'),
    path('<int:pk>/delete/', ActivityDeleteView.as_view(), name='activity-delete'),
    path('<int:pk>/updateinfo/', ActivityDefaultUpdateView.as_view(), name='activity-update-default'),
    path('<int:pk>/updatesession/', ActivitySessionUpdateView.as_view(), name = 'activity-session-update'),
    path('<int:pk>/updatematerial/', ActivityMaterialUpdateView.as_view(), name = 'activity-material-update'),
    path('<int:pk>/updatelocation/', ActivityLocationUpdateView.as_view(), name = 'activity-location-update'),

    # config
    path('config/', ConfigActivityView.as_view(), name = 'config-activity'),
    path('config/nova', ConfigActivityCreateView.as_view(), name = 'config-create'),
    path('config/atualizar/', ConfigActivityUpdateView.as_view(), name = 'config-update'),

    # temas
    path('tema/', ThemeListView.as_view(), name='theme-list'),
    path('tema/criar/', ThemeCreateView.as_view(), name='theme-create'),
    path('tema/criartema/', ThemeCreateView2.as_view(), name='theme-create-theme'),
    path('tema/<int:pk>/', ThemeDetailView.as_view(), name='theme-detail'),
    path('tema/<int:pk>/alterar/', ThemeUpdateView.as_view(), name='theme-update'),
    path('tema/<int:pk>/eliminar/', ThemeDeleteView.as_view(), name='theme-delete'),

    # materiais
    path('material/criar/', MaterialCreateView.as_view(), name='material-create'),
    path('material/criarmaterial/', MaterialCreateView2.as_view(), name='material-create-material'),
    path('material/', MaterialListView.as_view(), name='material-list'),
    path('material/<int:pk>/eliminar/', MaterialDeleteView.as_view(), name='material-delete'),
    path('material/<int:pk>/alterar/', MaterialUpdateView.as_view(), name='material-update'),

    #ajax paths
    path('ajax/load-buildings/', load_buildings, name='ajax_load_buildings'),  # <-- this one here
    path('ajax/load-location/', load_location, name='ajax_load_location'),  # <-- this one here
    path('ajax/delete-session/', delete_session, name='ajax_delete-session'),
    path('ajax/delete-material/', delete_material, name='ajax_delete-material'),
    path('ajax/activity_detail_info/',  activity_detail_info, name='ajax_detail-info'),
    path('ajax/change_status/', change_status, name='ajax_change-activity-status'),
    path('ajax/modal_fill/', modal_fill, name='ajax_modal-fill'),
    path('ajax/load_free_location/', load_free_location, name='ajax_load-free-location'),
    path('ajax/get_duration/', get_duration, name='ajax_get-duration'),
    path('ajax/request_change/', request_change, name='ajax_request-change'),
]
