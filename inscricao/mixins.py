from utils.mixins import UserTypesTestMixin
from users.models import Teacher

class ActivityUserTestMixin(UserTypesTestMixin):
    user_types = [Teacher]
