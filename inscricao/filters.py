import django_filters
from django_filters import DateFilter
from django.db.utils import ProgrammingError
from users.models import *
from inscricao.models import *
from django.forms import *
from .models import *


def get_school_choices():
    try:
        choices = [(dep.school.id,dep.school.name) for dep in Subscription.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

def get_participant_choices():
    try:
        choices = [(dep.participant.id,dep.participant) for dep in Subscription.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

def get_date_choices():
    try:
        choices = [(date.date, date.date) for date in Subscription.objects.all()]
    except ProgrammingError:
        choices = []
    return choices

class SubscriptionFilter(Form):
    school=ChoiceField(
        label='Escola',
        choices =[('','Escola')] + get_school_choices(),
        required=False,
        widget=Select(attrs={'class':'input'}))
    participant=ChoiceField(
        label='Participante',
        choices =[('','Participante')] + get_participant_choices(),
        required=False,
        widget=Select(attrs={'class':'input'}))
    date=ChoiceField(
        label='Data',
        choices=[('', 'Data')] + get_date_choices(),
        required=False,
        widget=Select(attrs={'class':'input'}))
    
    def qs(self):
        #parent = super().qs
        date = self.data.get('date')
        if date == '' or date is None:
               date = Subscription.objects.all()
        else:
            date = Subscription.objects.filter(date=date)
        sch = self.data.get('school')
        if sch == '' or sch is None:
            sch = Subscription.objects.all()
        else:
            sch = Subscription.objects.filter(school=sch)
        partici=self.data.get('participant')
        if partici =='' or partici is None:
            partici=Subscription.objects.all()
        else:
            partici=Subscription.objects.filter(participant=partici)
        return date & sch & partici	
   
