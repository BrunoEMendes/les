from django.shortcuts import render, redirect, reverse, HttpResponseRedirect
from .models import *
from .forms import *
from atividades.models import (Activity, Session,
                    SessionActivity, MaterialActivity,
                    Material, SessionDay)
from atividades.forms import (ActivityForm, 
                    MaterialFormSetFinal, MaterialForm,
                    MaterialSessionForm)
from django.views.generic import (ListView, UpdateView,
                                    DetailView, CreateView,
                                    DeleteView, TemplateView)
from config_dia_aberto.models import Config,Transport,UniTransport,OwnTransport
from formtools.wizard.views import SessionWizardView
from django.urls import reverse
from atividades.mixins import ActivityUserTestMixin
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models.signals import post_save
from django.forms.models import inlineformset_factory
from university.models import Department
from users.models import *
from .filters import *
from django.shortcuts import Http404


# Create your views here.


def viewInscricao(request):
    if not request.user.is_authenticated:
            raise Http404
    
    if request.user.administrator:
            list_inscricoes = Subscription.objects.all()
    
            myFilter = SubscriptionFilter(request.GET)
            list_inscricoes = myFilter.qs
    else:
            raise Http404


    return render(request,
                template_name='inscricao/inscricaolist.html', 
                context = {'inscricoes': list_inscricoes, 'myFilter':myFilter})

def viewInscricao2(request, pk_test):
    try:
        if not request.user.is_authenticated:
                raise Http404
        if request.user.participant:
                list_escolas = Subscription.objects.get(id=pk_test)

        else:
                raise Http404
        if not list_escolas.participant  == request.user.participant:
                raise Http404 
    except:
            if not request.user.administrator:
                raise Http404
            else: 
                list_escolas = Subscription.objects.get(id=pk_test) 
    return render(request,
                template_name='inscricao/listINFO1.html', 
                context = {'escola':list_escolas})

def viewInscricao3(request, pk_test):
    try:
        if not request.user.is_authenticated:
                raise Http404
        if request.user.participant:
                list_escolas2 = Subscription.objects.get(id=pk_test)

        else:
                raise Http404
        if not list_escolas2.participant  == request.user.participant:
                raise Http404 
    except:
            if not request.user.administrator:
                raise Http404
            else: 
                list_escolas2 = Subscription.objects.get(id=pk_test) 
                            
    return render(request,
                template_name='inscricao/listINFO2.html', 
                context = {'escola2':list_escolas2})

def viewInscricao4(request, pk_test):
    try:
        if not request.user.is_authenticated:
                raise Http404
        if request.user.participant:
            list_escolas3 = Subscription.objects.get(id=pk_test)
            list_escolas4 = SubscriptionSessionAtivitiy.objects.filter(sub=list_escolas3.id)  
        else:
                raise Http404
        if not list_escolas3.participant  == request.user.participant:
                raise Http404 
    except:
            if not request.user.administrator:
                raise Http404
            else: 
                list_escolas3 = Subscription.objects.get(id=pk_test)
                list_escolas4 = SubscriptionSessionAtivitiy.objects.filter(sub=list_escolas3.id)  


    return render(request,
                template_name='inscricao/listINFO3.html', 
                context = {'escola4': list_escolas4,'escola3':list_escolas3 })

def viewInscricao5(request, pk_test):
    try:
        if not request.user.is_authenticated:
                raise Http404
        if request.user.participant:
                list_escolas5 = Subscription.objects.get(id=pk_test)
                list_escolas6 = SubscriptionSessionAtivitiy.objects.get(sub=list_escolas5.id)   
        else:
                raise Http404
        if not list_escolas5.participant  == request.user.participant:
                raise Http404 
    except:
            if not request.user.administrator:
                raise Http404
            else: 
                try:
                    list_escolas5 = Subscription.objects.get(id=pk_test)
                    list_escolas6 = SubscriptionSessionAtivitiy.objects.get(sub=list_escolas5.id)   
                except:
                        raise Http404  
    return render(request,
                template_name='inscricao/listINFO4.html', 
                context = {'escola6': list_escolas6,'escola5':list_escolas5 })

def viewInscricao6(request, pk_test):
    try:
        if not request.user.is_authenticated:
                raise Http404
        if request.user.participant:
                list_escolas7 = Subscription.objects.get(id=pk_test)
                list_escolas8 = MealAmmount.objects.filter(sub=list_escolas7.id)     
        else:
                raise Http404
        if not list_escolas7.participant  == request.user.participant:
                raise Http404 
    except:
            if not request.user.administrator:
                raise Http404
            else: 
                    try:
                        list_escolas7 = Subscription.objects.get(id=pk_test)
                        list_escolas8 = MealAmmount.objects.filter(sub=list_escolas7.id)   
                    except:
                        list_escolas7 = Null
                        list_escolas8 = Null  
    return render(request,
                template_name='inscricao/listINFO5.html', 
                context = {'escola8':list_escolas8, 'escola7': list_escolas7}
                )

def home(request):
    try:
                current_user = request.user.participant
                subscriptionUser = int(Subscription.objects.get(participant=current_user).id)

                return render(request,
                template_name='inscricao/index.html',
                context={'getID':subscriptionUser}
                )
        
        
    except:
                pass

                return render(request,
                template_name='inscricao/index.html',
                )
    


    

