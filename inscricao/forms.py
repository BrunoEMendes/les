from django.forms import *
from django import forms
from . models import *
from atividades.models import Session
from config_dia_aberto.models import Config
from django.urls import reverse_lazy
from datetime import datetime, date, timedelta

class ResponsavelForm(ModelForm):
    class Meta:
        model = Responsavel
        fields = ['name', 'email', 'phone']

class ParticipantForm(ModelForm):
	class Meta:
		model = Participant
		fields = ['user', 'area', 'school_year']
		widgets = {
			'school_year': NumberInput(attrs={'class': 'numberinput form-control', 'min':'9', 'max':'12'})
		}

class ParticipantGroupForm(ModelForm):
    class Meta:
        model = Participant_Group
        fields = ['n_participants']
        widgets = {
            'n_participants': NumberInput(attrs={'class': 'numberinput form-control', 'min':'1', 'max':'1000'})
        }

class SchoolForm(ModelForm):

	class Meta:
		model = School
		fields = ['name', 'address', 'zip', 'city']


class MealAmmountForm(ModelForm):

	class Meta:
		model = MealAmmount
		exclude = ['sub']


class TimeWidget(TimeInput):

    def __init__(self, attrs=None, format=None, input_type='time'):
        if input_type is not None:
            self.input_type=input_type
        if attrs is not None:
            self.attrs = attrs.copy()
        else:
            self.attrs = {'class': 'textinput textInput form-control'}
        if format is not None:
            self.format = format
        else: 
            self.format = '%H:%M'

class TransportForm(Form):
	TRANSPORT_CHOICES = [
		('Transport_Uni', 'Transporte da Universidade'),
		('Carro', 'Carro'),
		('Comboio', 'Comboio'),
		('Autocarro', 'Autocarro'),
	]
	transport_type = ChoiceField(choices=TRANSPORT_CHOICES, widget=Select(attrs={'class': 'textinput textInput form-control'}))
	transport_entry = CharField(widget=TimeWidget())
	transport_exit = CharField(widget=TimeWidget())
	
class SubscriptionForm(ModelForm):
	class Meta:
		model = Subscription
		exclude = ['date']

class SessionForm(ModelForm):
	class Meta:
		model = Session
		fields = '__all__'

class SubscriptionSessionAtivitiyForm(ModelForm):
    class Meta:
        model = SubscriptionSessionAtivitiy
        exclude = ['own_transport']
