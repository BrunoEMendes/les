"use strict";

//----------------------------------------------------------------------------
// bulma setup
//----------------------------------------------------------------------------

bulmaTagsinput.attach();

const calendars = bulmaCalendar.attach('.dayInputFilter', {
  type: 'date',
  color: 'link',
  dateFormat: 'YYYY-MM-DD',
  timeFormat: 'HH:mm',
  lang: 'pt',
  todayLabel: 'Hoje',
  cancelLabel: 'Cancelar',
  clearLabel: 'Limpar',
  validateLabel: 'Selecionar',
  minDate: new Date("2020-1-1"),
  maxDate: new Date(),
});




//----------------------------------------------------------------------------
// datatable
//----------------------------------------------------------------------------

$.fn.dataTable.ext.search.push(
  /**
   * decides if the row should get drawed or not
   * @param {*} settings list of settings
   * @param {*} data data of all collumns
   * @param {*} dataIndex row index
   * @returns true if contains all filters, false otherwise
   */
  function (settings, tableData, dataIndex, data) {
    return currentView.applyFilters(data);
  }
);

const DATATABLE_LANG_OPTIONS = {
  "emptyTable": "Não foi encontrado nenhuma notificação",
  "loadingRecords": "A carregar...",
  "processing": "A processar...",
  "lengthMenu": "Mostrar _MENU_ registos",
  "zeroRecords": "Não foram encontrados resultados",
  "info": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
  "infoEmpty": "Mostrando de 0 até 0 de 0 registos",
  "infoFiltered": "(filtrado de _MAX_ registos no total)",
  "infoPostFix": "",
  "search": "Procurar:",
  "url": "",
  "paginate": {
    "first": "Primeiro",
    "previous": '<i class="fas fa-chevron-left fa-lg"></i>',
    "next": '<i class="fas fa-chevron-right fa-lg"></i>',
    "last": "Último"
  }
};


function renderDate(date) {
  const timeDelta = Date.now() - date.getTime();
  if (timeDelta < 60000) {
    return "agora mesmo";
  }
  else if (timeDelta < 3600000) {
    const minutes = Math.floor(timeDelta / 60000);
    return "há " + minutes + " minuto" + (minutes > 1 ? "s" : "");
  }
  else if (timeDelta < 86400000) {
    const hours = Math.floor(timeDelta / 3600000);
    return "há " + hours + " hora" + (hours > 1 ? "s" : "");
  }
  else if (timeDelta < 172800000) {
    return "ontem";
  }
  else if (timeDelta < 604800000) {
    const days = Math.floor(timeDelta / 86400000);
    return "há " + days + " dia" + (days > 1 ? "s" : "");
  }

  return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

function renderPriority(isPriority) {
  if (isPriority) {
    return "<i class=\"fas fa-star\"></i>";
  }
  return "<i class=\"far fa-star\"></i>";
}

// taken from https://stackoverflow.com/questions/6234773/can-i-escape-html-special-chars-in-javascript
function escapeHtml(unsafe) {
  return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
}

function loadDataTable(view) {
  view.table = $(view.tableConfig.tableElement).DataTable({
    "autoWidth": false,
    "ajax": view.tableConfig.url,
    "columns": view.tableConfig.collumns,
    "dom": "",
    rowCallback: view.tableConfig.rowCallback,

    language: DATATABLE_LANG_OPTIONS,
  });


  // table page config

  $(view.tableConfig.previousBtn).on('click', () => {
    view.table.page('previous').draw('page');
  });

  $(view.tableConfig.nextBtn).on('click', () => {
    view.table.page('next').draw('page');
  });
}


function clearFilter(filterElement) {
  if (filterElement.type === "checkbox") {
    filterElement.checked = false;
  }
  else if (filterElement instanceof bulmaCalendar) {
    filterElement.clear();
  }
  else {
    filterElement.value = null;
  }
}


function buildFilters(view, filterElements) {
  // const searchBar = document.getElementById('ntfSearch');
  $(filterElements.searchBar).keyup(() => {
    view.updateSearch();
  });

  if (filterElements.onlyUnseenFilter) {
    $(filterElements.onlyUnseenFilter).change(view.updateSearch.bind(view));
    view.filters.push(function(data) {
      return filterElements.onlyUnseenFilter.checked && data.seen;
    });
  }
  if (filterElements.onlyPriorityFilter) {
    $(filterElements.onlyPriorityFilter).change(view.updateSearch.bind(view));
    view.filters.push(function(data) {
      return filterElements.onlyPriorityFilter.checked && !data.priority;
    });
  }


  let minTime = null;
  let maxTime = null;

  if (filterElements.startCalendarFilter && filterElements.endDateCalendar) {
    filterElements.startCalendarFilter.on('select', function () {
      const newDate = new Date(filterElements.startCalendarFilter.value());
      if (minTime !== newDate.getTime()) {
        minTime = newDate.getTime();

        // change maxTime for other calendar
        filterElements.endDateCalendar.datePicker.min = newDate;
        filterElements.endDateCalendar.datePicker.refresh();

        view.updateSearch();
      }
    });
    filterElements.startCalendarFilter.on('clear', function () {
      if (minTime !== null) {
        minTime = null;

        filterElements.endDateCalendar.datePicker.min = new Date("2020-1-1");
        filterElements.endDateCalendar.datePicker.refresh();

        view.updateSearch();
      }
    });

    filterElements.endDateCalendar.on('select', function () {
      const newDate = new Date(filterElements.endDateCalendar.value());
      if (maxTime !== newDate.getTime()) {

        // note: 86400000 = one day
        maxTime = newDate.getTime() + 86400000;

        // change minTime for other calendar
        filterElements.startCalendarFilter.datePicker.max = newDate;
        filterElements.startCalendarFilter.datePicker.refresh();

        view.updateSearch();
      }
    });
    filterElements.endDateCalendar.on('clear', function () {
      if (maxTime !== null) {
        maxTime = null;

        // change maxTime for other calendar
        filterElements.startCalendarFilter.datePicker.max = new Date();
        filterElements.startCalendarFilter.datePicker.refresh();

        view.updateSearch();
      }
    });

    view.filters.push(function (data) {
      return minTime !== null && data.sent_time.getTime() < minTime;
    });
    view.filters.push(function (data) {
      return maxTime !== null && data.sent_time.getTime() > maxTime;
    });
  }

  if (filterElements.clearBtn) {
    filterElements.clearBtn.addEventListener('click', (function() {
      const elements = Object.keys(filterElements).map(key => filterElements[key]);

      return function() {
        for (const element of elements) {
          clearFilter(element);
        }
        view.updateSearch();
      }

    })());
  }
}







class NotificationView {
  constructor(components) {
    this.loaded = false;
    this.components = components;
    this.filters = [];
  }

  show() {
    for (const comp of this.components) {
      comp.hidden = false;
    }
  }

  hide() {
    for (const comp of this.components) {
      comp.hidden = true;
    }
  }

  applyFilters(data) {
    for (const filter of this.filters) {
      if (filter(data)) return false;
    }
    return true;
  }

  load() {
    this.refreshBtn.classList.add('loading');
    this.refreshBtn.children[1].innerText = " a atualizar...";

    loadDataTable(this);

    function handler() {
      this.refreshBtn.classList.remove('loading');
      this.refreshBtn.children[1].innerText = "Atualizar";
    }

    this.table.on('xhr', handler.bind(this));
    this.table.on('error', handler.bind(this));

    buildFilters(this, this.filterElements);

    this.loaded = true;
  }

  changeHistoryState() {
    history.replaceState(this.viewPath, "", "?view=" + this.viewPath);
  }
}

const inboxSideBar = document.getElementById('inboxSideBar');

const receiveView = new NotificationView([
  document.getElementById('receiveViewFilters'),
  document.getElementById('receiveViewControlBar'),
  document.getElementById('receiveViewContentPannel'),
]);
receiveView.viewPath = "receive";
receiveView.filterElements = {
  searchBar: document.getElementById('receiveSearch'),
  onlyUnseenFilter: document.getElementById('receiveOnlyUnseenFilter'),
  onlyPriorityFilter: document.getElementById('receiveOnlyPriorityFilter'),
  startCalendarFilter: calendars[0],
  endDateCalendar: calendars[1],
  clearBtn: document.getElementById('receiveClearFilter'),
}
receiveView.updateSearch = function() {
  this.table.columns(2).search(this.filterElements.searchBar.value).draw();
};
receiveView.tableConfig = {
  tableElement: document.getElementById("receiveTable"),
  url: GET_ALL_RECEIVED_NOTIFICATIONS_URL_PATH,
  collumns: [
    { 'data': "priority", 'render': renderPriority },
    { 'data': "sender.name", 'render': escapeHtml },
    {
      'data': function (data) {
        return data.subject + " " + data.content;
      },
      'render': function (a, b, data) {
        return "<div><em>" + escapeHtml(data.subject) + "</em> - " + escapeHtml(data.content) + "</div>";
      }
    },
    {
      'data': function (data) {
        return data.sent_time = new Date(data.sent_time);
      },
      'render': renderDate
    },
  ],
  rowCallback(row, data) {
    // highlights unseen notifications
    if (!data.seen) {
      row.classList.add('ntfUnseen');
    }
  },
  previousBtn: document.getElementById('receivedPreviousBtn'),
  nextBtn: document.getElementById('receivedNextBtn'),
}




const sentView = new NotificationView([
  document.getElementById('sentViewFilters'),
  document.getElementById('sentViewControlBar'),
  document.getElementById('sentViewContentPannel'),
]);
sentView.viewPath = "sent";
sentView.filterElements = {
  searchBar: document.getElementById('sentSearch'),
  onlyPriorityFilter: document.getElementById('sentOnlyPriorityFilter'),
  startCalendarFilter: calendars[2],
  endDateCalendar: calendars[3],
  clearBtn: document.getElementById('sentClearFilter'),
}
sentView.updateSearch = function () {
  this.table.columns(1).search(this.filterElements.searchBar.value).draw();
};
sentView.tableConfig = {
  tableElement: document.getElementById("sentTable"),
  url: GET_ALL_SENT_NOTIFICATIONS_URL_PATH,
  collumns: [
    { 'data': "priority", 'render': renderPriority },
    {
      'data': function (data) {
        return data.subject + " " + data.content;
      },
      'render': function (a, b, data) {
        return "<div><em>" + escapeHtml(data.subject) + "</em> - " + escapeHtml(data.content) + "</div>";
      }
    },
    {
      'data': function (data) {
        return data.sent_time = new Date(data.sent_time);
      },
      'render': renderDate
    },
  ],
  rowCallback(row, data) {},
  previousBtn: document.getElementById('sentPreviousBtn'),
  nextBtn: document.getElementById('sentNextBtn'),
}

const detailViewControlBar = document.getElementById('detailViewControlBar');
const detailSideBar = document.getElementById('detailSideBar');
const detailContentPannel = document.getElementById('detailContentPannel');

const detailView = new NotificationView([
  detailViewControlBar,
  detailSideBar,
  detailContentPannel,
]);
{
  const detailContentContainer = document.getElementById('detailContentContainer');
  const detailAuthorContainer = document.getElementById('detailAuthorContainer');
  const detailReceiversContainer = document.getElementById('detailReceiversContainer');
  const detailSubjectContainer = document.getElementById('detailSubjectContainer');
  const detailViewActionButtons = document.getElementById('detailViewActionButtons');
  const detailDateContainer = document.getElementById('detailDateContainer');
  const replyPageBtn = document.getElementById('replyPageBtn');

  detailView.show = function() {
    this.__proto__.show.call(this);
    inboxSideBar.hidden = true;
  };
  detailView.hide = function() {
    this.__proto__.hide.call(this);
    inboxSideBar.hidden = false;
  };
  detailView.load = function() {

    if (!detailView.data) return detailView.showErrorPage();

    // set default window and hide error page
    detailContentPannel.children[0].hidden = false;
    detailContentPannel.children[1].hidden = true;
    detailViewActionButtons.hidden = false;

    detailContentContainer.innerText = this.data.content;
    detailDateContainer.innerText = new Date(this.data.sent_time);

    detailSubjectContainer.innerHTML = this.data.subject;
    detailReceiversContainer.innerText = this.data.receivers.map(r => r.name).join(", ");

    replyPageBtn.href = REPLY_PAGE__URL_PATH[0] + this.data.id + REPLY_PAGE__URL_PATH[1];

    if (this.data.sender.id === MY_ID) {
      detailAuthorContainer.innerText = "Eu";
    }
    else {
      detailAuthorContainer.innerText = this.data.sender.name;
    }

    if (this.data.priority) {
      detailSetPriorityBtn.hidden = true;
      detailResetPriorityBtn.hidden = false;
    } else {
      detailSetPriorityBtn.hidden = false;
      detailResetPriorityBtn.hidden = true;
    }

  };
  detailView.changeHistoryState = function() {
    history.pushState(this.data.id, "", "?view=" + this.data.id);
  }
  detailView.showErrorPage = function() {
    detailContentPannel.children[0].hidden = true;
    detailContentPannel.children[1].hidden = false;
    detailViewActionButtons.hidden = true;
    detailSideBar.hidden = true;
  }

}



// notification table click handle
$('#contentPannel').on('click', 'tr', function (event) {
  detailView.row = currentView.table.row(event.currentTarget);
  detailView.data = detailView.row.data();

  // ignore clicks that are not in a valid row
  // such as the loading row
  if (!detailView.data) return;

  if (currentView === receiveView) {
    $.ajax({
      url: MARK_AS_READ_URL_PATH[0] + detailView.data.id + MARK_AS_READ_URL_PATH[1],
      success() {
        detailView.data.seen = true;

        // remove ntfUnseen class from row
        detailView.row.node().classList.remove('ntfUnseen');
      }
    });
  }

  currentView.hide();
  detailView.show();
  detailView.load();
  detailView.changeHistoryState();

});


// MODE TOGGLE LOGIC
const modeToggle = document.querySelector('#ntfModeTabs ul');

modeToggle.children[0].addEventListener('click', function() {
  if (currentView === sentView) {
    this.classList.add("is-active");
    modeToggle.children[1].classList.remove("is-active");

    currentView.hide();
    currentView = receiveView;
    currentView.show();
    currentView.changeHistoryState();

    if (!currentView.loaded) {
      currentView.load();
    }
  }
});
modeToggle.children[1].addEventListener('click', function() {
  if (currentView === receiveView) {
    this.classList.add("is-active");
    modeToggle.children[0].classList.remove("is-active");

    currentView.hide();
    currentView = sentView;
    currentView.show();
    currentView.changeHistoryState();

    if (!currentView.loaded) {
      currentView.load();
    }
  }
});



// back button logic
const detailBackButton = document.getElementById('detailBackButton');
detailBackButton.addEventListener('click', function() {
  if (currentView) {
    history.back();
  } else {
    detailView.hide();
    currentView = receiveView;
    loadFirstListView();
  }
});

// refresh buttons
receiveView.refreshBtn = document.getElementById('receiveViewRefreshBtn');
sentView.refreshBtn = document.getElementById('sentViewRefreshBtn');

receiveView.refreshBtn.addEventListener('click', function() {
  // do nothing when alredy in action
  if (this.classList.contains('loading')) return;

  this.classList.add('loading');
  this.children[1].innerText = " a atualizar...";
  receiveView.table.ajax.reload();
});
sentView.refreshBtn.addEventListener('click', function() {
  // do nothing when alredy in action
  if (this.classList.contains('loading')) return;

  this.classList.add('loading');
  this.children[1].innerText = " a atualizar...";
  sentView.table.ajax.reload();
});


// priority buttons
const detailSetPriorityBtn = document.getElementById('detailSetPriorityBtn');
const detailResetPriorityBtn = document.getElementById('detailResetPriorityBtn');

detailSetPriorityBtn.addEventListener('click', function() {
  // no action if already processing
  if (this.classList.contains('is-loading')) return;

  const btn = this;
  btn.classList.add('is-loading');


  const xhr = new XMLHttpRequest();

  xhr.open("GET", SET_PRIORITY_URL_PATH[0] + detailView.data.id + SET_PRIORITY_URL_PATH[1]);

  xhr.onreadystatechange = function() {
    if (this.readyState !== 4) return;

    switch (this.status) {
      case 200:
        btn.hidden = true;
        btn.classList.remove('is-loading');
        detailResetPriorityBtn.hidden = false;
        detailView.data.priority = true;
        if (detailView.row) {
          detailView.row.data(detailView.data);
        }
        break;

      case 403:
      case 404:
        // this message is not suposed to be showing
        if (detailView.row) {
          detailView.row.remove().draw();
        }
        history.back();
    }
    btn.classList.remove('is-loading');
  }
  xhr.onerror = () => {
    btn.classList.remove('is-loading');
  };
  xhr.send();
});

detailResetPriorityBtn.addEventListener('click', function () {
  // no action if already processing
  if (this.classList.contains('is-loading')) return;

  const btn = this;
  btn.classList.add('is-loading');

  const xhr = new XMLHttpRequest();

  xhr.open("GET", RESET_PRIORITY_URL_PATH[0] + detailView.data.id + RESET_PRIORITY_URL_PATH[1]);

  xhr.onreadystatechange = function() {
    if (this.readyState !== 4) return;

    switch (this.status) {
      case 200:
        btn.hidden = true;
        btn.classList.remove('is-loading');
        detailSetPriorityBtn.hidden = false;
        detailView.data.priority = false;
        if (detailView.row) {
          detailView.row.data(detailView.data);
        }
        break;

      case 403:
      case 404:
        // this message is not suposed to be showing
        if (detailView.row) {
          detailView.row.remove().draw();
        }
        history.back();
    }
    btn.classList.remove('is-loading');
  }
  xhr.onerror = () => {
    btn.classList.remove('is-loading');
  };
  xhr.send();

});

// delete buttons
detailView.detailDeleteBtn = document.getElementById('detailDeleteBtn');
detailView.deleteConfirmBtn = document.getElementById('deleteConfirmBtn');

const deleteModalBody = document.querySelector('#deleteConfirmModal .modal-body');

detailView.detailDeleteBtn.addEventListener('click', function() {
  // check if is author
  if (detailView.data.sender.id === MY_ID) {
    deleteModalBody.children[1].hidden = true;
    deleteModalBody.children[2].hidden = false;
  } else {
    deleteModalBody.children[1].hidden = false;
    deleteModalBody.children[2].hidden = true;
  }
});

detailView.deleteConfirmBtn.addEventListener('click', function() {
  detailDeleteBtn.disabled = true;
  detailDeleteBtn.classList.add('is-loading');

  const xhr = new XMLHttpRequest();

  xhr.open("GET", DELETE_NOTIFICATION_URL_PATH[0] + detailView.data.id + DELETE_NOTIFICATION_URL_PATH[1]);

  xhr.onreadystatechange = function () {
    if (this.readyState !== 4) return;

    switch (this.status) {
      case 200:
        detailDeleteBtn.classList.remove('is-loading');
        detailDeleteBtn.disabled = false;

        if (detailView.row) {
          setTimeout(function() {
            detailView.row.remove().draw();
          }, 500);
        }
        history.back();
        break;

      case 403:
      case 404:
        // this message is not suposed to be showing
        if (detailView.row) {
          detailView.row.remove().draw();
        }
        history.back();
    }
    detailDeleteBtn.classList.remove('is-loading');
    detailDeleteBtn.disabled = false;
  }
  xhr.onerror = () => {
    btn.classList.remove('is-loading');
  };
  xhr.send();
});


let currentView = null;

// display right view acording to paths query
(function() {
  // show inbox sidebar for any view except detail
  inboxSideBar.hidden = false;

  const queryView = location.href.match(/view=(\w+|\d+)/);
  if (queryView) {
    if (queryView[1] === "sent") {
      currentView = sentView;
      return;
    }
    else if (queryView[1].match(/\d+/)) {
      // handle detail view directly
      inboxSideBar.hidden = true;
      detailView.show();
      const xhr = new XMLHttpRequest();
      xhr.open(
        "GET",
        GET_NOTIFICATION_DATA_URL_PATH[0] + queryView[1] + GET_NOTIFICATION_DATA_URL_PATH[1],
      )
      xhr.onreadystatechange = function() {
        if (this.readyState !== 4) return;

        switch (this.status) {
          case 200:
            detailView.data = JSON.parse(this.responseText);
            detailView.load();
            detailView.changeHistoryState();
            break;

          default:
            detailView.showErrorPage();
        }
      }
      xhr.send();
      return;
    }
  }

  // default startup, received list
  currentView = receiveView;
  currentView.changeHistoryState();
})();

// set back actions
window.onpopstate = function (e) {
  if (history.state) {
    detailView.hide();
    if (!currentView) {
      currentView = receiveView;
    }
    currentView.show();
    if (!currentView.loaded) {
      currentView.load();
    }
  }
};

function loadFirstListView() {
  currentView.show();
  modeToggle.children[Number(currentView === sentView)].classList.add("is-active");

  if (!currentView.loaded) {
    currentView.load();
    currentView.changeHistoryState();
  }
}

$(document).ready(function () {
  if (currentView) {
    loadFirstListView();
  }
});
