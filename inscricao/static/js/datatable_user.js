/**
 * File that creates datatables and also has the filter functions and other functionalities
 *
 * @author André Brito, Bruno Mendes, Leandro Quintans, Teresa Goela
 */


/**
 *
 * chooses which kind of datatable you wanna start when the page loads
 */

$(document).ready(function() {
    if ($('#activitytable_coord').length > 0)
        datatable_coord();
    else if ($('#activitytable_admin').length > 0)
        datatable_admin()
    else if ($('#activitytable_all').length > 0)
        datatable_all()
    else
        datable_teacher()


});


/**
 *
 * @param {string} data value from a collumn
 * @param {string} value value that you wanna compare with
 * @return true if values are equal
 */
function get_selected_value(data, value) {
    if (!value)
        return true
    return data.toLowerCase().indexOf(value.toLowerCase()) >= 0 || !isNaN(value)
}

/**
 * function AND with any ammount of parameters
 * @param  {...boolean} input - list of booleans
 * @return false if there is any false in the parameters, true otherwise
 */
function get_return_value(...input) {
    // console.log(input.some(entry => entry === false))
    // console.log(input)
    return !input.some(entry => entry === false)
}


/**
 * check https://stackoverflow.com/questions/5450104/using-jquery-to-get-all-checked-checkboxes-with-a-certain-class-name
 * finds all the checked checkboxes
 * @returns all the checked checkboxes
 */
function get_checked_checkboxes() {
    var result = []
    $('input:checkbox').each(function() {
        var sThisVal = (this.checked ? $(this).val() : "");
        if (sThisVal)
            result.push(sThisVal)
    });
    return result;
}


/**
 *
 * @param {*} table table which this list of functions will be applied to
 */
function table_functions(table) {
    /**
     * extends the function search from the datatable
     * https://datatables.net/
     * https://datatables.net/reference/api/search()
     */
    $.fn.dataTable.ext.search.push(
        /**
         * decides if the row should get drawed or not
         * @param {*} settings list of settings
         * @param {*} data data of all collumns
         * @param {*} dataIndex row index
         * @returns true if contains all filters, false otherwise
         */
        function(settings, data, dataIndex) {

            //values of the filter fields (if they exist)
            var status = $('#status').val()
            var o_u = $('#org_unit').val()
            var t_list = $('#t_list').val()
            var campus = $('#campus-filter').val()
            var max_par = $('#max_par_filter').val()
            var a_name = $('#activity-name-filter').val()

            // result variables
            var result_from_time_filter,
                result_status_filter,
                result_ou_filter,
                result_teacher_filter,
                result_day_filter,
                result_name_filter;


            // this vars exist in all views
            result_day_filter = filter_session_day(dataIndex, data)
            result_from_time_filter = start_time_change_filter(dataIndex, data);
            result_name_filter = data[0].toLowerCase().includes(a_name.toLowerCase())

            // view for everyone :)
            if ($('#activitytable_all').length > 0) {
                return get_return_value(result_from_time_filter,
                    get_selected_value(data[1], o_u),
                    get_selected_value(data[3], campus),
                    result_name_filter,
                    result_day_filter,
                    data[2] >= max_par,
                    themes_list_filter(dataIndex, data, get_checked_checkboxes()),
                )
            } else {
                result_status_filter = get_selected_value(data[2], status)
                result_ou_filter = get_selected_value(data[3], o_u)
                if ($('#activitytable_admin').len === 0)
                    result_teacher_filter = get_selected_value(data[3], t_list)
                else
                    result_teacher_filter = get_selected_value(data[4], t_list)

            }

            // because the admin and coord table submission date, then we have to send this as an argument
            if ($('.submission-pick').length > 0) {
                var result_sub_range = sub_range_filter(dataIndex, data);
                return get_return_value(result_from_time_filter,
                    result_status_filter,
                    result_ou_filter,
                    result_day_filter,
                    result_sub_range,
                    result_teacher_filter,
                    result_name_filter)
            } else
                return get_return_value(result_from_time_filter,
                    result_status_filter,
                    result_ou_filter,
                    result_day_filter,
                    result_teacher_filter,
                    result_name_filter)
        }
    );

    /**
     *
     * @param {*} dataIndex index of the row
     * @param {*} data info of the row
     */
    function sub_range_filter(dataIndex, data) {
        let start_date = $('.submission-pick .is-datetimepicker-range').val()
        let end_date = $('.submission-pick .datetimepicker-dummy-input:last').val()
        if (start_date === '' && end_date === '')
            return true
        let value = $(table.row(dataIndex).data()[1]).attr('name')
        return value >= start_date && value <= end_date
    }


    /**
     * filters by status
     */
    $('#status').change(function() {
        table.draw();
    });

    /**
     * filters by Organic Unit
     */
    $('#org_unit').change(function() {
        table.draw();
    })

    /**
     * filters by start time
     */
    $('#fromtime').change(() => {
        table.draw()
    })

    /**
     * filters by end time
     */
    $('#tilltime').change(() => {
        table.draw()
    })

    /**
     * filters by campus
     */
    $('#campus-filter').change(() => {
        table.draw()
    })

    /**
     * filters by input num + 1
     */
    $('#increase_num').on('click', () => {
        table.draw()
    })

    /**
     * filters by input num - 1
     */
    $('#decrease_num').on('click', () => {
        table.draw()
    })

    /**
     * filters by checkbox change
     */
    $('input:checkbox').change(() => {
        table.draw()
    })

    $('#activity-name-filter').keyup(() => {
        table.draw()
    })

    $('#t_list').change(() => {
        table.draw()
    })


    /**
     *   this is an example how detect value change on bulma calendars
     *   it aint pretty but it works, no clue where i stole this from
     *   but thank you kind stranger
     */

    // To access to bulmaCalendar instance of an element
    var element = document.querySelector('#date-session-filter');
    if (element) {
        // bulmaCalendar instance is available as element.bulmaCalendar
        element.bulmaCalendar.on('select', function(datepicker) {
            table.draw()
        });
    }

    var calendar_sub = document.querySelector('#sub_calendar')
    if (calendar_sub)
        calendar_sub.bulmaCalendar.on('select', function(datepicker) {
            table.draw();
        })


    $('.datetimepicker-clear-button').click((index, value) => {
        table.draw()
    })

    /**
     * clears filters
     */
    $('#clear-filters').click(() => {
        $('#status').val('')
        $('#org_unit').val('')
        $('#t_list').val('')
        $('#fromtime').val('')
        $('#tilltime').val('')
        $('#max_par_filter').val('')
        $('#campus-filter').val('')
        $('.datetimepicker-dummy-input').each(function() {
            $(this).val('')
        })
        $('input:checkbox').each(function() {
            $(this)[0].checked = false
        })
        $('#activity-name-filter').val('')
        table.draw();
    })


    /**
     * filters by day
     * @param {*} dataIndex collumn with the index
     * @param {*} data  data of the column
     * @returns true if activity on this row contains that day, false otherwise
     */
    function filter_session_day(dataIndex, data) {
        var start_date = $('.range-pick .datetimepicker-dummy-input').val()
        if (!start_date)
            return true;
        var list_of_all_values = []
        var table_time_val;
        if ($('#activitytable_admin').length > 0)
            table_time_val = $($(table.row(dataIndex).data()[5])).find('#session-day')
        else
            table_time_val = $($(table.row(dataIndex).data()[4])).find('#session-day')
        $.each(table_time_val, (index, date_i) => {
            return list_of_all_values.push($(date_i)[0].innerHTML.slice(0, 2))
        })
        return list_of_all_values.includes(start_date.slice(-2))
    }

    /**
     * filters by themes
     * @param {*} dataIndex collumn with the index
     * @param {*} data  data of the column
     * @returns true if activity on this row all the themes, false otherwise
     */
    function themes_list_filter(dataIndex, data, theme_list) {
        var var_themes = $(table.row(dataIndex).data()[4]).find('#activity-themes .theme')
        var themes_from_act = []
        $.each($(var_themes), (index, value) => {
            themes_from_act.push($(value).text())
        })
        var result = true
        $.each(theme_list, (index, value) => {
            if (!themes_from_act.includes(value)) {
                result = false;
                return false;
            }
        })
        return result;
    }

    /**
     * filters by time
     * @param {*} dataIndex collumn with the index
     * @param {*} data  data of the column
     * @returns true if activity on this row contains that has any session that is between time_start and time_end, false otherwise
     */
    function start_time_change_filter(dataIndex, data) {
        var start_time = $('#fromtime').val()
        var end_time = $('#tilltime').val()
        if (end_time === '' && start_time === '')
            return true;
        if (start_time !== '' || end_time !== '') {
            if ($('#activitytable_admin').length > 0)
                var table_time_val = $(table.row(dataIndex).data()[5]).find('#time-check')
            else
                var table_time_val = $(table.row(dataIndex).data()[4]).find('#time-check')

            var list_of_all_values = []
            $.each(table_time_val, (index, time_i) => {
                return list_of_all_values.push($(time_i)[0].attributes['value'].value)
            })

            // if start time and end_time
            if (start_time !== '' && end_time === '')
                if ($(list_of_all_values).get(-1) > start_time)
                    return true;
                else
                    return false;
            else if (start_time === '' && end_time !== '')
                if ($(list_of_all_values).get(-1) < end_time)
                    return true;
                else
                    return false;
            else {
                return list_of_all_values.reduce((r, a) => r + (a >= start_time && a <= end_time), 0) > 0;
            }

        } else
            return true
    }

    $('div.dataTables_length select').addClass('form-control d-inline-flex w-auto');

}

/**
 * loads datatable for activity all view
 */
function datatable_all() {
    var collapsedGroups = {};
    // initiates a datatable
    $('#activitytable_all').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            "emptyTable": "Não foi encontrado nenhum registo",
            "loadingRecords": "A carregar...",
            "processing": "A processar...",
            "lengthMenu": "Mostrar _MENU_ registos",
            "zeroRecords": "Não foram encontrados resultados",
            "info": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            "infoEmpty": "Mostrando de 0 até 0 de 0 registos",
            "infoFiltered": "(filtrado de _MAX_ registos no total)",
            "infoPostFix": "",
            "search": "Procurar:",
            "url": "",
            "aria": {
                "sortAscending": ": Ordenar colunas de forma ascendente",
                "sortDescending": ": Ordenar colunas de forma descendente"
            },
            paginate: {
                previous: '<i class="fas fa-chevron-left fa-lg"></i>',
                next: '<i class="fas fa-chevron-right fa-lg"></i>',
            }
        },
        columnDefs: [{
                "targets": [4],
                "orderable": false
            },
            {
                width: "20%",
                "targets": [2],
            },
            {
                width: "30%",
                targets: [0, 1, 3]
            }
        ],
    });


    var table = $('#activitytable_all').DataTable();
    $('#activitytable_all_filter').remove()
    table_functions(table)
}

/**
 * loads datatable for activity admin view
 */
function datatable_admin() {
    var collapsedGroups = {};

    // initiates a datatable
    $('#activitytable_admin').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            "emptyTable": "Não foi encontrado nenhum registo",
            "loadingRecords": "A carregar...",
            "processing": "A processar...",
            "lengthMenu": "Mostrar _MENU_ registos",
            "zeroRecords": "Não foram encontrados resultados",
            "info": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            "infoEmpty": "Mostrando de 0 até 0 de 0 registos",
            "infoFiltered": "(filtrado de _MAX_ registos no total)",
            "infoPostFix": "",
            "search": "Procurar:",
            "url": "",
            "aria": {
                "sortAscending": ": Ordenar colunas de forma ascendente",
                "sortDescending": ": Ordenar colunas de forma descendente"
            },
            paginate: {
                previous: '<i class="fas fa-chevron-left fa-lg"></i>',
                next: '<i class="fas fa-chevron-right fa-lg"></i>',
            }
        },
        columnDefs: [{
                // "targets": [4],
                // "orderable": false
            },
            {
                "targets": [0, 1, 2],
            }
        ],
    });

    var table = $('#activitytable_admin').DataTable();
    $('#activitytable_admin_filter').remove()
    table_functions(table)
}

/**
 * loads datatable for activity coordinator view
 */
function datatable_coord() {
    var collapsedGroups = {};

    // initiates a datatable
    $('#activitytable_coord').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            "emptyTable": "Não foi encontrado nenhum registo",
            "loadingRecords": "A carregar...",
            "processing": "A processar...",
            "lengthMenu": "Mostrar _MENU_ registos",
            "zeroRecords": "Não foram encontrados resultados",
            "info": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            "infoEmpty": "Mostrando de 0 até 0 de 0 registos",
            "infoFiltered": "(filtrado de _MAX_ registos no total)",
            "infoPostFix": "",
            "search": "Procurar:",
            "url": "",
            "aria": {
                "sortAscending": ": Ordenar colunas de forma ascendente",
                "sortDescending": ": Ordenar colunas de forma descendente"
            },
            paginate: {
                previous: '<i class="fas fa-chevron-left fa-lg"></i>',
                next: '<i class="fas fa-chevron-right fa-lg"></i>',
            }
        },
        columnDefs: [{
                "targets": [4],
                "orderable": false
            },
            {
                "targets": [0, 1, 2],
            }
        ],
    });

    $('#activitytable_coord_filter').remove()
    var table = $('#activitytable_coord').DataTable();
    table_functions(table)

}


/**
 * loads datatable for activity teacher view
 */
function datable_teacher() {
    // collapse group
    var collapsedGroups = {};

    // initiates a datatable
    $('#activitytable').DataTable({
        // responsive
        responsive: true,
        // search field, gonna remove later
        language: {
            "emptyTable": "Não foi encontrado nenhum registo",
            "loadingRecords": "A carregar...",
            "processing": "A processar...",
            "lengthMenu": "Mostrar _MENU_ registos",
            "zeroRecords": "Não foram encontrados resultados",
            "info": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            "infoEmpty": "Mostrando de 0 até 0 de 0 registos",
            "infoFiltered": "(filtrado de _MAX_ registos no total)",
            "infoPostFix": "",
            "search": "Procurar:",
            "url": "",
            "aria": {
                "sortAscending": ": Ordenar colunas de forma ascendente",
                "sortDescending": ": Ordenar colunas de forma descendente"
            },
            paginate: {
                previous: '<i class="fas fa-chevron-left fa-lg"></i>',
                next: '<i class="fas fa-chevron-right fa-lg"></i>',
            }
        },
        columnDefs: [{
                "targets": [3],
                "orderable": false
            },
            {
                "width": "30%",
                "targets": [0, 1, 2],
            }
        ],
    });

    var table = $('#activitytable').DataTable();
    $('#activitytable_filter').remove()
    table_functions(table)
}