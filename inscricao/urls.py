from django.contrib import admin
from django.urls import path
from .views import (viewInscricao, viewInscricao2,home, viewInscricao3, viewInscricao4, viewInscricao5, viewInscricao6)

urlpatterns = [
    path('', home, name = "home"),
    path('viewInscricao', viewInscricao, name = 'viewInscricao'),
    path('viewInscricao/<int:pk_test>/', viewInscricao2, name = 'idnome'),
    path('viewInscricao2/<str:pk_test>/', viewInscricao3, name = 'idnome2'),
    path('viewInscricao3/<str:pk_test>/', viewInscricao4, name = 'idnome3'),
    path('viewInscricao4/<str:pk_test>/', viewInscricao5, name = 'idnome4'),
    path('viewInscricao5/<str:pk_test>/', viewInscricao6, name = 'idnome5'),
    # path('inscricao', inscricao.as_view(FORMS), name = 'inscricao'),
    # path('InscricaoUpdateView', InscricaoUpdateView.as_view(), name = 'InscricaoUpdateView'),
]

