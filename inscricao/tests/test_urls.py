from django.test import SimpleTestCase
from django.urls import reverse, resolve
from inscricao.views import viewInscricao, viewInscricao2, viewInscricao3, viewInscricao4, viewInscricao5, viewInscricao6, home

class TestUrls(SimpleTestCase):
    def test_list_url_is_resolved(self):
        url = reverse('viewInscricao')
        print(resolve(url))
        self.assertEquals(resolve(url).func, viewInscricao)

        url = reverse('idnome', args=['1'])
        self.assertEquals(resolve(url).func, viewInscricao2)

        url = reverse('idnome2', args=['1'])
        self.assertEquals(resolve(url).func, viewInscricao3)

        url = reverse('idnome3', args=['1'])
        self.assertEquals(resolve(url).func, viewInscricao4)

        url = reverse('idnome4', args=['1'])
        self.assertEquals(resolve(url).func, viewInscricao5)

        url = reverse('idnome5', args=['1'])
        self.assertEquals(resolve(url).func, viewInscricao6)