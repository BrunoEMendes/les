from django.test import TestCase
from inscricao.models import *
from users.models import *

class TestModels(TestCase):
    
    def test_subscription(self):
        school1 = School.objects.create(name="Faro", address="Faro", zip="12345", city="Faro")
        participant1 = Participant.objects.create(school_year="2", user=User.objects.create(username="Rafael", password="Teste12345", email="naosei@naosei.com", first_name="Rafael", last_name="Gomes"), area="Ciencias")
        responsavel1 = Responsavel.objects.create(name="Rafael", email="naosei@naosei.com", phone="123456789")
        bd = Subscription.objects.create(date=date(2021,6,10), school=school1, participant=participant1, responsável=responsavel1)

        self.assertEquals(Subscription.objects.filter(date=date(2021,6,10), school=school1, participant=participant1, responsável=responsavel1).exists(), False)
        school1.delete()
        participant1.delete()
        responsavel1.delete()
        bd.delete()


    def test_school(self):
        school1 = School.objects.create(name="Faro", address="Faro", zip="12345", city="Faro")
        self.assertEquals(School.objects.filter(name="Faro", address="Faro", zip="12345", city="Faro").exists(), True)
        school1.delete()

    def test_responsavel(self):
        responsavel1 = Responsavel.objects.create(name="Rafael", email="naosei@naosei.com", phone="123456789")
        self.assertEquals(Responsavel.objects.filter(name="Rafael", email="naosei@naosei.com", phone="123456789").exists(), True)
        responsavel1.delete()

    def test_mealAmount(self):
        school1 = School.objects.create(name="Faro", address="Faro", zip="12345", city="Faro")
        participant1 = Participant.objects.create(school_year="2", user=User.objects.create(username="Rafael", password="Teste12345", email="naosei@naosei.com", first_name="Rafael", last_name="Gomes"), area="Ciencias")
        responsavel1 = Responsavel.objects.create(name="Rafael", email="naosei@naosei.com", phone="123456789")
        sub1 = Subscription.objects.create(date=date(2021,6,10), school=school1, participant=participant1, responsável=responsavel1)

        mealamount1 = MealAmmount.objects.create(sub=sub1, qt_normal="2", qt_economic="3")

        self.assertEquals(MealAmmount.objects.filter(sub=sub1, qt_normal="2", qt_economic="3").exists(), True)

        school1.delete()
        participant1.delete()
        responsavel1.delete()
        sub1.delete()
        mealamount1.delete()


#falta os test_models para o subscription session activity