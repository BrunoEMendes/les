import django_filters
from django_filters import DateFilter
from users.models import *
from coordenadores.models import *
from django.forms import *
from .models import *

class ColaboratorFilter(Form):
    datas =[('','Todos os dias')] + [(schedule.date,schedule.date) for schedule in Schedule_Colaborator.objects.all()]
    datas_unique = []
    for key, value in datas:
        tuple_var = (key,value)
        if tuple_var not in datas_unique:
            datas_unique.append(tuple_var)
    schedule=ChoiceField(label='',choices=datas_unique, required=False, widget=Select(attrs={'class':'input'}))
    ou=ChoiceField(label='Unidade Orgânica', 
            choices =[('','Unidade Orgânica')] + [(dep.id,dep.ou.name) for dep in Department.objects.all()],
            required=False,
            widget=Select(attrs={'class':'input'}))
    department=ChoiceField(label='Departamento',
            choices =[('','Departamento')] + [(dep.id,dep.name) for dep in Department.objects.all()],
            required=False,
            widget=Select(attrs={'class':'input'}))
    def qs(self):
        #parent = super().qs
        data = self.data.get('schedule')
        if data == '' or data is None:
            data = Colaborator.objects.all()
        else:
            data = Colaborator.objects.filter(schedule__date=data)
        dep = self.data.get('department')
        if dep == '' or dep is None:
            dep = Colaborator.objects.all()
        else:
            dep = Colaborator.objects.filter(department=dep)
        ou=self.data.get('ou')
        if ou =='' or ou is None:
            ou=Colaborator.objects.all()
        else:
            ou=Colaborator.objects.filter(department__ou=ou)
        return data & dep & ou