from django.db import models
from atividades.models import DaySessionActivity
from config_dia_aberto.models import OwnTransport, UniTransport
from django.contrib.auth.models import User
from users.models import Participant, Participant_Group
from config_dia_aberto.models import Menu
from datetime import date
# Create your models here.

class School(models.Model):
    name = models.CharField(max_length = 32, verbose_name = 'nome')
    address = models.CharField(max_length = 64, verbose_name = 'morada')
    #change later to zip-field or something
    zip = models.IntegerField(verbose_name = 'código postal')
    city = models.CharField(max_length = 32, verbose_name = 'localização')
    def __str__(self):
        return f'{self.name} | {self.city}'

    class Meta:
        verbose_name_plural = 'Escolas'


class Responsavel(models.Model):
    name = models.CharField(max_length = 64, verbose_name = 'responsável')
    email = models.EmailField()
    phone = models.IntegerField(verbose_name = 'contacto')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'Responsável'

class Subscription(models.Model):
    date = models.DateField(verbose_name = 'data', auto_now_add=date.today())
    school = models.ForeignKey(School, on_delete = models.CASCADE, verbose_name = 'escola')
    #mudar para participante
    participant = models.ForeignKey(Participant, on_delete = models.CASCADE, verbose_name = 'participante')
    responsável  = models.ForeignKey(Responsavel, on_delete = models.CASCADE, verbose_name = 'responsavel')


    def __str__(self):
        return f'{self.participant} | {self.school}'

    class Meta:
        verbose_name_plural = 'Inscrições'


class SubscriptionSessionAtivitiy(models.Model):

    n_students = models.IntegerField(verbose_name = 'nº estudantes')
    session_activity = models.ForeignKey(DaySessionActivity, on_delete = models.CASCADE, verbose_name = 'sessão da atividade')
    sub = models.ForeignKey(Subscription, on_delete = models.CASCADE, verbose_name = 'inscrição')
    uni_transport = models.ForeignKey(UniTransport, on_delete = models.CASCADE, verbose_name = 'transporte universitário', blank = True, null = True)
    own_transport = models.ForeignKey(OwnTransport, on_delete = models.CASCADE, verbose_name = "transporte próprio", blank = True, null = True)

    def __str__(self):
        return f'{self.sub}|{self.session_activity}|{self.n_students}'

    class Meta:
        verbose_name_plural = 'Inscrições em Sessões de Atividades'

class MealAmmount(models.Model):
    sub = models.ForeignKey(Subscription, on_delete = models.CASCADE)
    qt_normal = models.IntegerField()
    qt_economic = models.IntegerField()

    def __str__(self):
        return f'{self.sub}|{self.qt_normal}|{self.qt_economic}'

    class Meta:
        verbose_name_plural = 'Quantidade de Refeições'
