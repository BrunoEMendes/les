from django.contrib import admin
from .models import School, Subscription, SubscriptionSessionAtivitiy, Responsavel

# Register your models here.

admin.site.register(School)
admin.site.register(SubscriptionSessionAtivitiy)
admin.site.register(Subscription)
admin.site.register(Responsavel)

