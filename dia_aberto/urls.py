"""dia_aberto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
from .sitemap import StaticViewSitemap
from django.conf.urls import url


sitemaps = {
    'static': StaticViewSitemap,
}

urlpatterns = [

    path('admin/', admin.site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('sitemap.xml', sitemap, {'sitemaps':sitemaps}),
    path('', include('inscricao.urls')),
    path('', include('users.urls')),
    path('atividades/', include('atividades.urls')),
    path('', include('config_dia_aberto.urls')),
    path('notificacoes/', include('notificacoes.urls')),
    path('', include('coordenadores.urls')),
    path('colaboradores/', include('colaboradores.urls')),
]
