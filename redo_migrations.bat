:: Name: redo_migrations.bat
:: Puporse: to reset the django db and its migrations without losing the data
:: Author: Bruno Mendes a62181
:: Revision: end of Febuary 2020 - initial version
::           20 March 2020 - second version
::                         * now deletes the db
::                         * saves the data in a json file
::                         * loads the data into the db after migrating
::           19 June 2020 - 3rd version
::                         * no longer saves data to a json file
::                         * no longer loads the data after migrating
::                         * doesnt run the server after redoing the db

@ECHO OFF

:: checks if folder exists, if it doesnt creates it
:: if not exist "fixtures\" mkdir fixtures

:: saves all the data into a json file
:: ECHO saving all your database database
:: python manage.py dumpdata --format=json --all > fixtures/dummy_data.json

:: deletes the db
ECHO Deleting database
python manage.py reset_db

:: deletes all the migrations related files
ECHO Deleting all migration folders
FOR /d /r . %%d IN ("migrations") DO @IF EXIST "%%d" rd /s /q "%%d"

:: makes new migrations
ECHO Creating new migrations..
python manage.py makemigrations
python manage.py makemigrations config_dia_aberto  atividades colaboradores coordenadores inscricao notificacoes university users

:: migrates into the db
ECHO Migrating
python manage.py migrate

:: loads the data into the new db
:: ECHO loading fixtures
:: python manage.py loaddata fixtures/dummy_data.json

:: starts server
:: python manage.py runserver
