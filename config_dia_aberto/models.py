from datetime import date, datetime, timedelta

from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from utils.mixins import SingleInstanceMixin



class Config(SingleInstanceMixin, models.Model):
    """ Contains the information required for Dia Aberto """

    title = models.CharField(max_length=32, verbose_name='título')
    description = models.TextField(verbose_name='descrição')
    start_datetime = models.DateTimeField(verbose_name='início do Dia Aberto')
    end_datetime = models.DateTimeField(verbose_name='fim do Dia Aberto')
    activity_limit_subscription_start_datetime = models.DateTimeField(verbose_name='início do prazo de proposição de atividades')
    activity_limit_subscription_end_datetime = models.DateTimeField(verbose_name='fim do prazo de proposição de atividades')
    participant_limit_subscription_start_datetime = models.DateTimeField(verbose_name='início do prazo de inscrição de participantes')
    participant_limit_subscription_end_datetime = models.DateTimeField(verbose_name='fim do prazo de inscrição de participantes')

    # def clean(self, *args, **kwargs):
    #     super().clean(*args, **kwargs)
    #     validation_errors = {}
    #     if self.end_datetime <= self.start_datetime:
    #         validation_errors['end_datetime'] = ValidationError(_('O fim do Dia Aberto tem que ser depois do início do Dia Aberto.'), code='invalid')
    #     if self.activity_limit_subscription_end_datetime <= self.activity_limit_subscription_start_datetime:
    #         validation_errors['activity_limit_subscription_end_datetime'] = ValidationError(_('O fim da proposição de atividades tem que ser depois do início da proposição de atividades.'), code='invalid')
    #     if self.participant_limit_subscription_end_datetime <= self.participant_limit_subscription_start_datetime:
    #         validation_errors['participant_limit_subscription_end_datetime'] = ValidationError(_('O fim da inscrição ao Dia Aberto tem que ser depois do início da inscrição ao Dia Aberto.'), code='invalid')
    #     if validation_errors:
    #         raise ValidationError(validation_errors)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if hasattr(self, 'configactivity'):
            self.configactivity.update_sessiondays()

    def __str__(self):
        return f'Título: {self.title}'

    def get_absolute_url(self):
        return reverse('config_dia_aberto:config')

    def is_within_activity_limit(self, datetime_arg):
        return self.activity_limit_subscription_start_datetime <= datetime_arg <= self.activity_limit_subscription_end_datetime

    def is_within_activity_datetime_limit(self):
        # i need this, dont touch it again :( - lolrekt, touched again
        return self.is_within_activity_limit(timezone.now())

    def is_within_participant_limit(self, datetime_arg):
        return self.participant_limit_subscription_start_datetime <= datetime_arg <= self.participant_limit_subscription_end_datetime

    def is_now_within_participant_limit(self):
        return self.is_within_participant_limit(timezone.now())

    def is_within_dia_aberto(self, datetime_arg):
        return self.start_datetime <= datetime_arg <= self.end_datetime

    def is_now_within_dia_aberto(self):
        return self.is_within_dia_aberto(timezone.now())

    def has_participant_sub_started(self):
        return self.participant_limit_subscription_start_datetime <= timezone.now()

    def has_activity_sub_started(self):
        return self.activity_limit_subscription_start_datetime <= timezone.now()

    def has_dia_aberto_started(self):
        return self.start_datetime <= timezone.now()

    class Meta:
        verbose_name = 'Configuração do Dia Aberto'
        verbose_name_plural = 'Configurações do Dia Aberto'


class Contact(models.Model):
    config = models.ForeignKey(Config, on_delete=models.CASCADE, verbose_name='Dia Aberto')

    def is_contact_email(self):
        return hasattr(self, 'contactemail')

    def is_contact_telephone(self):
        return hasattr(self, 'contacttelephone')

    def is_contact_locale(self):
        return hasattr(self, 'contactlocale')

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'


class ContactEmail(Contact):
    email = models.EmailField(verbose_name='email', unique=True)

    class Meta:
        verbose_name = 'Contacto Correio Eletrónico'
        verbose_name_plural = 'Contactos Correio Eletrónicos'

    def get_absolute_url(self):
        return reverse('config_dia_aberto:config')


class ContactTelephone(Contact):
    telephone = models.CharField(max_length=20, verbose_name='telefone', unique=True)

    class Meta:
        verbose_name = 'Contacto Telefónico'
        verbose_name_plural = 'Contactos Telefónicos'

    def get_absolute_url(self):
        return reverse('config_dia_aberto:config')


class ContactLocale(Contact):
    address = models.CharField(max_length=100, verbose_name='morada', unique=True)
    region = models.CharField(max_length=50, verbose_name='região')
    # zip_code = models.CharField(max_length=8, verbose_name='código postal')

    class Meta:
        verbose_name = 'Contacto Localização'
        verbose_name_plural = 'Contactos Localização'

    def get_absolute_url(self):
        return reverse('config_dia_aberto:config')


# -------------------------TRANSPORT--------------------------------------------------

class Transport(models.Model):
    class TransportType(models.TextChoices):
        BUS = 'autocarro', _('Autocarro')
        TRAIN = 'comboio', _('Comboio')
        CAR = 'carro', _('Carro')

    config = models.ForeignKey(Config, on_delete=models.CASCADE, verbose_name='Dia Aberto')
    transport_type = models.CharField(
        max_length=10,
        choices=TransportType.choices,
        default=TransportType.BUS,
        verbose_name='tipo de transporte',
    )

    def save(self, *args, **kwargs):
        self.config = Config.objects.first()
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.id} {self.transport_type}'

    class Meta:
        verbose_name_plural = 'Transportes'


class OwnTransport(Transport):
    """ Information about how the participants are going to Dia Aberto on their own """
    arrival_date = models.TimeField(verbose_name='hora de chegada')
    departure_date = models.TimeField(verbose_name='hora de partida')

    class Meta:
        verbose_name = 'Transporte Próprio'
        verbose_name_plural = 'Transportes Próprios'


    def __str__(self):
        return f'Transporte: {self.transport_type} | hora de chegada: {self.arrival_date} | hora de partida: {self.departure_date}'


class UniTransport(Transport):
    """ Transport provided by the university """
    class UniTransportStatus(models.TextChoices):
        PUBLICADO = 'publicado', _('Publicado')
        POR_PUBLICAR = 'por publicar', _('Por publicar')

    class TransportLocation(models.TextChoices):
        PENHA = 'Penha', _('Penha')
        GAMBELAS = 'Gambelas', _('Gambelas')
        FARO = 'Terminal Rodoviário de Faro', _('Terminal Rodoviário de Faro')

    name_identifier = models.CharField(max_length=30, verbose_name='Identificador')
    from_location = models.CharField(
        max_length=30,
        choices=TransportLocation.choices,
        verbose_name='origem'
    )
    destination_location = models.CharField(
        max_length=30,
        choices=TransportLocation.choices,
        verbose_name='destino'
    )
    departure_datetime = models.DateTimeField(verbose_name='horário de partida')
    status = models.CharField(
        max_length=20,
        choices=UniTransportStatus.choices,
        default=UniTransportStatus.POR_PUBLICAR,
        verbose_name='estado',
    )

    def clean(self, *args, **kwargs):
        """ Checks to see if the chosen departure_datetime is within the dates configured in Config """
        super(UniTransport, self).clean(*args, **kwargs)
        validation_errors = {}

        end_datetime = Config.objects.first().end_datetime
        end_datetime = end_datetime.replace(hour=0, minute=0, second=0, microsecond=0)
        start_datetime = Config.objects.first().start_datetime
        start_datetime = start_datetime.replace(hour=0, minute=0, second=0, microsecond=0)

        if self.departure_datetime > end_datetime \
        and self.departure_datetime < start_datetime:
            validation_errors['departure_datetime'] = ValidationError(_('O horário de partida tem que estar dentro do intervalo do Dia Aberto.'), code='invalid')
        if validation_errors:
            raise ValidationError(validation_errors)

    def __str__(self):
        return f'Transporte Universitário número {self.name_identifier}'

    class Meta:
        verbose_name = 'Transporte da Universidade'
        verbose_name_plural = 'Transportes da Universidade'


# -------------------------END TRANSPORT--------------------------------------------------
# -------------------------MENU-----------------------------------------------------------

class ConfigMeal(SingleInstanceMixin, models.Model):
    normal_prof_price = models.DecimalField(max_digits=19, decimal_places=4, verbose_name='preço normal para docente',)
    normal_student_price = models.DecimalField(max_digits=19, decimal_places=4, verbose_name='preço normal para estudante',)
    eco_prof_price = models.DecimalField(max_digits=19, decimal_places=4, verbose_name='preço económico para docente',)
    eco_student_price = models.DecimalField(max_digits=19, decimal_places=4, verbose_name='preço económico para estudante',)

    class Meta:
        verbose_name = 'Configuração dos Almoços'

class Menu(models.Model):
    date = models.DateField(
        verbose_name='dia da refeição'
    )


    def getFormatedDate(self):
        return self.date.strftime("%d/%m/%Y")

    def __str__(self):
        return f'Menu {self.date.strftime("%d-%b-%Y")}'

    class Meta:
        verbose_name = 'Menu'


class Meal(models.Model):
    class MealType(models.TextChoices):
        SOUP = 'SOPA', _('sopa')
        CARNE = 'CARNE', _('prato de carne')
        PEIXE = 'PEIXE', _('prato de peixe')
        VEGETARIAN = 'VEG', _('prato vegetariano')
        SPECIAL = 'SPECI', _('prato especial')
        DRINK = 'BBIDA', _('bebida')
        DESSERT = 'SOBRE', _('sobremesa')

    name = models.CharField(max_length=32, verbose_name='nome')
    meal_type = models.CharField(
        max_length=5,
        choices=MealType.choices,
        verbose_name='tipo de opção'
    )
    description = models.TextField(
        verbose_name='descrição'
    )
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE, verbose_name='menu')

    class Meta:
        verbose_name = 'Opção'
        verbose_name_plural = 'Opções'

    def __str__(self):
        return f'{self.meal_type} {self.name} | {self.menu.date.strftime("%d-%b-%Y")}'
