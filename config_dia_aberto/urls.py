from django.urls import path

from . import views

app_name = 'config_dia_aberto'
urlpatterns = [
    # path('config/', views.ConfigListView.as_view(), name='config-list'),
    path('config/', views.config_detail_view, name='config'),
    path('config/criar/', views.ConfigCreateView.as_view(), name='config-create'),
    path('config/alterar/', views.config_update_view, name='config-update'),
    # path('config/<int:pk>/delete/', views.ConfigDeleteView.as_view(), name='config-delete'),


    path('contactos/criar/', views.contact_create_view, name='contact-create'),

    path('contactos/email/<int:pk>/alterar/', views.ContactEmailUpdateView.as_view(), name='contact-email-update'),
    path('contactos/email/<int:pk>/eliminar/', views.ContactEmailDeleteView.as_view(), name='contact-email-delete'),

    path('contactos/telefone/<int:pk>/alterar/', views.ContactTelephoneUpdateView.as_view(), name='contact-telephone-update'),
    path('contactos/telefone/<int:pk>/eliminar/', views.ContactTelephoneDeleteView.as_view(), name='contact-telephone-delete'),

    path('contactos/localizacao/<int:pk>/alterar/', views.ContactLocaleUpdateView.as_view(), name='contact-locale-update'),
    path('contactos/localizacao/<int:pk>/eliminar/', views.ContactLocaleDeleteView.as_view(), name='contact-locale-delete'),

    path('transportes/', views.UniTransportListView.as_view(), name='transport-list'),
    # path('transport/<int:pk>/', views.UniTransportDetailView.as_view(), name='transport-detail'),
    path('transportes/criar/', views.UniTransportCreateView.as_view(), name='transport-create'),
    path('transportes/<int:pk>/alterar/', views.UniTransportUpdateView.as_view(), name='transport-update'),
    path('transportes/<int:pk>/eliminar/', views.UniTransportDeleteView.as_view(), name='transport-delete'),
    path('transportes/publicar/', views.UniTransportPublishView.as_view(), name='transport-publish'),


    path('refeicoes/', views.MealListView.as_view(), name='meal-list'),
    path('refeicoes/criar/', views.MenuCreateView.as_view(), name='meal-create'),
    path('refeicoes/<int:pk>/alterar/', views.MenuUpdateView.as_view(), name='meal-update'),
    path('refeicoes/<int:pk>/eliminar/', views.delete_meal, name='meal-delete'),
    path('refeicoes/precos/', views.delete_meal, name='meal-prices'),


    #config meals
    path('refeicoes/config/', views.ConfigMealListView.as_view(), name="config-meal-list"),
    path('refeicoes/config/criar/', views.ConfigMealCreateView.as_view(), name="config-meal-create"),
    path('refeicoes/config/update/', views.ConfigActivityUpdateView.as_view(), name="config-meal-update"),
]
