from django.apps import AppConfig


class ConfigDiaAbertoConfig(AppConfig):
    name = 'config_dia_aberto'
