from django import forms
from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _
from django.forms import inlineformset_factory
from django.db.utils import ProgrammingError
from django.core.exceptions import ValidationError


from bootstrap_datepicker_plus import DateTimePickerInput

# from dateutil.relativedelta import relativedelta
# from crispy_forms.helper import FormHelper
from intl_tel_input.widgets import IntlTelInputWidget
from localflavor.pt.forms import PTZipCodeField
from localflavor.pt import pt_regions

from .models import (
    Config,
    ConfigMeal,
    Contact,
    ContactEmail,
    ContactTelephone,
    ContactLocale,
    UniTransport,
    Menu,
    Meal,
)


class ConfigForm(forms.ModelForm):
    class Meta:
        model = Config
        fields = [
            'title', 'description',
            'start_datetime', 'end_datetime',
            'activity_limit_subscription_start_datetime',
            'activity_limit_subscription_end_datetime',
            'participant_limit_subscription_start_datetime',
            'participant_limit_subscription_end_datetime'
        ]

    def clean(self, *args, **kwargs):
        cleaned_data = super().clean()
        validation_errors = {}
        if cleaned_data.get('end_datetime') <= cleaned_data.get('start_datetime'):
            validation_errors['end_datetime'] = ValidationError(_('O fim do Dia Aberto tem que ser depois do início do Dia Aberto.'), code='invalid')
        if cleaned_data.get('activity_limit_subscription_end_datetime') <= cleaned_data.get('activity_limit_subscription_start_datetime'):
            validation_errors['activity_limit_subscription_end_datetime'] = ValidationError(_('O fim da proposição de atividades tem que ser depois do início da proposição de atividades.'), code='invalid')
        elif cleaned_data.get('activity_limit_subscription_end_datetime') > cleaned_data.get('participant_limit_subscription_start_datetime'):
            validation_errors['activity_limit_subscription_end_datetime'] = ValidationError(_('O período de proposição de atividades não pode terminar depois do começo das inscrições ao Dia Aberto.'), code='invalid')
        if cleaned_data.get('participant_limit_subscription_end_datetime') <= cleaned_data.get('participant_limit_subscription_start_datetime'):
            validation_errors['participant_limit_subscription_end_datetime'] = ValidationError(_('O fim da inscrição ao Dia Aberto tem que ser depois do início da inscrição ao Dia Aberto.'), code='invalid')
        elif cleaned_data.get('participant_limit_subscription_end_datetime') > cleaned_data.get('start_datetime'):
            validation_errors['participant_limit_subscription_end_datetime'] = ValidationError(_('O período de inscrições ao Dia Aberto não pode terminar depois do começo do Dia Aberto.'), code='invalid')

        if validation_errors:
            raise ValidationError(validation_errors)


class ContactForm(forms.Form):
    class ContactType(TextChoices):
        TELEPHONE = 'TELEPHONE', _('Telefone')
        EMAIL = 'EMAIL', _('Correio Eletrónico')
        LOCALE = 'LOCALE', _('Localização')

    contact_type = forms.ChoiceField(choices=ContactType.choices, label='Tipo de contacto', widget=forms.RadioSelect())


class ContactEmailForm(forms.ModelForm):
    class Meta:
        model = ContactEmail
        fields = ['email']


class ContactTelephoneForm(forms.ModelForm):
    class Meta:
        model = ContactTelephone
        fields = ['telephone']
        widgets = {
            'telephone': IntlTelInputWidget(preferred_countries=['pt', 'es', 'gb'], default_code='pt'),
        }


class ContactLocaleForm(forms.ModelForm):
    # zip_code = PTZipCodeField(label='Código Postal')
    region = forms.ChoiceField(label='Região', initial=pt_regions.REGION_CHOICES[7], choices=pt_regions.REGION_CHOICES)

    class Meta:
        model = ContactLocale
        # fields = ['address', 'region', 'zip_code']
        fields = ['address', 'region']


# TRANSPORTS forms #
################################################################################

class UniTransportForm(forms.ModelForm):
    class Meta:
        model = UniTransport
        fields = ['name_identifier', 'transport_type',
                  'from_location', 'destination_location',
                  'departure_datetime']

    def clean_destination_location(self):
        data_from = self.cleaned_data['from_location']
        data = self.cleaned_data['destination_location']

        if data == data_from:
            raise ValidationError(_('Destino não pode ser igual a origem'))

        return data

# MEALS form #
################################################################################

class MenuFormSet(forms.ModelForm):
    class Meta:
        model = Menu
        fields = [
            'date',
        ]




class MealFormSet(forms.ModelForm):
    class Meta:
        model = Meal
        fields = [
            'meal_type',
            'name',
            'description',
        ]

class ConfigMealForm(forms.ModelForm):
    class Meta:
        model = ConfigMeal
        fields = [
            'normal_prof_price',
            'normal_student_price',
            'eco_prof_price',
            'eco_student_price',
        ]

MenuForm = inlineformset_factory(Menu, Meal, fields=('name', 'meal_type',))
