from django.contrib import admin

from utils.mixins import SingleInstanceAdminMixin
from .models import *

class ConfigAdmin(SingleInstanceAdminMixin, admin.ModelAdmin):
    model = Config

class ConfigMealAdmin(SingleInstanceAdminMixin, admin.ModelAdmin):
    model = ConfigMeal

admin.site.register(Config, ConfigAdmin)
admin.site.register(Contact)
admin.site.register(ContactEmail)
admin.site.register(ContactTelephone)
admin.site.register(ContactLocale)
admin.site.register(Transport)
admin.site.register(OwnTransport)
admin.site.register(UniTransport)
admin.site.register(ConfigMeal, ConfigMealAdmin)


class MealInline(admin.TabularInline):
    model = Meal
    extra = 1

class MenuAdmin(admin.ModelAdmin):
    inlines = [
      MealInline,
    ]

admin.site.register(Menu, MenuAdmin)
