from utils.mixins import UserTypesTestMixin
from users.models import Administrator
from django.core.exceptions import PermissionDenied
from .models import Config


class AdministratorTestMixin(UserTypesTestMixin):
    """Makes sure the user accessing the view is an Administrator."""
    user_types = (Administrator,)




#checks if config exists

class ConfigDiaAbertoExists:
    def dispatch(self, request, *args, **kwargs):
        if Config.objects.first():
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied()

