from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.utils.http import urlencode
from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.forms import inlineformset_factory
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from django.templatetags.static import static
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

from localflavor.pt import pt_regions

from users.models import Administrator
from utils.decorators import user_types_test
from inscricao.models import SubscriptionSessionAtivitiy
from .mixins import AdministratorTestMixin
from .models import (
    Config,
    ConfigMeal,
    Contact,
    ContactEmail,
    ContactTelephone,
    ContactLocale,
    UniTransport,
    Meal,
    Menu,
)
from .forms import (
    ConfigForm,
    ConfigMealForm,
    ContactForm,
    ContactEmailForm,
    ContactTelephoneForm,
    ContactLocaleForm,
    UniTransportForm,
    MenuFormSet,
    MealFormSet,
)

from django.contrib.auth.models import Group # TEMP

################################################################################
# Config Views #

def config_detail_view(request):
    config = Config.objects.first()
    if config:
        contacts = config.contact_set.all()
        contacts_email = []
        contacts_telephone = []
        contacts_locale = []
        for contact in contacts: # divides the contacts into their types, for easier templating
            if contact.is_contact_email():
                contacts_email.append(contact.contactemail)
            elif contact.is_contact_telephone():
                contacts_telephone.append(contact.contacttelephone)
            elif contact.is_contact_locale():
                contacts_locale.append(contact.contactlocale)

        context = {
            'config': config,
            'contacts_email': contacts_email,
            'contacts_telephone': contacts_telephone,
            'contacts_locale': contacts_locale,
            'pt_regions': pt_regions.REGION_CHOICES, # needed to convert the stored value in the template
        }

        return render(request, 'config_dia_aberto/config.html', context)

    return render(request, 'config_dia_aberto/config_missing.html')

class ConfigMealCreateView(SuccessMessageMixin, AdministratorTestMixin, CreateView):
    template_name = 'config_dia_aberto/configmeal_form.html'
    model = ConfigMeal
    form_class = ConfigMealForm
    success_url = '/config/meal/'
    success_message = 'Configuração de Refeições criada com sucesso!'

    def get_success_url(self, **kwargs):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return reverse('config_dia_aberto:config-meal-list')



class ConfigActivityUpdateView(AdministratorTestMixin, SuccessMessageMixin, UpdateView):
    template_name = 'config_dia_aberto/configmeal_form_update.html'
    success_message = 'Configuração alterada com sucesso!'
    form_class = ConfigMealForm

    def get_success_url(self, **kwargs):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return reverse('config_dia_aberto:config-meal-list')

    def get_object(self):
        """
        Returns the first Object of the model because there will be only one of these
        """
        return ConfigMeal.objects.first()


class ConfigMealListView(SuccessMessageMixin, ListView):
    template_name = 'config_dia_aberto/configmeal_list.html'
    context_object_name = 'meal'
    def get_queryset(self, **kwargs):
        return ConfigMeal.objects.first()



class ConfigCreateView(SuccessMessageMixin, AdministratorTestMixin, CreateView):
    model = Config
    form_class = ConfigForm
    success_url = '/config/'
    success_message = 'Configuração criada com sucesso!'
    extra_context = {'breadcrumb': 'Criar Configuração',}

    def get_success_url(self):
        if self.request.GET.get('next', '/') != '/':
            return self.request.GET.get('next', '/')
        return self.success_url

    def get(self, request, *args, **kwargs):
        if self.model.objects.first():
            return redirect('config_dia_aberto:config-update')
        return super().get(request, *args, **kwargs)


    # def form_valid(self, form):
    #     return super().form_valid(form)


@user_types_test(Administrator)
def config_update_view(request):
    config = Config.objects.first()
    if config:
        form = ConfigForm(request.POST or None, instance=config)
        if form.is_valid():
            form.save()
            messages.success(request, 'Configuração alterada com sucesso!')
            return redirect('config_dia_aberto:config')
        return render(request, 'config_dia_aberto/config_form.html', {'form': form, 'breadcrumb': 'Alterar Configuração',})

    raise Http404("No existing Dia Aberto configuration.")

################################################################################
# Contact Views #

@user_types_test(Administrator)
def contact_create_view(request):
    contact_form = ContactForm(request.POST or None)
    contact_email_form = ContactEmailForm(request.POST or None)
    contact_telephone_form = ContactTelephoneForm(request.POST or None)
    contact_locale_form = ContactLocaleForm(request.POST or None)

    context = {
        'contact_form': contact_form,
        'contact_email_form': contact_email_form,
        'contact_telephone_form': contact_telephone_form,
        'contact_locale_form': contact_locale_form,
    }

    config = Config.objects.first()
    if config: # Checks if config is not None
        if contact_form.is_valid():
            # Check the contact_type and save the correct contact
            if contact_form.cleaned_data['contact_type'] == ContactForm.ContactType.TELEPHONE \
            and contact_telephone_form.is_valid():
                contact_telephone_form.instance.config = config
                contact_telephone_form.save()
                messages.success(request, 'Contacto criado com sucesso!')
                return redirect('config_dia_aberto:config')
            elif contact_form.cleaned_data['contact_type'] == ContactForm.ContactType.EMAIL \
            and contact_email_form.is_valid():
                contact_email_form.instance.config = config
                contact_email_form.save()
                messages.success(request, 'Contacto criado com sucesso!')
                return redirect('config_dia_aberto:config')
            elif contact_form.cleaned_data['contact_type'] == ContactForm.ContactType.LOCALE \
            and contact_locale_form.is_valid():
                contact_locale_form.instance.config = config
                contact_locale_form.save()
                messages.success(request, 'Contacto criado com sucesso!')
                return redirect('config_dia_aberto:config')

        return render(request, 'config_dia_aberto/contact_form.html', context)

    raise Http404("No existing Dia Aberto configuration.")



class ContactEmailUpdateView(SuccessMessageMixin, AdministratorTestMixin, UpdateView):
    model = ContactEmail
    form_class = ContactEmailForm
    success_url = '/config/'
    success_message = 'Contacto alterado com sucesso!'


class ContactEmailDeleteView(SuccessMessageMixin, AdministratorTestMixin, DeleteView):
    model = ContactEmail
    success_url = '/config/'
    success_message = 'Contacto eliminado com sucesso!'

    def delete(self, request, *args, **kwargs):
        result = super().delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return result


class ContactTelephoneUpdateView(SuccessMessageMixin, AdministratorTestMixin, UpdateView):
    model = ContactTelephone
    form_class = ContactTelephoneForm
    success_url = '/config/'
    success_message = 'Contacto alterado com sucesso!'


class ContactTelephoneDeleteView(SuccessMessageMixin, AdministratorTestMixin, DeleteView):
    model = ContactTelephone
    success_url = '/config/'
    success_message = 'Contacto eliminado com sucesso!'

    def delete(self, request, *args, **kwargs):
        result = super().delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return result


class ContactLocaleUpdateView(SuccessMessageMixin, AdministratorTestMixin, UpdateView):
    model = ContactLocale
    form_class = ContactLocaleForm
    success_url = '/config/'
    success_message = 'Contacto alterado com sucesso!'


class ContactLocaleDeleteView(SuccessMessageMixin, AdministratorTestMixin, DeleteView):
    model = ContactLocale
    success_url = '/config/'
    success_message = 'Contacto eliminado com sucesso!'

    def delete(self, request, *args, **kwargs):
        result = super().delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return result


################################################################################
# Transport Views #


class UniTransportListView(ListView):
    model = UniTransport
    extra_context = {'locations': UniTransport.TransportLocation,}

    def get_queryset(self): # Not tested for participant
        """
            For a participant user, gets only the UniTransports that he has chosen in his subscriptions,
            for an administrator user, gets all Unitransports and
            for all other users, gets all UniTransports published
        """
        if hasattr(self.request.user, 'participant'):
            transports = set()
            subs = self.request.user.participant.subscription_set.all()
            ssas = SubscriptionSessionAtivitiy.objects.none()

            for sub in subs:
                ssas = ssas.union(sub.subscriptionsessionativitiy_set.all())

            for ssa in ssas:
                transports = transports.add(ssa.uni_transport.pk)

            return self.model.objects.filter(pk__in=transports)

        elif hasattr(self.request.user, 'administrator'):
            return super().get_queryset()

        return self.model.objects.filter(status=self.model.UniTransportStatus.PUBLICADO)

    def get(self, request, *args, **kwargs):
        # needed in template for redirection to config creation if NoneType
        self.extra_context['config'] = Config.objects.first()
        return super().get(request, *args, **kwargs)


# class UniTransportDetailView(AdministratorTestMixin, DetailView):
#     model = UniTransport


class UniTransportCreateView(SuccessMessageMixin, AdministratorTestMixin, CreateView):
    model = UniTransport
    form_class = UniTransportForm
    success_url = '/transportes/'
    success_message = 'Transporte criado com sucesso!'
    extra_context = {'breadcrumb': 'Criar Transporte',} # Correcting the breadcrumb in the template

    def get(self, request, *args, **kwargs):
        # dates needed to limit the datepicker
        self.extra_context['start_date'] = Config.objects.first().start_datetime.strftime('%Y-%m-%d')
        self.extra_context['end_date'] = Config.objects.first().end_datetime.strftime('%Y-%m-%d')
        return super().get(request, *args, **kwargs)


class UniTransportUpdateView(SuccessMessageMixin, AdministratorTestMixin, UpdateView):
    model = UniTransport
    form_class = UniTransportForm
    success_url = '/transportes/'
    success_message = 'Transporte alterado com sucesso!'
    extra_context = {'breadcrumb': 'Alterar Transporte',}

    def get(self, request, *args, **kwargs):
        self.extra_context['start_date'] = Config.objects.first().start_datetime.strftime('%Y-%m-%d')
        self.extra_context['end_date'] = Config.objects.first().end_datetime.strftime('%Y-%m-%d')
        return super().get(request, *args, **kwargs)

    # MAYBE TODO: Change this so it doesn't do 2 save() queries to the database
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.status = self.model.UniTransportStatus.POR_PUBLICAR
        self.object.save()
        return super().post(request, *args, **kwargs)


class UniTransportDeleteView(AdministratorTestMixin, DeleteView):
    model = UniTransport
    success_url = '/transportes/'
    success_message = 'Transporte eliminado com sucesso!'

    def delete(self, request, *args, **kwargs):
        result = super().delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return result



class UniTransportPublishView(AdministratorTestMixin, ListView):
    """
        Provide the ability to publish UniTransport objects.
        This view piggybacks off ListView, so it can obtain a queryset of instances
        and act on all of them
        Inspired by django's DeleteView, for more info on django class based views:
        https://ccbv.co.uk/
    """
    model = UniTransport
    template_name_suffix = '_confirm_publish'
    success_url = '/transportes/'
    success_message = 'Transportes publicados com sucesso!'

    def get_queryset(self):
        return self.model.objects.filter(status=self.model.UniTransportStatus.POR_PUBLICAR)

    def publish(self, request, *args, **kwargs):
        """
        Publish the UniTransport objects that haven't been published yet.
        """
        self.object_list = self.get_queryset()
        for obj in self.object_list:
            obj.status = self.model.UniTransportStatus.PUBLICADO
            obj.save()

        return HttpResponseRedirect(self.success_url)

    # Add support for browsers which only accept GET and POST for now.
    def post(self, request, *args, **kwargs):
        result = self.publish(request, *args, **kwargs)
        messages.success(self.request, self.success_message)
        return result


################################################################################
# MEALSs Views #


class MealListView(ListView):
    model = Menu

    def get(self, request, *args, **kwargs):

        # show this alternative page for menu list when no configuration
        if not Config.objects.first():
            return render(request, 'config_dia_aberto/menu_list_not_ready.html')

        return super().get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # config is always defined sinse get_context_data() is
        # called only if super().get() is called on get()
        config = Config.objects.first()

        # compute variables to the template
        context['start_datetime'] = config.start_datetime.date()
        context['end_datetime'] = config.end_datetime.date()
        context['daysRange'] = (config.end_datetime - config.start_datetime).days + 1
        context['menusAddedCount'] = Menu.objects.all().count()

        # reference to access in template all types of options
        context['all_meal_types'] = Meal.MealType

        return context


class MenuCreateView(AdministratorTestMixin, SuccessMessageMixin, CreateView):
    model = Menu
    form_class = MenuFormSet
    success_message = 'Refeição criada com sucesso!'

    object = None

    def get(self, *args, **kwargs):
        config = Config.objects.first()

        # check if days range is configured
        if not config:
            return redirect('config_dia_aberto:meal-list')

        # check if all days are full, and if they are
        # no more can be added so redirect to list view
        num_of_days = (config.end_datetime - config.start_datetime).days + 1
        if num_of_days <= Menu.objects.all().count():
            return redirect('config_dia_aberto:meal-list')


        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        return self.render_to_response(
            self.get_context_data(
                form = form,
                meal_form=MealFormSet(),
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['titleMsg']     = "criar refeição"
        context['formTitleMsg'] = "Criar Refeição"
        context['submitBtnMsg'] = "Criar refeição"

        config = Config.objects.first()
        context['start_datetime'] = config.start_datetime.__str__()
        context['end_datetime'] = config.end_datetime.__str__()

        disabled_days = []
        for menu in Menu.objects.all():
            disabled_days.append(menu.date.__str__())

        context['disabled_days'] = disabled_days

        return context

    def post(self, request, **kwargs):
        config = Config.objects.first()

        # check if days range is configured
        if not config:
            return redirect('config_dia_aberto:meal-list')

        # get menu form
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        # check if date format is valid
        if not form.is_valid():
            # invalid date, can be due to race condition.
            return HttpResponse("invalid date", status=400)

        # check if the new menu date is in the period defined in the config
        if form.cleaned_data['date'] < config.start_datetime.date() \
        or form.cleaned_data['date'] > config.end_datetime.date():
            return HttpResponse("date must be in the defined data range", status=400)

        # check if the new menu date is not occupied by other menu
        if Menu.objects.filter(date=form.cleaned_data['date']).exists():
            return HttpResponse("already exist a menu with this date", status=400)

        # validate meal forms
        mealForms = []
        for mealName, mealType, mealDesc in zip(request.POST.getlist('name'), request.POST.getlist('meal_type'), request.POST.getlist('description')):
            meal = MealFormSet({
                'name': mealName,
                'meal_type': mealType,
                'description': mealDesc,
            })
            if not meal.is_valid():
                return HttpResponse("invalid meal", status=400)
            mealForms.append(meal)

        # check if is at least one meal added to the menu
        if len(mealForms) < 1:
            return HttpResponse("must have at least 1 meal", status=400)

        menu = Menu(**form.cleaned_data)
        menu.save()
        for mealForm in mealForms:
            meal = Meal(**mealForm.cleaned_data)
            meal.menu = menu
            meal.save()

        messages.success(self.request, self.success_message)
        return redirect('config_dia_aberto:meal-list')


class MenuUpdateView(AdministratorTestMixin, SuccessMessageMixin, CreateView):
    model = Menu
    form_class = MenuFormSet
    success_message = 'Refeição atualizada com sucesso!'

    object = None

    def get(self, *args, **kwargs):
        config = Config.objects.first()

        try:
            # get menu to update
            menu = Menu.objects.get(pk=self.kwargs['pk'])
        except self.model.DoesNotExist:
            return redirect('config_dia_aberto:meal-list')

        # redirect to the list if the meal doesn't exist
        if not menu:
            return redirect('config_dia_aberto:meal-list')

        # the creation is ready. days range must be configured first
        if not config:
            return redirect('config_dia_aberto:meal-list')

        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        mealDate = menu.getFormatedDate()

        # if date is out of of the configuration day range remove
        # default value to avoid user of submitting the invalid date
        if menu.date < config.start_datetime.date() \
        or menu.date > config.end_datetime.date():
            mealDate = ""

        # get old data to template
        initialData = {
            'mealDate': mealDate,
            'mealOptions': Meal.objects.filter(menu=menu)
        }

        return self.render_to_response(
            self.get_context_data(
                form = form,
                meal_form=MealFormSet(),
                initialData=initialData,
                menu=menu,
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['titleMsg']     = "alterar refeição"
        context['formTitleMsg'] = "Alterar Refeição"
        context['submitBtnMsg'] = "Guardar alterações"

        config = Config.objects.first()
        context['start_datetime'] = config.start_datetime.date().__str__()
        context['end_datetime'] = config.end_datetime.__str__()

        meal_to_update = kwargs['menu']
        disabled_days = []
        for menu in Menu.objects.all():
            if menu != meal_to_update:
                disabled_days.append(menu.date.__str__())

        context['disabled_days'] = disabled_days

        context['to_update_date'] = meal_to_update.date.__str__()

        return context


    def post(self, request, **kwargs):
        config = Config.objects.first()

        # the creation is ready. days range must be configured first
        if not config:
            return redirect('config_dia_aberto:meal-list')

        # get menu form
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        # check if date format is valid
        if not form.is_valid():
            # invalid date, can be due to race condition.
            return HttpResponse("invalid date", status=400)

        # check if the new menu date is in the period defined in the config
        if form.cleaned_data['date'] < config.start_datetime.date() \
        or form.cleaned_data['date'] > config.end_datetime.date():
            return HttpResponse("date must be in the defined data range", status=400)

        # get menu to be updated
        menu = Menu.objects.get(pk=self.kwargs['pk'])

        # check if date is changed after update
        if menu.date != form.cleaned_data['date']:
            # check if the new menu date is not occupied by other menu
            if Menu.objects.filter(date=form.cleaned_data['date']).exists():
                return HttpResponse("already exist a menu with this date", status=400)

            menu.date = form.cleaned_data['date']

        # validate meal forms
        mealForms = []
        for mealName, mealType, mealDesc in zip(request.POST.getlist('name'), request.POST.getlist('meal_type'), request.POST.getlist('description')):
            meal = MealFormSet({
                'name': mealName,
                'meal_type': mealType,
                'description': mealDesc,
            })
            if not meal.is_valid():
                return HttpResponse("invalid meal", status=400)
            mealForms.append(meal)

        # check if is at least one meal added to the menu
        if len(mealForms) < 1:
            return HttpResponse("must have at least 1 meal", status=400)

        # delete all old options in menu
        Meal.objects.filter(menu=menu).delete()

        menu.save()
        for mealForm in mealForms:
            meal = Meal(**mealForm.cleaned_data)
            meal.menu = menu
            meal.save()

        messages.success(self.request, self.success_message)
        return redirect('config_dia_aberto:meal-list')


# @user_types_test(Administrator)
def delete_meal(request, pk):
    # the creation is ready. days range must be configured first
    if not Config.objects.first():
        return HttpResponse("require config", status=423)

    if not request.user.is_authenticated:
        return HttpResponse("auth required", status=401)

    if not hasattr(request.user, "administrator"):
        return HttpResponse("not admin", status=403)

    try:
        Menu.objects.get(pk=pk).delete()
        return HttpResponse(f"menu {pk} deleted", status=200)
    except Menu.DoesNotExist:
        return HttpResponse("menu not found", status=404)
